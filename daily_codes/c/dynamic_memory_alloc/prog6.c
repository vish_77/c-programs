#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr1 = (int*)calloc(5,sizeof(int));
	printf("Enter values = \n");
	for(int i = 0; i<5; i++){
		scanf("%d",(ptr1+i));
	}

	printf("ptr1 = \n");

	for(int i = 0; i<5; i++){
		printf("%d\n",*(ptr1+i));
	}

	printf("Enter values of ptr2= \n");

	int* ptr2 = (int *)realloc(ptr1,8);

	for(int i = 5; i<8; i++){
		scanf("%d",(ptr2+i));
	}
	
	printf("ptr2 = \n");
	for(int i = 0; i<8; i++){
		printf("%d\n",*(ptr2+i));
	}
	
	free(ptr1);
}		
