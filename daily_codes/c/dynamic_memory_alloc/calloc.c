#include<stdio.h>
#include<stdlib.h>
extern void free (void *__ptr) __THROW;

void main(){

	int *ptr = (int*)calloc(5,sizeof(int));

	for(int i = 0; i<5; i++){
		*(ptr+i) = 10+i;
	}

	for(int i = 0; i<5; i++){	
		printf("%d\n",*(ptr+i));
	}

	printf("realloc = \n");

	int *ptr2 = (int *)realloc(ptr,9);

	for(int i = 0; i<8; i++){
		*(ptr2 + i) = 10+i;
	}
	
	printf("ptr2 = \n");
	for(int i = 0; i<8; i++){

		printf("%d\n",*(ptr2+i));
	}

	printf("ptr = \n");

	for(int i = 0; i<5; i++){
		printf("%d\n",*(ptr+i));
	}
	free(ptr);
}
