/**
 *
 * & = Bitwise and
 * | = Bitwise or
 * ^ = Bitwise XOR
 * << = leftshift
 * >> = Rightshift
 *
 */

#include<stdio.h>
void main(){
	int x = 4;
	int y = 5;
	int ans;
	ans = x & y;
	printf("%d\n",ans);
}
