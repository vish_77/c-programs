/*

# # # # # # # # # # # 
  # # # # # # # # # 
    # # # # # # # 
      # # # # # 
        # # # 
          # 

*/

#include<stdio.h>
void main(){
	int rows,cols;
	
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	
	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i = 0;i<rows;i++){

		for(int j = 0;j<i;j++){
			printf("  ");
		}
		
		for(int k = 0;k<((2*rows)-i*2-1);k++){
			printf("# ");
		}
		printf("\n");
	}

}

