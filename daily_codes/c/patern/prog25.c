/*
      A 
    A B 
  A B C 
A B C D 

*/

#include<stdio.h>
void main(){
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	
	for(int i = 1;i<=rows;i++){
		for(int j = 1; j<= rows-i;j++){
			printf("  ");
		}
		
		for(int j = 1;j<=i;j++){
			printf("%c ",j+64);
		}
		printf("\n");
	}
}
