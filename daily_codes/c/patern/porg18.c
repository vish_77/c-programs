/*
 
 	 	 	 	 	 	 	a	
 	 	 	 	 	 	1	2	3	
 	 	 	 	 	b	c	d	e	f	
 	 	 	 	4	5	6	7	8	9	10	
 	 	 	g	h	i	j	k	l	m	n	o	
 	 	11	12	13	14	15	16	17	18	19	20	21

*/

#include<stdio.h>
void main(){
	int rows,cols,num = 1;
	char chr = 97;

	printf("Enter no of rows = ");
	scanf("%d",&rows);

	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i = 0 ;i<rows;i++){

		for(int j=0; j<(rows-i+1);j++){
			printf(" \t");
		}

		for(int k = 0; k<(2*i+1);k++){
			if(i%2 == 0){
				printf("%c\t",chr);
				chr ++;
			}else{
				printf("%d\t",num);
				num ++;
			}
		}

		printf("\n");
	}
}
