/* 
 * WAP a program to take marks from user and print the sum of marks 
 * /
 */


#include<stdio.h>
void main(){
	int marks;
	int sum = 0 ;

	for(int i = 0; i<5; i++){
		printf("Enter subject %d marks\n",i);
		scanf("%d",&marks);
		sum += marks;
	}
	
	printf("%d is your total marks \n",sum);
}
