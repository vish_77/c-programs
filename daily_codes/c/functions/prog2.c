#include<stdio.h>
void fun(int *x){
	printf("%p\n",x);
}
void main(){
	int x = 10;	//warning: passing argument 1 of ‘fun’ makes pointer from integer without a cast [-Wint-conversion]

	fun(x);
}
