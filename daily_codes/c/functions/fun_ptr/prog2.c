#include<stdio.h>
void add(int,int);
void sub(float,float);
void mult(int,int);

void main(){

	void (*ptr)(int,int);

	ptr = add;
	ptr(20,30);

	ptr = sub;	//warning: assignment to ‘void (*)(int,  int)’ from incompatible pointer type ‘void (*)(float,  float)’
	ptr(50,20);

	ptr = mult;
	ptr(2,4);
}

void add(int a, int b){
	printf("%d\n", a+b);
}

void sub(float a, float b){
	printf("%f\n", a-b);

}

void mult(int a, int b){
	printf("%d\n",a*b);
}
