#include<stdio.h>
void CallByAddress(int*);

void main(){
	int x = 10;
	
	printf("%d\n",x);
	printf("%p\n",&x);

	CallByAddress(&x);
	
	printf("%d\n",x);
}

void CallByAddress(int *ptr){

	printf("%p\n",ptr);
	printf("%d\n",*ptr);

	*ptr = 50;

	printf("%d\n",*ptr);
}

