#include<stdio.h>
int ArrSum(int*,int);

void main(){

	int arr[] = {10,20,30,40,50};
	int arrSize = sizeof(arr)/sizeof(int);

	int sum = ArrSum(arr,arrSize);

	printf("Sum of array elements = %d\n",sum);

}
int ArrSum(int *ptr, int arrSize){
        int sum = 0;

        for(int i = 0; i<arrSize; i++){
                sum = sum + *(ptr+i);
        }
        return sum;

}
