#include<stdio.h>

int diagSum1(int (*ptr)[3][3],int p,int x, int y){

	int sum = 0;
	
	for(int i = 0; i < p; i++){
		
		for(int j = 0; j<x; j++){

			for(int k = 0 ; k<y; k++){

				if(j + k % 2 == 0){
					sum = sum + *(*(*(ptr+i)+j)+k);
				}
			}
		}
	}

	return sum;
}

void main(){

	int arr[2][3][3] = {{1,2,3,4,5,6,7,8,9},{10,11,12,13,14,15,16,17,18}};

	int sum = diagSum1(arr,2,2,3);
	//int sum = diagSum2(arr);
	
	printf("%d\n",sum);
}

