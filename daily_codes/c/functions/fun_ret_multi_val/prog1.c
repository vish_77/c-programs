#include<stdio.h>

void fun(int a, int b,int *ptradd, int *ptrsub){
	*ptradd = a+b;
	*ptrsub = a-b;
}

void main(){

	int x= 10;
	int y = 20;

	int add;
	int sub;

	fun(x,y,&add,&sub);
	printf("add = %d\n",add);
	printf("sub = %d\n",sub);
}
