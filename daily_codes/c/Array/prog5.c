//Assigning array elemnts

#include<stdio.h>
void main(){
	int arr[3];
	arr[0] = 10;
	arr[1] = 20;
	arr[2] = 30;
	
	printf("Array 1 elements\n");
	for(int i = 0;i<3;i++){
		printf("%d\n",arr[i]);
	}
	
	int arr2[3];
	for(int i = 0;i<3;i++){
		arr2[i]=arr[i];
	}

	printf("Array 2 elements\n");
	for(int i = 0;i<3;i++){
		printf("%d\n",arr2[i]);
	}
}
