#include<stdio.h>
void main(){
//	int arr1[][] = {1,2,3,4,5,6,7,8,9};	
//	error: array type has incomplete element type ‘int[]’
//	note: declaration of ‘arr1’ as multidimensional array must have bounds for all dimensions except the first

	int arr[3][3] = {1,2,3,4,5,6,7,8,8};
	
	/*
	1	2	3
	4	5	6
	7	8	8
	*/

	int arr2[2][3] = {1,2,3,4};
	/* 
	1 2 3
	4 0 0
	*/

	
	int arr3[][3] = {1,2,3,4,5,6,7,8,9};
	/*
	1	2	3
	4	5	6	
	7	8	9
	*/

	int arr4[][3] = {1,2,3,{4,5},{6}};
	/*
	1	2	3
	4	5	0
	6	0	0
	*/

	int arr5[][3] = {1,2,3,4,{5,6}};
	/*
	1 2 3 
	4 5 0
        */

	int arr6[][3] = {{1,2,3,4},{5,6},{7,8,9}};

	/*
	1	2	3
	5	6	0
	7	8	9	
	*/


}
