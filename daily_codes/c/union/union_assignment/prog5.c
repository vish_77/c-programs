/*	
 *	for memory allocation base value treated as 8 bytes i.e. highest datatype 
 *	array of ch is of 30 bytes
 *	hence 4 plates of 8 bytes can hold this ch array
 *	hence size of union Demo is of 32 bytes
*/


#include<stdio.h>

union Demo{

	char ch[30];
	int * x;
	int y;
	char z;
	double a;

}obj1;

union Demo2{

	int arr[7];
	char ch;
	int x;
}obj2;

void main(){

	printf("%ld\n",sizeof(obj1));	//32
	printf("%ld\n",sizeof(obj2));	//28

}
