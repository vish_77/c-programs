#include<stdio.h>

union Demo{

	int x;
	int y;
}obj1={10};

//obj1.x=10;		//error
//obj1.y=10;
void fun(){

	union Demo obj2;
	
	obj2.x=20;
	printf("%d\n",obj2.x);
	
	obj2.y=30;
	printf("%d\n",obj2.x);

}

void main(){
	
	printf("%d\n",obj1.x);
	printf("%d\n",obj1.y);

	fun();

	union Demo obj3;
	
	obj3.x=40;
	printf("%d\n",obj3.x);
	
	obj3.y=50;
	printf("%d\n",obj3.y);
}
