#include<stdio.h>

union Employee{

	int empId;
	float sal;
};

void main(){
	
	union Employee emp1 = {10,50.20};
	
	printf("%d\n",emp1.empId);	//10
	printf("%f\n",emp1.sal);	//0.0000
}
