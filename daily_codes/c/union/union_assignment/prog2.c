#include<stdio.h>

union Employee{

	int empId;
	float sal;
};

void main(){
	
	//problem
	union Employee emp1 = {10,50.20};	//warning: excess elements in union initializer
	
	//proper way
	union Employee emp2;

	emp2.empId = 15;
	printf("%d\n",emp2.empId);	//15

	emp2.sal = 65.43;
	printf("%f\n",emp2.sal);	//65.43
}
