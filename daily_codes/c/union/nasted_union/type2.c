#include<stdio.h>
#include<string.h>

union Movie{

	char mName[20];
	float imdb;

	union actorInfo{
	
		char actName[20];
		float charge;
	}obj;
};

void main(){

	union Movie obj1;

	strcpy(obj1.mName,"Bramhashtra");
	printf("%s\n",obj1.mName);

	obj1.imdb = 7.6;
	printf("%f\n",obj1.imdb);

	strcpy(obj1.obj.actName,"Ranvir");
	printf("%s\n",obj1.obj.actName);

	obj1.obj.charge = 40.2;
	printf("%f\n",obj1.obj.charge);
}
