#include<stdio.h>

union Demo{

	int x;
	float y;
};

void gun(union Demo obj){
	printf("%f\n",obj.y);
}

void fun(union Demo *obj){

	printf("%f\n",obj->y);
}
void main(){

	union Demo obj;

	obj.x=10;
	obj.y=20.3;
	
	fun(&obj);
	gun(obj);	
}
