/*
1. If we pass string as commandline arguments then we can change that string
2. because its like passing string to the function as arguments hence its not stored in RO data stction
*/

#include<stdio.h>

void main(int argc, char *argv[]){	//argc => argument count
					//array of character pointer
	for(int i = 0; i<argc; i++)
		printf("%s\n",argv[i]);
	
	printf("%d\n",argc);
}
