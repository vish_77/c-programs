#include<stdio.h>
void main(){
	int x = 10;
	int y = 20;

	const int* const ptr = &x;

	*ptr = 30;	//error: assignment of read-only location ‘*ptr’
	ptr = &y;	//error: assignment of read-only variable ‘ptr’

}
