#include<stdio.h>

enum Partners{

	pramodsir,
	sri,
	sachin,
	akshay = 17,
	swapnil,
	pankaj,
	saurabh=2,
	kunal,
	raj,
	vishwajit
};

void main(){

	printf("%d\n",pramodsir);	//0
	printf("%d\n",sri);		//1
	printf("%d\n",sachin);		//2
	printf("%d\n",akshay);		//17
	printf("%d\n",swapnil);		//18
	printf("%d\n",pankaj);		//19
	printf("%d\n",saurabh);		//2
	printf("%d\n",kunal);		//3
	printf("%d\n",raj);		//4
	printf("%d\n",vishwajit);	//5

	int x = akshay;
	printf("%d\n",x);		//17

//	enum Partners obj = {vishal,omkar,parsha};	//error we cannot assign to enum like that
	
//	akshay = 12;	//error
	
}
