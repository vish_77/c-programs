#include<stdio.h>

enum  days{

	Sunday,
	Monday,
	Tuesday,
	Wednesday,
	Thursday,
	Friday,
	Saturday
};

void main(){

	enum days obj;			//there is no need of creating obj for enum
//	printf("%d\n",obj.Sunday);	//error: request for member ‘Sunday’ in something not a structure or union
	printf("%ld\n",sizeof(obj));

	printf("%d\n",Sunday);		//0
	printf("%d\n",Monday);		//1
	printf("%d\n",Tuesday);		//2
	printf("%d\n",Wednesday);	//3
	printf("%d\n",Thursday);	//4
	printf("%d\n",Friday);		//5
	printf("%d\n",Saturday);	//6
}
