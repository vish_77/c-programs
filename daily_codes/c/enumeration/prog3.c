#include<stdio.h>

enum players{

	sachin,
	virat,
	jadeja,
	ashwin,
	dhoni,
	pandiya,
};

void main(){
	
	enum players obj;

//	printf("%p\n",&sachin);		//error: lvalue required as unary ‘&’ operand
	printf("%p\n",&obj);
//	printf("%p\n",&obj.sachin);	//error: request for member ‘sachin’ in something not a structure or union

//	dhoni = 7;			//error: lvalue required as left operand of assignment

}

