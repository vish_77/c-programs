// Both fun() and gun() are same 
// functions having default extern storage class(linking scope)

#include<stdio.h>
void fun(){

	printf("In fun\n");
}

extern void gun(){

	printf("In gun\n");
}

