#include<stdio.h>
void main(){
	printf("Start main \n");
	
	char ch1 = 'A';
	char ch2 = '48' ;
	char ch3 = 48;

	if(ch1) {
		printf("In first if block \n");
	}

	if(ch2){
		printf("In second if block \n");
	}
	
	if(ch3){
		printf("In third if block \n");
	}

	printf("End main \n");

}
