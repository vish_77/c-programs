#include<stdio.h>
#include<string.h>

struct Movie{

	char mName[20];

	struct MovieInfo{
		char actor[20];
		float imdb;
	}obj1;
};

void main(){
	
	struct Movie obj3 = {"charli777",{"shetty",8.0}};
	struct Movie obj2;

	strcpy(obj2.mName,"Kantara");
	strcpy(obj2.obj1.actor,"Rushabh Shetty");
	obj2.obj1.imdb = 9.7;

	printf("%s\n",obj2.mName);
	printf("%s\n",obj2.obj1.actor);
	printf("%f\n",obj2.obj1.imdb);

	printf("%s\n",obj3.mName);
	printf("%s\n",obj3.obj1.actor);
	printf("%f\n",obj3.obj1.imdb);
}
