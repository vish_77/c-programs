#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Society{

	char sName[20];
	int wings;
	float avgRent;
};

void main(){

	struct Society *ptr = (struct Society*)malloc(sizeof(struct Society));

	strcpy(ptr->sName,"Sun Universe");
	ptr -> wings = 16;
	(*ptr).avgRent = 14500.00;

	printf("%s\n",(*ptr).sName);
	printf("%d\n",ptr->wings);
	printf("%f\n",ptr->avgRent);
}
