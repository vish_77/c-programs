#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct IPL{

	char sName[20];
	int tTeams;
	double prize;
};

void main(){

	struct IPL *ptr = (struct IPL*)malloc(sizeof(struct IPL));

	strcpy((*ptr).sName,"Tata");		
	ptr -> tTeams = 8;		
	(*ptr).prize = 10.00;		//both * . are same ->

	printf("%s\n",(*ptr).sName);
	printf("%d\n",(*ptr).tTeams);
	printf("%lf\n",(*ptr).prize);
	
	printf("%s\n",ptr -> sName);
	printf("%d\n",ptr -> tTeams);
	printf("%lf\n",ptr -> prize);
}
