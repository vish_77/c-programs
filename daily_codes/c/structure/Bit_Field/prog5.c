#include<stdio.h>
struct Demo{
	
	int d;
	int x:4;
	int y:4;
	int z:4;
	int a:4;
	int b:4;
	int c:5;
};

void main(){
	struct Demo obj = {14,15,16,17,18,19,32};
	
	printf("%p\n",obj.d);	
	printf("%d\n",obj.x);
	printf("%d\n",obj.y);
	printf("%d\n",obj.z);
	printf("%d\n",obj.a);
	printf("%d\n",obj.b);
	printf("%d\n",obj.c);

	printf("%d\n",obj);
	printf("%p\n",obj);
	
//	printf("%ld\n",sizeof(obj.x));	//error - sizeof is not applicable
}
