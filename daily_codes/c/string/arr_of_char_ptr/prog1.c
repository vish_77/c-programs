#include<stdio.h>
#include<string.h>

void main(){
	
	char *arr1[3] = {"Vishal","sanjay","Vishal"};

	printf("%p\n",arr1[0]);		//0x562894b6f004
	printf("%p\n",arr1[1]);		//0x562894b6f00b
	printf("%p\n",arr1[2]);		//0x562894b6f004
	
	puts(arr1[0]);			//Vishal

	printf("%s\n",arr1[2]);		//Vishal

	strcpy(arr1[2], "More");	//Segmentation fault (core dumped)

	puts(arr1[0]);
	printf("%s\n",arr1[2]);

}
