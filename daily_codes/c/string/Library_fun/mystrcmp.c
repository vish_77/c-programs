
#include<stdio.h>
#include<string.h>

int mystrcmp(char *str1, char *str2){
	
	int flag = 0;
	while((*str1 != '\0') && (*str2 != '\0')){
		
		flag ++;

		if(*str1 == *str2){
			str1++;
			str2++;

		}else{
			return *str1 - *str2;
		}
	}
	if(flag == 0){
		return 1;
	}else{
		return 0;
	}
}

void main(){

	char *str1 = "Vishal";
	char *str2 = "Visial";

	printf("%d\n",mystrcmp(str1,str2));

}

