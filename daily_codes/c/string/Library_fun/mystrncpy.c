#include<stdio.h>

char *mystrncpy(char *dest, char *src, int num){

	int i = 0;
	while(*src != '\0' && i<num){
		
		*dest = *src;
		dest++;
		src++;
		i++;
		
	}
	*dest = '\0';
	return dest;
}

void main(){

	char *str1 = "Vishal";
	char str2[20];
	int num = 4;

	mystrncpy(str2,str1,num);

	puts(str2);
}
