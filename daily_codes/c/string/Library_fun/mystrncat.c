#include<stdio.h>

char *mystrncat(char *dest, char *src, int num){

	int i = 0;

	while(*dest != '\0'){
		dest++;
	}

	while(*src != '\0' && i<num){
		*dest = *src;
		dest++;
		src++;
		i++;
	}
	*dest = '\0';
	return dest;
}

void main(){

	char *str1 = "More";
	char str2[20] = {'V','i','s','h','a','l','\0'};
	
	int num = 2;

	puts(str2);
	mystrncat(str2,str1,num);
	puts(str2);
}
