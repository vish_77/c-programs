#include<stdio.h>

int Mystrlen(char *ptr){

	int count = 0;

	while( *ptr != '\0'){
		count++;
		ptr++;
	}
	return count;
}

void main(){

	char name[10] = {'K','L','R','a','h','u','l','\0'};
	char *str = "Virat Kohli";

	int lenName = Mystrlen(name);
	int lenstr = Mystrlen(str);

	printf("%d\n",lenName);
	printf("%d\n",lenstr);
}
