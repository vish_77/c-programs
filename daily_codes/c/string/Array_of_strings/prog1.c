//Following both arr1 and arr2 are same both are stored in main stack frame


#include<stdio.h>
void main(){

	char arr1[3][10] = {"Vishal","Sanjay","More"};
	
	char arr2[][10] = {{'V','i','h','a','l','\0'},{'S','a','n','j','a','y','\0'},{'M','o','r','e','\0'}};
	
	printf("%p\n",&(arr1[1]));
	printf("%p\n",&(arr1[1][1]));
	printf("%p\n",arr1[2]);
	
}

