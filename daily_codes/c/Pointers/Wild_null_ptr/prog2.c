#include<stdio.h>
void main(){
	int x = 10;
	
	//Null pointers ptr1 and ptr2
	int *ptr1 = NULL;
	int *ptr2 = 0;

	printf("%p\n",ptr1);	//nil
	printf("%p\n",ptr2);	//nil

	printf("%d\n",*ptr1);	//segmentation fault	
	printf("%d\n",*ptr2);	//segmentation fault
}
