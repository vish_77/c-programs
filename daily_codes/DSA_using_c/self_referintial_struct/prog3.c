#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{
	int empId;
	char empName[20];
	float sal;
	struct Employee *next;
}Emp;

void main(){

	Emp *emp1 = (Emp*)malloc(sizeof(Emp));
	Emp *emp2 = (Emp*)malloc(sizeof(Emp));
	Emp *emp3 = (Emp*)malloc(sizeof(Emp));


	emp1 -> empId = 1;
	strcpy(emp1 -> empName,"Omkar");
	emp1 -> sal = 50.0;
	emp1 -> next = emp2;

	emp1 -> next -> empId = 2;
	strcpy(emp1 -> next -> empName,"Saurabh");
	emp1 -> next -> sal = 60.0;
	emp1 -> next -> next = emp3;

	emp1 -> next -> next -> empId = 3;
	strcpy(emp1 -> next -> next -> empName,"Pankaj");
	emp1 -> next -> next -> sal = 70.0;
	emp1 -> next -> next -> next = NULL;

	printf("%d\n",emp1 -> empId);
	printf("%s\n",emp1 -> empName);
	printf("%f\n",emp1 -> sal);
	printf("%p\n",emp1 -> next);

	printf("%d\n",emp1 -> next -> empId);
	printf("%s\n",emp1 -> next -> empName);
	printf("%f\n",emp1 -> next -> sal);
	printf("%p\n",emp1 -> next -> next);

	printf("%d\n",emp1 -> next -> next -> empId);
	printf("%s\n",emp1 -> next -> next -> empName);
	printf("%f\n",emp1 -> next -> next -> sal);
	printf("%p\n",emp1 -> next -> next -> next);
}
