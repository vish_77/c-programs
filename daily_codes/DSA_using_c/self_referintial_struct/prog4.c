#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Movie{

	int count;
	char mName[20];
	float imdb;
	struct Movie *next;
}mv;

void EnterData(mv *, mv *);
void Access(mv *);

void main(){

	mv *movie1 = (mv*)malloc(sizeof(mv));
	mv *movie2 = (mv*)malloc(sizeof(mv));
	mv *movie3 = (mv*)malloc(sizeof(mv));

	EnterData(movie1,movie2);
	EnterData(movie2,movie3);
	EnterData(movie3,NULL);
	
	Access(movie1);
	Access(movie2);
	Access(movie3);
}

void EnterData(mv *ptr, mv *ptr1){
	
	char arr[20];

	printf("Enter tikit count = ");
	scanf("%d",&(ptr->count));

	printf("Enter movie name = ");
	scanf("%s",arr);
	strcpy(ptr-> mName,arr);

	printf("enter imdb rating = ");
	scanf("%f",&(ptr->imdb));
	
	ptr->next = ptr1;
}

void Access(mv *ptr){

	printf("%d\n",ptr -> count);
	printf("%s\n",ptr -> mName);
	printf("%f\n",ptr -> imdb);
	printf("%p\n",ptr-> next);
}
