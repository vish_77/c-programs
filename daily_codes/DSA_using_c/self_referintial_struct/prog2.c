#include<stdio.h>
#include<string.h>

struct Batsman{

	int jerNo;
	char name[20];
	float avg;
	struct Batsman *next;
};

struct company{

	int compcount;
	char Name[20];
	float revenue;
	struct company *next;
};

void main(){
	
	struct Batsman obj1,obj2,obj3;
	struct company obj4,obj5,obj6;

	struct Batsman *head1 = &obj1;
	struct company *head2 = &obj4;

	head1 -> jerNo = 7;
	strcpy(head1 -> name,"Dhoni");
	head1 -> avg = 50.5;
	head1 -> next = &obj2;

	head1 -> next -> jerNo = 18;
	strcpy(head1 -> next -> name ,"virat");
	head1 -> next -> avg = 60.5;
	head1 -> next -> next = &obj3;

	head1 -> next -> next -> jerNo = 20;
	strcpy(head1 -> next -> next -> name,"rohit");
	head1 -> next -> next -> avg = 50.4;
	head1 -> next -> next -> next = NULL;
	
	printf("%d\n",head1 -> jerNo);
	printf("%s\n",head1 -> name);
	printf("%f\n",head1 -> avg);

	printf("%d\n",head1 -> next -> jerNo);
	printf("%s\n",head1 -> next -> name);
	printf("%f\n",head1 -> next -> avg);

	printf("%d\n",head1 -> next -> next -> jerNo);
	printf("%s\n",head1 -> next -> next -> name);
	printf("%f\n",head1 -> next -> next -> avg);

	head2 -> compcount = 1;
	strcpy(head2 -> Name,"Google");
	head2 -> revenue = 500.00;
	head2 -> next = &obj5;

	head2 -> next -> compcount = 2;
	strcpy(head2 -> next -> Name,"youtube");
	head2 -> next -> revenue = 400.0;
	head2 -> next -> next = &obj6;

	head2 -> next -> next -> compcount = 3;
	strcpy(head2 -> next -> next -> Name,"map");
	head2 -> next -> next -> revenue = 600.00;
	head2 -> next -> next -> next = NULL;

	printf("company information = \n");

	printf("%d\n",head2 -> compcount);
	printf("%s\n",head2 -> Name);
	printf("%f\n",head2 -> revenue);

	printf("%d\n",head2 -> next -> compcount);
	printf("%s\n",head2 -> next -> Name);
	printf("%f\n",head2 -> next -> revenue);

	printf("%d\n",head2 -> next -> next -> compcount);
	printf("%s\n",head2 -> next -> next -> Name);
	printf("%f\n",head2 -> next -> next -> revenue);
	
}
