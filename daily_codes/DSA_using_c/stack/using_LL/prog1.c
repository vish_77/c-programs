#include<stdio.h>
#include<stdlib.h>

int countnode = 0;

typedef struct Stack{
	int data;
	struct Stack *next;
}stack;

stack *head = NULL;

stack *CreateNode(){

	stack *stacknode = (stack*)malloc(sizeof(stack));

	printf("Enter data = ");
	scanf("%d",&stacknode -> data);

	stacknode -> next = NULL;
	
	return stacknode;
}
int flag = 0;

int NodeCount(){

	stack *temp = head;
	int count = 0;

	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

int push(){
	int count = NodeCount();
	if(countnode > count){
		flag = 0;
		printf("Stack overload\n");
	}
	stack *stacknode = CreateNode();
	if(head == NULL){
		head = stacknode;
	}else{
		stack *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = stacknode;
	}
}

int pop(){
	if(head == NULL){
		flag = 0;
		printf("Stack is empty\n");
		return -1;
	}else{
		flag = 1;
		if(head -> next == NULL){
			
			free(head);
			head = NULL;
			
		}else{
			stack *temp = head;
			int data;
			while(temp -> next -> next != NULL){
				temp = temp -> next;
			}
			data = temp -> next -> data;
			temp -> next = NULL;
			return data;
		}
	}
}

int peek(){

	if(head == NULL){
		printf("Invalid peek\n");
		return -1;
	}else{
		stack *temp = head;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		return temp -> data;
	}
}

void main(){

	char choice;
	do{
		printf("Enter 1 for push\n");
		printf("Enter 2 for pop\n");
		printf("Enter 3 for peek\n");

		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){

			case 1 :
				push();
				break;

			case 2 :
				printf("%d\n",pop());	
				break;

			case 3 :
				printf("%d\n",peek());
				break;

			default :
				printf("Invalid choice \n");
		}

		getchar();
		printf("Do you want to continue =");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');

}
