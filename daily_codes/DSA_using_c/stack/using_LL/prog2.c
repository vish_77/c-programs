/*
 
   All 5 functions of stack is implemented in Singly Linked list

*/


#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct Node{
	
	int data;
	struct Node *next;
}node;

node *head = NULL;
node *top = NULL;
int NodeCount = 0;
int flag = 0;

int eleCount(){

	node *temp = head;
	int count = 0;

	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

bool isFull(){

	if(eleCount() == NodeCount)
		return true;
	else
		return false;
}

node *CreateNode(){

	node *newnode = (node *)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();
	top = newnode;

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

int push(){

	if(isFull())
		return -1;
	else{
		AddNode();
		return 0;
	}
}

bool isEmpty(){

	if(eleCount()== 0)
		return true;
	else 
		return false;
}

int pop(){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		if(head -> next == NULL){
			flag = 1;
			int data = head -> data;
			free(head);
			head = NULL;
			return data;
		
		}else{
			top = head;
			flag = 1;

			while(top -> next -> next != NULL){

				top = top -> next;
			}
			int data = top -> data;
		       	free(top -> next);
			top -> next = NULL;
			return data;
		}
	}
}

int peek(){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return top -> data;
	}
}

void main(){

        printf("Enter size of stack = ");
        scanf("%d",&NodeCount);

        char choice;

        do{
                printf("Enter 1 for push\n");
                printf("Enter 2 for pop\n");
                printf("Enter 3 for peek\n");
                printf("Enter 4 for isFull\n");
                printf("Enter 5 for isEmpty\n");

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int ret;
                                        ret = push();

                                        if(ret == -1){
                                                printf("Stack overflow\n");
                                        }
                                }
                                break;

                        case 2 :
                                {
                                        int ret;
                                        ret = pop();

                                        if(flag == 0)
                                                printf("Stack underflow\n");
                                        else
                                                printf("%d is poped\n",ret);

                                }
                                break;
                        case 3 :
                                {
                                        int ret;
                                        ret = peek();
                                        if(flag == 0){
                                                printf("stack is empty\n");
                                        }else{
                                                printf("%d is peeked\n",ret);
                                        }
                                }
                                break;

                        case 4 :
                                {
                                        if(isFull())
                                                printf("Stack is full\n");
                                        else
                                                printf("Stack is not full\n");
                                }
                                break;

                        case 5 :{
                                        if(isEmpty())
                                                printf("Stack is empty\n");
                                        else
                                                printf("Stack is not empty\n");
                                }
                                break;

                        default :
                                printf("Enter correct choice\n");

                        }



                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);
        }while(choice == 'Y' || choice == 'y');
}


