/*
 
   take array elements and data from the user and print the sum of array ele.

*/

#include<stdio.h>

void main(){

	int sum = 0;

	int ele;
	printf("Enter number of elements = ");
	scanf("%d",&ele);

	int arr[ele];
	printf("Enter elements = \n");
	for(int i = 0; i< ele; i++){

		scanf("%d",&arr[i]);
		sum = sum + arr[i];
	}

	printf("%d\n",sum);
}

