/*
 
   All 5 stack functions are implemented 

 */


#include<stdio.h>
#include<stdbool.h>

int size = 0;
int flag = 0;
int top = -1;

bool isFull(){

	if(top == size-1)
		return true;
	else
		return false;
}

int push(int stack[]){

	if(isFull()){
		return -1;
	}else{
		printf("Enter data = ");
		scanf("%d",&stack[++top]);
		return 0;
	}
}

bool isEmpty(){

	if(top == -1)
		return true;
	else
		return false;
}

int pop(int *stack){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		int val = stack[top];
		top--;
		flag = 1;
		return val;
	}
}

int peek(int stack[]){

	if(isEmpty()){
		flag = 0;
		return -1;
	}else{
		int val = stack[top];
		flag = 1;
		return val;
	}
}


void main(){

	printf("Enter size of stack = ");
	scanf("%d",&size);

	int stack[size];
	char choice;

	do{
		printf("Enter 1 for push\n");
		printf("Enter 2 for pop\n");
		printf("Enter 3 for peek\n");
		printf("Enter 4 for isFull\n");
		printf("Enter 5 for isEmpty\n");

		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){

			case 1 :
				{
					int ret;
					ret = push(stack);

					if(ret == -1){
						printf("Stack overflow\n");
					}
				}
				break;

			case 2 :
				{
					int ret;
					ret = pop(stack);

					if(flag == 0)
						printf("Stack underflow\n");
					else
						printf("%d is poped\n",ret);

				}
				break;
			case 3 :
				{
					int ret;
					ret = peek(stack);
					if(flag == 0){
						printf("stack is empty\n");
					}else{
						printf("%d is peeked\n",ret);
					}
				}
				break;

			case 4 :
				{
					if(isFull())
						printf("Stack is full\n");
					else
						printf("Stack is not full\n");
				}
				break;

			case 5 :{
					if(isEmpty())
						printf("Stack is empty\n");
					else
						printf("Stack is not empty\n");
				}
				break;

			default :
				printf("Enter correct choice\n");

			}



		getchar();
		printf("Do you want to continue = ");
		scanf("%c",&choice);
	}while(choice == 'Y' || choice == 'y');
}
