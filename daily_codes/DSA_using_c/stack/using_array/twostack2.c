//push pop for two stack in one function.

#include<stdio.h>

int size = 0;
int top1 = 0;
int top2 = 0;
int flag1 = 0,flag2 = 0;

int push(int arr[],int num){

	if(top1 == top2 -1){
		return -1;
	}else{
		printf("Enter data = ");
		if(num == 1)
			scanf("%d",&arr[++top1]);
		else
			scanf("%d",&arr[--top2]);
		
		return 0;
	}
}

int pop(int *arr,int num){
	
	if(num == 1){
		if(top1 == -1){
			flag1 = 0;
			return -1;
		}else{
			flag1 = 1;
			int data = arr[top1];
			top1--;
			return data;
		}
	}else{
		if(top2 == size){
                	flag2 = 0;
               		return -1;
        	}else{
          	      flag2 = 1;
                	int data = arr[top2];
           	   	top2++;
                	return data;
       		}	

	}
}



void main(){

        printf("Enter size of stack = ");
        scanf("%d",&size);

     	int arr[size];

	top1 = -1;
	top2 = size;

        char choice;

        do{
                printf("Enter 1 for push1\n");
                printf("Enter 2 for push2\n");
                printf("Enter 3 for pop1\n");
                printf("Enter 4 for pop2\n");
                
                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int ret;
                                        ret = push(arr,1);

                                        if(ret == -1){
                                                printf("Stack1 overflow\n");
                                        }
                                }
                                break;

                        case 2 :
                                {
                                        int ret;
                                        ret = push(arr,2);

                                        if(ret == -1){
                                                printf("Stack2 overflow\n");
                                        }
                                }
                                break;

			case 3 :
				{

					int ret;
					ret = pop(arr,1);

					if(flag1 == 0)
						printf("Stack1 underflow\n");
                                        else
                                                printf("%d is poped\n",ret);

                                }
                                break;
			case 4 :
				{
					int ret;
					ret = pop(arr,2);

					if(flag2 == 0)
						printf("Stack2underflow\n");
                                        else
                                                printf("%d is poped\n",ret);
				}
				break;

  
			default :
                                printf("Enter correct choice\n");

                        }



                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);
        }while(choice == 'Y' || choice == 'y');
}
