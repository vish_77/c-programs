#include<stdio.h>

int BiSrch(int *arr,int key,int size){

	int start = 0;
	int end = size - 1;
	int mid;

	while(start <= end){

		if(start == end && arr[start] == key){

			return start;
		}else{


			mid = (start + end)/2;
	
			if(arr[mid] == key)
				return mid;

			if(arr[mid] < key)
				start = mid + 1;

			if(arr[mid] > key)
				end = mid - 1;
		}
	}
	return -1;
}

void main(){

	int arr[] = {1,3,6,7,9,11,15,22,40};

	int key;
	printf("Enter a element to search = \n");
	scanf("%d",&key);

	int size = sizeof(arr)/sizeof(int);

	int ret = BiSrch(arr,key,size);

	if(ret == -1)
		printf("Element not found\n");
	else
		printf("Element found at index = %d\n",ret);
}
