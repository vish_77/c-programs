#include<stdio.h>

int Interpolation(int *arr,int size,int key){

	int start = 0;
	int end = size - 1;

	int index = start + ((key-arr[start])/(arr[end]-arr[start])) * (end - start);
	
	return index;
}

void main(){

	int arr[] = {1,3,5,7,9,11,13};

	int ele;
	printf("Enter the element = ");
	scanf("%d",&ele);

	int ret = Interpolation(arr,sizeof(arr)/sizeof(int),ele);
	
	printf("Array element found at %d index\n",ret);
		
}
