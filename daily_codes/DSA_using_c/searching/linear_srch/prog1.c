#include<stdio.h>

//find the ele from the array & return index if not found return -1

int Presence(int *arr,int key,int size){

	for(int i = 0;i<size; i++){

		if(arr[i] == key)
			return i;

	}
	return -1;
}

//find the last occurance of the array element.

int LastOcc(int *arr,int key, int size){

	int last = -1;

	for(int i = 0; i<size; i++){

		if(arr[i] == key){
			last = i;
		}
	}
	return last;
}

//find the second last occurance of the array element.

int SecLastOcc(int *arr, int key, int size){

	int last = -1;
	int seclast = -1;

	for(int i = 0; i<size; i++){

		if(arr[i] == key){
			seclast = last;
			last = i;
		}
	}
	return seclast;
}

void main(){

	int arr[] = {1,2,3,4,5,6,3};

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);
	
	int size = sizeof(arr)/sizeof(int);
	
	int ret1 = Presence(arr,num,size);
	if(ret1 == -1)
		printf("Elemnt is not present\n");
	else
		printf("element is present at index = %d\n",ret1);

	
	int ret2 = LastOcc(arr,num,size);
	if(ret2 == -1)
		printf("Element is not present\n");
	else
		printf("last occurance of ele at index = %d\n",ret2);


	int ret3 = SecLastOcc(arr,num,size);
	if(ret3 == -1)
		printf("Element is not present\n");
	else
		printf("second last occurance of ele at index = %d\n",ret3);
	
}
