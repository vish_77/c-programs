
#include<stdio.h>
#include<stdlib.h>

struct BSTNode{

	int data;
	struct BSTNode* left;
	struct BSTNode* right;
};

void printBST(struct BSTNode* root){

	if(root == NULL){
		return;
	}

	printBST(root -> left);
	printf("%d > ",root->data);
	printBST(root -> right);
}


struct BSTNode* createBST(struct BSTNode* root, int data){

	if(root == NULL){

		struct BSTNode* newNode = malloc(sizeof(struct BSTNode));
		newNode -> data = data;
		newNode -> left = NULL;
		newNode -> right = NULL;
		
		root = newNode;
		return root;
	}

	if(data < root->data){

		root->left = createBST(root->left,data);

	}else if(data > root -> data){

		root->right = createBST(root -> right,data);
	}
	return root;
}

void main(){
	
	struct BSTNode* root = NULL;
	
	printf("Enter the number of nodes\n");
	int num;
	scanf("%d",&num);

	printf("Enter the data\n");

	for(int i=0; i<num; i++){
		
		int val = scanf("%d",&val);
		root = createBST(root,val);
		printf("%p\n",root);
	}

	printBST(root);
}

