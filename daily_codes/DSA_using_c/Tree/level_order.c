#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct Queue{

	struct TreeNode *btNode;
	struct Queue *next;

};

struct Queue *front = NULL;
struct Queue *rear = NULL;

bool isEmpty(){

	if(front == NULL && rear ==NULL)
		return true;
	else
		return false;
}

void enqueue(struct TreeNode *temp){

	struct Queue * newNode = malloc(sizeof(struct Queue));
	newNode -> btNode = temp;
	newNode -> next = NULL;

	if(isEmpty()){

		front = rear = newNode;
	}else{
		rear->next = newNode;
		rear = newNode;
	}
}

struct TreeNode *dequeue(){

	if(isEmpty()){
		printf("Tree is Empty\n");
	}else{
		struct Queue *temp = front;
		struct TreeNode *item = temp -> btNode;

		if(front == rear){
			front = rear = NULL;
		}else{
			front = front -> next;
		}

		free(temp);
		return item;
	}
}

void levelOrder(struct TreeNode *root){

	struct TreeNode * temp = root;
	enqueue(root);
	while(!isEmpty()){
		temp = dequeue();
		printf("%d\n",temp -> data);
		
		if(temp -> left != NULL){
			enqueue(temp -> left);
		}
		if(temp -> right != NULL){
			enqueue(temp -> right);
		}
	}
}


struct TreeNode* CreateNode(int level){
	
	level+=1;
	struct TreeNode *newnode= (struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter data = ");
	scanf("%d",&(newnode -> data));

	getchar();
	char ch;
        printf("Want to create left subtree for level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newnode->left = CreateNode(level);
        }else{
                newnode -> left = NULL;
        }

        getchar();
        printf("Want to create right subtree for level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newnode->right = CreateNode(level);
        }else{
                newnode -> right = NULL;
        }

	return newnode;
}

void main(){

	printf("**********************Binary tree************************\n");

	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	printf("Enter root node = ");
	scanf("%d",&(root -> data));
	printf("\t\t\t\tTree rooted with %d data\n",root->data);

	getchar();
	char ch;
	printf("Want to create left subtree for root node\n");
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->left = CreateNode(0);
	}else{
		root -> left = NULL;
	}
	
	getchar();
        printf("Want to create right subtree for root node\n");
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                root->right = CreateNode(0);
        }else{
                root -> right = NULL;
        }

	levelOrder(root);
}
