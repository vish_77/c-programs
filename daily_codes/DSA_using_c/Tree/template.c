#include<stdio.h>
#include<stdlib.h>

struct TreeNode{

	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};


void inorder(struct TreeNode *root){

	if(root == NULL)
		return;

	inorder(root->left);
	printf("%d  ",root -> data);
	inorder(root -> right);

}

void preorder(struct TreeNode *root){

	if(root == NULL)
		return;

	printf("%d  ",root -> data);
	preorder(root -> left);
	preorder(root -> right);
}

void postorder(struct TreeNode *root){

	if(root == NULL)
		return;

	postorder(root -> left);
	postorder(root -> right);
	printf("%d  ",root -> data);
}

void PrintTree(struct TreeNode *root){
	
	char ch;
	do{

		printf("For inorder press : 1\n");
		printf("For preorder press : 2\n");
		printf("For postorder press : 3\n");

		int num;
		printf("Enter = ");
		scanf("%d",&num);

		switch(num){
			case 1:
				inorder(root);
				printf("\n");
				break;

			case 2:
				preorder(root);
				printf("\n");
				break;

			case 3:
				postorder(root);
				printf("\n");
				break;

			default:
				printf("Invalid choice");

		}


	getchar();
	printf("Do you want to continue ?");
	scanf("%c",&ch);

	}while(ch == 'Y' || ch == 'y');
}


struct TreeNode* CreateNode(int level){
	
	level+=1;
	struct TreeNode *newnode= (struct TreeNode*)malloc(sizeof(struct TreeNode));
	printf("Enter data = ");
	scanf("%d",&(newnode -> data));

	getchar();
	char ch;
        printf("Want to create left subtree for level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newnode->left = CreateNode(level);
        }else{
                newnode -> left = NULL;
        }

        getchar();
        printf("Want to create right subtree for level %d\n",level);
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                newnode->right = CreateNode(level);
        }else{
                newnode -> right = NULL;
        }

	return newnode;
}

void main(){

	printf("**********************Binary tree************************\n");

	struct TreeNode *root = (struct TreeNode*)malloc(sizeof(struct TreeNode));

	printf("Enter root node = ");
	scanf("%d",&(root -> data));
	printf("\t\t\t\tTree rooted with %d data\n",root->data);

	getchar();
	char ch;
	printf("Want to create left subtree for root node\n");
	scanf("%c",&ch);

	if(ch == 'y' || ch == 'Y'){
		root->left = CreateNode(0);
	}else{
		root -> left = NULL;
	}
	
	getchar();
        printf("Want to create right subtree for root node\n");
        scanf("%c",&ch);

        if(ch == 'y' || ch == 'Y'){
                root->right = CreateNode(0);
        }else{
                root -> right = NULL;
        }

	PrintTree(root);
}																	
