//stack using linked list


#include<stdio.h>
#include<stdlib.h>

struct StackFrame{

	int data;
	struct StackFrame *next;
};

struct StackFrame *head = NULL;

struct StackFrame * CreateFrame(){

	struct StackFrame * frame = (struct StackFrame*)malloc(sizeof(struct StackFrame));
	printf("Enter data:");

	scanf("%d",&(frame -> data));
	frame -> next = NULL;

	return frame;
}

void push(){

	struct StackFrame *frame = CreateFrame();

	if(head == NULL)
		head = frame;
	else{

		struct StackFrame *temp = head;
		while(temp->next != NULL){
			temp = temp -> next;
		}

		temp -> next = frame;
	}
}

void pop(){
	if(head == NULL){
		printf("Stack is empty");
	}else{
		struct StackFrame * temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}

		temp = NULL;
	}
}


void display(){

	struct StackFrame *temp = head;

	while(temp != NULL){

		printf("%d\n",temp -> data);
		temp = temp -> next;
	}
}

void main(){

	char choice;
	do{
		printf("Enter 1 for push :\n");
		printf("Enter 2 for pop :\n");
		printf("Enter 3 for display :\n");

		int x = 0;
		scanf("%d",&x);

		switch(x){

			case 1:
				push();
				break;
			case 2:
				pop();
				break;
			case 3:
				display();
				break;
				
			default:
				printf("invalid ip");

		}

		getchar();
		printf("do you want to continue\n");

		scanf("%c",&choice);

	}while(choice == 'Y' || choice == 'y');
}

