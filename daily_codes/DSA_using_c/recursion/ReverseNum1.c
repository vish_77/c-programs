/*
	without using recursion.
	reverse the number give from user.
*/

#include<stdio.h>

int ReverseNum(int num){
	
	int temp = 0;

	while(num){

		temp = temp * 10 + num % 10;
		num = num / 10;
	}
	return temp;
}

void main(){
	
	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	printf("Reverse num = %d\n",ReverseNum(num));
}
