/*
  without recursion.
  count no. of steps
  eg. 	num = 7 ,is odd then num = num - 1; 7-1;	
	num = 6 ,is even, then num = num/2;  3;
	num = 3 ,is odd	, then 3-1 = 2;
	num = 2 ,is even, 2/2 = 1;
	num = 1, is odd , 1-1 = 0;

  there are fieve steps to reach 0;
  steps = 5.

*/

#include<stdio.h>

int StepCount(int num){
	
	int count = 0;

	while(num){

		if(num % 2 == 0){
			num = num / 2;
		}else{
			num = num - 1;
		}
		count ++;
	}
	return count;
}

void main(){

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	printf("steps = %d\n",StepCount(num));
}
