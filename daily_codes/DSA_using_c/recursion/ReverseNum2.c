/*
 	Using Recursion.
	Reverse the number given from user.

*/

#include<stdio.h>

int ReverseNum(int num){

	int static temp = 0;

	if(num == 0){
		return temp;
	}
	temp = temp * 10 + num % 10;
	return ReverseNum(num/10);
}

void main(){

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	printf("Reverse number = %d\n",ReverseNum(num));
}
