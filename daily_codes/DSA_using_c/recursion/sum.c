//sum of n natural number n >= 1 ip from user.

#include<stdio.h>

int sum(int n){

	if(n == 1)
		return 1;

	return sum(n-1) + n;
}

void main(){

	printf("Enter a value = ");
	int num;
	scanf("%d",&num);
	
	printf("%d\n",sum(num));
}

