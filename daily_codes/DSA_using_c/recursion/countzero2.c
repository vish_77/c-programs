//using recursion
//count number of zeros.

#include<stdio.h>

int CountZero(int num){

	int static count = 0;

	if(num == 0){
		return count;
	}

	if(num % 10 == 0){
		count ++;
	}

	CountZero(num / 10);
}

void main(){

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	if(num == 0)
		printf("Count of zeros = 1\n");
	else
		printf("Count of zeros = %d\n",CountZero(num));

}
