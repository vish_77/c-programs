//proper fibonachi series code using recursion.


#include<stdio.h>

int fibo(int N){
	
	if(N == 1)
		return 1;

	if(N == 0)
		return 0;
	
	return fibo(N - 1) + fibo(N - 2);
}

void main(){
	
	int num;
	printf("Enter a number for fibonachi series = ");
	scanf("%d",&num);

	int ret = fibo(num);

	printf("fibonachi = %d\n",ret);
}
