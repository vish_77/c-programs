// print number from 10 - 1.

#include<stdio.h>

int fun(int x){
	
	printf("%d\n",x);

	if(x == 1)
		return 1;
	
	fun(x - 1);
}

void main(){

	fun(10);
}
