//sum of elements of array


#include<stdio.h>

int arrsum(int *arr, int size){
	printf("%d\n",size);
	if(size == 0)
		return 0;

	return arrsum(arr + 1,--size) + arr[size];
}	

void main(){

	int arr[] = {1,2,3};

	printf("%d\n",arrsum(arr,sizeof(arr)/sizeof(int)));
}
