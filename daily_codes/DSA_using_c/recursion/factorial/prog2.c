#include<stdio.h>

int factnum(int x){

	static int fact = 1;

	fact = fact * x;

	if(x != 1)
		factnum(--x);

	return fact;
}

void main(){
	printf("%d\n",factnum(5));
}
