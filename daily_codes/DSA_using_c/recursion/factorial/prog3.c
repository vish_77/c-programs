#include<stdio.h>

int factorial1(int n){
	static int fact = 1;

	if(n != 0){
		fact = fact * n;
		factorial1(--n);
	}
	return fact;
}

int factorial2(int n){

	static int fact = 1;

	if(n == 1)
		return fact;

	fact = fact * n;
	return factorial2(--n);
}

void main(){

	printf("%d\n",factorial1(5));
	printf("%d\n",factorial2(5));
}
