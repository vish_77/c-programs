//print number from 10 to 1.
//without recursion & with recursion.

//1.without recursion.

#include<stdio.h>

void fun(int);

void main(){

	for(int i = 10; i>= 1; i--){
		printf("%d\n",i);
	}
	fun(10);
}

//2. with recursion.

void fun(int x){

	if(x > 0){
		printf("%d\n",x);
		fun(--x);
	}
}
		
