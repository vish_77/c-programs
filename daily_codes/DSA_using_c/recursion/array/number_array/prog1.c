// sum of array elements without recursion.


#include<stdio.h>

int sum(int arr[], int size){

	int sum = 0;

	for(int i = 0; i<size; i++){

		sum = sum + arr[i];
	}
	return sum;
}

void main(){
	
	int arr[] = {1,2,3,4,5};

	printf("sum of array ele = %d\n",sum(arr,sizeof(arr)/sizeof(int)));
}
