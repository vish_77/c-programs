/*
 * using recursion
 Check weather array is sorted or not.
 arr[] = {1,2,3,5,6,7,9} -> sorted.
 arr[] = {2,5,3,2,1} -> not sorted.

*/

#include<stdio.h>
#include<stdbool.h>

bool IsSorted(int * arr, int start, int size){

	if(start == size - 1)
		return true;
	
	return arr[start] <= arr[start + 1] && IsSorted(arr,start + 1,size);
}

void main(){

	int arr[] = {1,2,3,4,5,5};

	int ret = IsSorted(arr,0,sizeof(arr)/sizeof(int));

	if(ret == 1){
		printf("Array is sorted\n");
	}else{
		printf("Array is not sorted\n");
	}
}
