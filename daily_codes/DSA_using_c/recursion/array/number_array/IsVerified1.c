/*
        If two even numbers are occured in aray
        then return true else return false.
*/

#include<stdio.h>
#include<stdbool.h>

bool IsVerified(int *arr,int size){

        int cnt = 0;
        for(int i = 0; i<size; i++){

                if(arr[i] % 2 == 0)
                        cnt++;

                if(cnt == 2)
                        return true;
        }
        return false;
}

void main(){

	int arr[] = {1,3,44,12,1,5};

	int ret = IsVerified(arr,sizeof(arr)/sizeof(int));

	if(ret == 1)
		printf("Array is verified\n");
	else
		printf("Array is not verified\n");
}
