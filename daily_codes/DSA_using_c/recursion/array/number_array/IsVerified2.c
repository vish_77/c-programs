/*
	using recursion.
	if two even numbers occured in array then return true
	else return false.

*/

#include<stdio.h>
#include<stdbool.h>

bool IsVerified(int * arr,int size){

	if(size == 0){
		return false;
	}
	
	int static cnt = 0;

	if(arr[size - 1] % 2 == 0)
		cnt++;

	if(cnt == 2)
		return true;
	
	return IsVerified(arr,size -1);
}

void main(){

	int arr[] = {3,5,3,2};

	int ret = IsVerified(arr,sizeof(arr)/sizeof(int));
	
	if(ret == 1)
		printf("Array is Verified\n");
	else
		printf("Array is not verified\n");
}
