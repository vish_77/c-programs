//sum of array elements using recursion.

#include<stdio.h>

int sum(int arr[], int size){
	
	if(size == 1){
		return arr[size-1];
	}

	return sum(arr,size - 1) + arr[size - 1];
}

void main(){

	int arr[] = {1,2,3,4,5};

	printf("sum of array ele = %d\n",sum(arr,sizeof(arr)/sizeof(int)));
}
