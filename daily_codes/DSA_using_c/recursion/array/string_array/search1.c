//without recursion
//search character from array & return true or false.

#include<stdio.h>
#include<stdbool.h>

bool SearchEle(char *arr,int size,char ch){

	for(int i = 0; i < size; i++){

		if(arr[i] == ch)
			return true;
	}
	return false;
}

void main(){

	char carr[] = {'A','B','C','D','E'};

	int ret = SearchEle(carr,sizeof(carr)/sizeof(char),'A');

	if(ret == 1)
		printf("Element is found\n");
	else
		printf("Element is not found\n");
}
	
