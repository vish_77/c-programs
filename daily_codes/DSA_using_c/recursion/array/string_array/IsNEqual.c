/*
 * using recursion
 check weather two strings are equal or not up to N. 
 N is from user.

*/

#include<stdio.h>
#include<stdbool.h>

bool Ncompare(char *carr1, char *carr2,int start, int N){

	if(start == N)
		return true;

	return carr1[start] == carr2[start] && Ncompare(carr1,carr2,start + 1,N);
}
		
		
void main(){

	int N = 4;

	char carr1[] = "Vishsafl";
	char carr2[] = "Vishal";

	int ret = Ncompare(carr1,carr2,0,N);

	if(ret == 1){
		printf("strings are equal upto N\n");
	}else{
		printf("Strings are not equal upto N\n");
	}
}
