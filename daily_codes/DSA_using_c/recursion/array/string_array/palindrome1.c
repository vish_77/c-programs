//palindrome string normal code.

#include<stdio.h>
#include<stdbool.h>

bool IsPalindrome(char *arr,int size){

	char *ptr1 = arr;
	char *ptr2 = arr + size - 2;

	while(ptr1 < ptr2){

		if(*ptr1 != *ptr2)
			return false;

		ptr1++;
		ptr2--;
	}
	return true;
}

void main(){

	char arr[] = "madam";
	int ret = IsPalindrome(arr,sizeof(arr)/sizeof(char));

	if(ret == 1){
		printf("String is palindrome\n");
	}else{
		printf("String is not palindrome\n");
	}
}



