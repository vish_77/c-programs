/*
 there are two strings check weather the given strings are equal or not.
*/

#include<stdio.h>
#include<stdbool.h>
#include<string.h>

// whithout recursion
bool IsEqual1(char *carr1,char *carr2,int size){

	for(int i = 0; i<size; i++){
		
		if(carr1[i] != carr2[i]){
			return false;
		}
	}
	return true;
}

//using recursion

bool IsEqual2(char *carr1, char *carr2,int start,int size){
	
	if(start == size){
		return true;
	}

	return carr1[start] == carr2[start] && IsEqual2(carr1,carr2,start+1,size);
}


void main(){

	char carr1[] = "vishal";
	char carr2[] = "vishal";

	if(strlen(carr1) == strlen(carr2)){

		int ret = IsEqual1(carr1,carr2,strlen(carr1));
		if(ret == 1){
			printf("strings are equal\n");
		}else
			printf("Strings are not equal\n");
	}else
		printf("Strings are not equal\n");
	
	if(strlen(carr1) == strlen(carr2)){

                int ret = IsEqual2(carr1,carr2,0,strlen(carr1));
                if(ret == 1){
                        printf("strings are equal\n");
                }else
                        printf("Strings are not equal\n");
        }else
                printf("Strings are not equal\n");
}

