//palindrome string using recursion.

#include<stdio.h>
#include<stdbool.h>

bool IsPalindrome(char *arr,int start,int end){

	if(start >= end)
		return true;

	if(arr[start] != arr[end])
		return false;

	return IsPalindrome(arr,start+1,end-1);
		
}

void main(){

	char arr[] = "madam";
	int size = sizeof(arr)/sizeof(char);

	int ret = IsPalindrome(arr,0,size-2);

	if(ret == 1){
		printf("String is palindrome\n");
	}else{
		printf("String is not palindrome\n");
	}
}



