//whithout recursion
//count no. of zeros from the given integer 

#include<stdio.h>

int CountZero(int num){
	
	if(num == 0){
		return 1;
	}

	int count = 0;
	while(num){

		if(num % 10 == 0){
			count ++;
		}

		num = num / 10;
	}
	return count;
}

void main(){
	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	printf("count of 0 = %d\n",CountZero(num));
}
