#include<stdio.h>
#include<stdlib.h>

typedef struct Node {
	
	int data;
	struct Node *next;
}node;

node *head = NULL;
node *lastNode = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	lastNode = newnode;
	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
		newnode -> next = head;
	}else{
		node *temp = head;
		while(temp -> next != head){
			temp = temp -> next;
		}

		temp -> next = newnode;
		newnode -> next = head;
	}
}

void AddFirst(){

	node *newnode = CreateNode();
	if(head == NULL){
		head = newnode;
		newnode -> next = head;
	}else{
		newnode -> next = head;

		node *temp = head;
		while(temp -> next != head){
			temp = temp -> next;
		}

		head = newnode;
		temp -> next = head;
	}
}

void PrintLL(){

	node *temp = head;

	while(temp -> next != head){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|",temp -> data);
	printf("\n");
}

int NodeCount(){

	if(head == NULL){
		printf("list is empty\n");
		return -1;
	}else{
		node *temp = head;
		int count = 1;

		while(temp -> next != head){
			count ++;
			temp = temp -> next;
		}
	return count;
	}
}

int AddAtPos(int pos){

	int count = NodeCount();

	if(pos <= 0 || pos > count + 1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos == 1){
			AddFirst();
		}else if(pos == count + 1){
			AddNode();
		}else{

			node *newnode = CreateNode();
			node *temp = head;
			while(pos - 2){
				temp = temp -> next;
				pos --;
			}

			newnode -> next = temp -> next;
			temp -> next = newnode;
		}
		return 0;
	}

}
			


int DeleteFirst(){

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else if(head -> next == head){
		free(head);
		head = NULL;
	}else{
		node *temp = head;
		while(temp -> next != head){
			temp = temp -> next;
		}
		head = head -> next;
		free(temp -> next);
		temp -> next = head;
	}
	return 0;
}

void DeleteFirst1(){

	head = head -> next;
	free(lastNode -> next);
	lastNode -> next = head;
}

int DeleteLast(){

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else if(head -> next == head){
		free(head);
		head = NULL;
	}else{
		node *temp = head;

		while(temp -> next -> next != head){
			temp = temp -> next;
		}
		free(temp -> next);
		temp -> next = head;
	}
	return 0;
}

int DeleteAtPos(int pos){
	
	int count = NodeCount();

	if(pos <= 0 || pos > count){
		printf("invalid position\n");
		return -1;
	}else{
		if(pos == 1){
			DeleteFirst();
		}else if(pos == count){
			DeleteLast();
		}else{
			node *temp1 = head;

			while(pos - 2){
				temp1 = temp1 -> next;
				pos --;
			}

			node *temp2 = temp1 -> next;
			temp1 -> next = temp2 -> next;
			free(temp2);
		}
		return 0;
	}
}


void main(){

	char choice;

	do{

		printf("Enter 1 for AddNode\n");
		printf("Enter 2 for AddFirst\n");
		printf("Enter 3 for pint LL\n");
		printf("Enter 4 for Nodecount\n");
		printf("Enter 5 for AddAtPos()\n");
		printf("Enter 6 for DeleteFirst()\n");
		printf("Enter 7 for DeleteLast()\n");
		printf("Enter 8 for DeleteAtPos()\n");
		printf("Enter 9 for DeleteFirst1()\n");
				
		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){

			case 1 :
				{
					int node;
					printf("Enter no of nodes = ");
					scanf("%d",&node);
					for(int i = 1; i<= node; i++){
						AddNode();
					}
				}
				break;
			
			case 2 :
				AddFirst();
				break;

			case 3 :
				PrintLL();
				break;

			case 4 :
				if(NodeCount() != -1)
					printf("%d is node count\n",NodeCount());
	
				break;

			case 5 :
				{
					int pos;
					printf("Enter position = ");
					scanf("%d",&pos);

					AddAtPos(pos);
				}
				break;
			
			case 6 :
				DeleteFirst();
				break;

			case 7 :
				DeleteLast();
				break;

			case 8 :
				{
					int pos;
					printf("Enter position = ");
					scanf("%d",&pos);
					DeleteAtPos(pos);
				}
				break;
				
			case 9 :
				DeleteFirst1();
				break;
						
			default :
				printf("Enter correct choice\n");
		}

		getchar();
		printf("Do you want to continue = ");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
