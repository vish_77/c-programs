// print LL riverse.


#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;
	struct Demo *next;

}demo;

demo *head = NULL;

demo *CreateNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter a number = ");
	scanf("%d",&newnode->data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	demo *newnode = CreateNode();

	if(head == NULL)
		head = newnode;
	else{

		demo *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void AddLast(){
	AddNode();
}

void AddFirst(){

	demo *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode -> next = head;
		head = newnode;
	}
}

int NodeCount(){

        demo *temp = head;
        int count = 0;

        while(temp != NULL){
                count++;
                temp = temp -> next;
        }
        return count;
}


void AddAtPos(int pos){
	
	int count = NodeCount();
	if(pos <= 0 || pos >= count + 2){
	
		printf("Enter position between 1 to %d\n",count);
	
	}else if(pos == 1){
		AddFirst();
	}else if(pos == count){
		AddLast();
	}else{
		demo *newnode = CreateNode();

		demo *temp = head;

		while(pos - 2){
			temp = temp -> next;
			pos --;
		}

		newnode -> next = temp -> next;
		temp -> next = newnode;
	}
}

void PrintLL(){
	
	if(head == NULL){
		printf("list is empty\n");
	}else{
		demo *temp = head;

		while(temp != NULL){
	
			printf("%d || ",temp -> data);
			temp = temp -> next;
		}
		printf("\n");
	}
}

void DeleteFirst(){

	if(head == NULL){
		printf("list is empty\n");
	}else{
		demo *temp = head;

		head = head -> next;
		free(temp);
	}
}

void DeleteLast(){
	
	int count = NodeCount();
	
	if(count == 0)
		printf("list is empyt\n");
	

	else if(count == 1){
		DeleteFirst();

	}else{

		demo *temp = head;

		while(temp -> next -> next != NULL){
			temp = temp -> next;
		}
		
		free(temp -> next);
		temp -> next = NULL;
	}
}

void DeleteAtPos(int pos){

	int count = NodeCount();

	if(pos <= 0 || pos > count){
		printf("Enter valid position\n");
	}else if(pos == count){
		DeleteLast();
	}else if(pos == 1){
		DeleteFirst();
	}else{
		demo *temp = head;

		while(pos - 2){
			temp = temp -> next;
			pos --;
		}

		demo * temp2 = temp;

		temp -> next = temp -> next -> next;
		free(temp2 -> next);
	}
}

void Swap(){

	demo *temp1 = head;
	int x = NodeCount();

	int num = NodeCount()/2;
	int temp;
	
	while(num){

		demo *temp2 = head;

		int num2 = x;
		while(num2 - 1){

			temp2 = temp2 -> next;
			num2--;
		}
		temp = temp2 -> data;
		temp2 -> data = temp1 -> data;
		temp1 -> data = temp;

		temp1 = temp1 -> next;
		num--;
		x--;
	}
}	

void main(){

	char choice;

	do{
		printf("*************** mainu ***************\n");
		printf("Enter 1 for AddNode() = \n");
		printf("Enter 2 for AddFirst() = \n");
		printf("Enter 3 for AddLast() = \n");
		printf("Enter 4 for AddAtPos() = \n");
		printf("Enter 5 for PrintLL() = \n");
		printf("Enter 6 for NodeCount() = \n");
		printf("Enter 7 for DeleteFirst() = \n");
		printf("Enter 8 for DeleteLast() = \n");
		printf("Enter 9 for DeleteAtPos() = \n");
		printf("Enter 10 for swap LL\n");

		int ch;
		printf("Enter choice = ");
		scanf("%d",&ch);

		switch (ch){

			case 1 :
				{
					int node;
					printf("Enter no of nodes = ");
					scanf("%d",&node);
					for(int i = 1; i<=node; i++){
						AddNode();
					}
				}
				break;
			
			case 2 :
				AddFirst();
				break;

			case 3 :
				AddLast();
				break;

			case 4 :
				{
					int pos;
					printf("Enter position you want to add node = ");
					scanf("%d",&pos);
					AddAtPos(pos);
				}
					break;

			case 5 : 
				PrintLL();
				break;

			case 6 :
				printf("node count = %d \n",NodeCount());
				break;

			case 7 :
				DeleteFirst();
				break;

			case 8 :
				DeleteLast();
				break;
			
			case 9 :{
					int pos;
					printf("Enter position = ");
					scanf("%d",&pos);
					DeleteAtPos(pos);
					
				}
				break;

			case 10 :
				 Swap();
				 break;

			default :
				printf("Enter correct choice = \n");
		}

		getchar();
		printf("Do you want to continu ? = ");
	    	scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');
}

		
