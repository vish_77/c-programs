// swap DLL

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	newnode -> prev = NULL;
	
	printf("Enter data = ");
	scanf("%d",&newnode->data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
		newnode -> prev = temp;
	}
}

void AddFirst(){
	
	node *newnode = CreateNode();

	if(head == NULL)
		head = newnode;	
	
	else{
		newnode -> next = head;
		head -> prev = newnode;
		head = newnode;
	}
}

int NodeCount(){

	node *temp = head;
	int count = 0;

	while(temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

void AddLast(){
	
	AddNode();
}

int AddAtPos(int pos){

	int count = NodeCount();
	
	if(pos <= 0 || pos >= count + 2){
		
		printf("Invalid input\n");
		return -1;

	}else if(pos == 1){
		AddFirst();
	}else if(pos == count+1){
		AddLast();
	}else{
		
		node *newnode = CreateNode();
		node *temp = head;

		while(pos - 2){

			temp = temp -> next;
			pos --;
		}
		newnode -> next = temp -> next;
		newnode -> prev = temp;
		newnode -> next -> prev = newnode;
		temp -> next = newnode;
		
	}
	return 0;
}

void PrintLL(){

	if(head == NULL){
		printf("List is empty\n");
	}else{
		node *temp = head;

		while(temp -> next != NULL){
			printf("|%d| -> ",temp -> data);
			temp = temp -> next;
		}
		printf("|%d|",temp -> data);
		printf("\n");
	}
}

int DeleteFirst(){

	int count = NodeCount();

	if(head == NULL){
		printf("list is empty\n");
		return -1;
	}else if(count == 1){
			
		free(head);
		head = NULL;
		
	}else{
		head = head -> next;
		free(head -> prev);
		head -> prev = NULL;
	}
	return 0;
}

void DeleteLast(){

	int count = NodeCount();

	if(head == NULL){
		printf("list is empty\n");
	}else if(count == 1){
		DeleteFirst();
	}else{
		node * temp = head;
		
		while(temp -> next -> next != NULL){
			temp = temp -> next;
		}
		free(temp -> next);
		temp -> next = NULL;
	}
}

int DeleteAtPos(int pos){

	int count = NodeCount();

	if(pos <= 0 || pos > count){
	
		printf("Invalid position\n");
		return -1;
	
	}else if(pos == count){
		DeleteLast();
	}else if(pos == 1){
		DeleteFirst();
	}else{
		node *temp = head;

		while(pos - 2){
			temp = temp -> next;
			pos --;
		}
		temp -> next = temp -> next -> next;
		free(temp -> next -> prev);
		temp -> next -> prev = temp;
	}
	return 0;
}

void reverseDLL(){

	node *temp1 = head;
	node *temp2 = head;

	while(temp1 -> next != NULL){
		temp1 = temp1 -> next;
	}

	int loop = NodeCount()/2;
	int temp;
	while(loop){
		temp = temp1 -> data;
		temp1 -> data = temp2 -> data;
		temp2 -> data = temp;

		temp1 = temp1 -> prev;
		temp2 = temp2 -> next;
		loop--;
	}
}

void main(){	

	char choice;

	do{
		printf("Enter 1 for AddNode()\n");
		printf("Enter 2 for AddFirst()\n");
		printf("Enter 3 for AddLast()\n");
		printf("Enter 4 for AddAtPos()\n");
		printf("Enter 5 for PrintLL()\n");
		printf("Enter 6 for NodeCount()\n");
		printf("Enter 7 for DeleteFirst()\n");
		printf("Enter 8 for DeleteLast()\n");
		printf("Enter 9 for DeleteAtPos()\n");
		printf("Enter 10 for swap dll\n");

		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){

			case 1 :
				{
					int node;
					printf("Enter no of nodes = \n");
					scanf("%d",&node);

					for(int i = 1; i<= node; i++){
						AddNode();
					}
				}
				break;

			case 2 :
				AddFirst();
				break;

			case 3 :
				AddLast();
				break;

			case 4 :
				{
					int pos;
					printf("Enter position = ");
					scanf("%d",&pos);
					AddAtPos(pos);
				}
				break;

			case 5 :
				PrintLL();
				break;

			case 6 :
				printf("Node count = %d\n",NodeCount());
				break;

			case 7 :
				DeleteFirst();
				break;

			case 8 :
				DeleteLast();
				break;

			case 9 : 
				{
					int pos;
					printf("Enter position = ");
					scanf("%d",&pos);
					DeleteAtPos(pos);
				}
				break;
			case 10:
				reverseDLL();
				break;
		
			default :
				printf("Enter corrct choice\n");

		}
			
		getchar();
		printf("Do you want to continue = ");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');
}
