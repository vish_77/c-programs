//Writing head inside the main func. 

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

void NewNode(node **head){

	node *newnode = (node*)malloc(sizeof(node));
	newnode -> data = 1;
	newnode -> next = NULL;
	*head = newnode;
}

void PrintLL(node *head){

	node *temp = head;

	while(temp != NULL){
		printf("%d\n",temp -> data);
		temp = temp -> next;
	}
}

void main(){

	node *head = NULL;
	NewNode(&head);
	PrintLL(head);
}

