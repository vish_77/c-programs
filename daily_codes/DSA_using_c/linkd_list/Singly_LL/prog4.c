/* by writing this prog printLL func will not print any node value because
   main head = NULL will copy in addNode head = NULL 
   but addNode head will not be reflect into main head because of local scope both head are different.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct student{

	int id;
	float ht;
	struct student *next;
}stud;

void addNode(stud * head){

	stud *newnode = (stud*)malloc(sizeof(stud));
	newnode -> id = 1;
	newnode -> ht = 5.6;
	newnode -> next = NULL;

	head = newnode;
}

void printLL(stud* head){

	stud * temp = head;

	while(temp != NULL){
		printf("%d\n",temp -> id);
		printf("%f\n",temp -> ht);
		temp = temp -> next;
	}
}

void main(){
	
	stud * head = NULL;
	addNode(head);
	printLL(head);
}
