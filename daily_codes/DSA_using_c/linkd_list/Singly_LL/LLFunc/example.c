#include<stdio.h>
#include<stdlib.h>

typedef struct Villages{

	char Vname[20];
	long population;
	float area;
	struct Villages *next;
}vlg;

vlg *head = NULL;

vlg *CreateNode(){

	vlg *newnode = (vlg*)malloc(sizeof(vlg));
	
	getchar();
	printf("Enter Name of village = ");

	char ch;
	int i = 0;
	while((ch = getchar()) != '\n'){

		(*newnode).Vname[i] = ch;
		i++;
	}

	printf("Enter population = ");
	scanf("%ld",&newnode -> population);

	printf("Enter area = ");
	scanf("%f",&newnode -> area);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	vlg *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{

		vlg *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void AddFirst(){

	vlg *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode -> next = head;
		head = newnode;
	}
}

void AddLast(){

	AddNode();
}

int NodeCount(){

	vlg *temp = head;
	int count = 0;

	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

int AddAtPos(int pos){

	int count = NodeCount();
	
	if(pos <= 0 || pos >= count+2){
		printf("Invlid position \n");
		return -1;
	}else if(pos == 1){
		AddFirst();
	}else if(pos == count+1){
		AddLast();
	}else{

		vlg *newnode = CreateNode();
		vlg *temp = head;

		while(pos - 2){
			temp = temp -> next;
			pos --;
		}
		newnode -> next = temp -> next;
		temp -> next = newnode;
	}
	return 0;
}

void PrintLL(){

	if(head == NULL){
		printf("List is empty\n");
	}else{
		vlg *temp = head;

		while(temp != NULL){
			printf("|| %s |",temp -> Vname);
			printf(" %ld |",temp -> population);
			printf(" %f ||",temp -> area);
			temp = temp -> next;
			printf("\n");
		}
		printf("\n");
	}
}

void DeleteFirst(){
	
	int count = NodeCount();

	if(head == NULL){
		printf("list is empty\n");
	}else if(count == 1){
		free(head);
		head = NULL;
	}else{
		vlg *temp = head;
		
		head = head -> next;
		free(temp);
	}
}

void DeleteLast(){
	
	int count = NodeCount();

	if(count == 1){
		DeleteFirst();
	}else{
		vlg *temp = head;

		while(temp -> next -> next != NULL){
	
			temp = temp -> next;
		}
		free(temp -> next);
		temp -> next = NULL;
	}
}

int DeleteAtPos(int pos){

	int count = NodeCount();

	if(pos <= 0 || pos > count){
		
		printf("Invlid position\n");
		return -1;

	}else if(pos == 1){
		DeleteFirst();
	}else if(pos == count){
		DeleteLast();
	}else{
		vlg *temp1 = head;
		vlg *temp2 = head;

		while(pos - 2){

			temp1 = temp1 -> next;
			pos --;
		}

		temp2 = temp1 -> next;
		temp1 -> next = temp2 -> next;
		free(temp2);
	}
	return 0;
}

void main(){

	char choice;
	do{
                printf("*************** mainu ***************\n");
                printf("Enter 1 for AddNode() = \n");
                printf("Enter 2 for AddFirst() = \n");
                printf("Enter 3 for AddLast() = \n");
                printf("Enter 4 for AddAtPos() = \n");
                printf("Enter 5 for PrintLL() = \n");
                printf("Enter 6 for NodeCount() = \n");
                printf("Enter 7 for DeleteFirst() = \n");
                printf("Enter 8 for DeleteLast() = \n");
                printf("Enter 9 for DeleteAtPos() = \n");

                int ch;
                printf("Enter choice = ");
                scanf("%d",&ch);

                switch (ch){

                        case 1 :
				{
					int node;
					printf("Enter no of nodes = ");
					scanf("%d",&node);
					for(int i = 1; i<= node; i++){
						AddNode();
					}
				}
                                break;

                        case 2 :
                                AddFirst();
                                break;

                        case 3 :
                                AddLast();
                                break;

                        case 4 :
                                {
                                        int pos;
                                        printf("Enter position you want to add node = ");
                                        scanf("%d",&pos);
                                        AddAtPos(pos);
                                }
                                        break;

                        case 5 :
                                PrintLL();
                                break;

                        case 6 :
                                printf("node count = %d \n",NodeCount());
                                break;

                        case 7 :
                                DeleteFirst();
                                break;

                        case 8 :
                                DeleteLast();
                                break;

                        case 9 :{
                                        int pos;
                                        printf("Enter position = ");
                                        scanf("%d",&pos);
                                        DeleteAtPos(pos);

                                }
                                break;

                        default :
                                printf("Enter correct choice = \n");
                }

                getchar();
                printf("Do you want to continu ? = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}


