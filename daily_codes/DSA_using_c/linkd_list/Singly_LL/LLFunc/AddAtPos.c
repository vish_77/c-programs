#include<stdio.h>
#include<stdlib.h>

typedef struct employee{

	char name[20];
	int ID;
	struct employee *next;
}emp;

emp *head = NULL;

emp *CreatNode(){

	emp *newnode = (emp*)malloc(sizeof(emp));

	getchar();

	printf("Enter employee name = ");

	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){

		(*newnode).name[i] = ch;
		i++;
	}

	printf("Enter id of employee = ");
	scanf("%d",&newnode -> ID);

	newnode -> next = NULL;
	
	return newnode;
}

void AddNode(){

	emp *newnode = CreatNode();

	if(head == NULL)
		head = newnode;
	else{
		emp *temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

int NodeCount(){

	emp *temp = head;
	int count=0;

	while(temp != NULL){
	
		count++;
		temp = temp -> next;
	}

	return count;
}

void PrintLL(){

	emp *temp = head;

	while(temp != NULL){

		printf("%s > ",temp -> name);
		printf("%d | ",temp -> ID);
		
		temp = temp -> next;
	}
	printf("\n");
}

void AddAtPos(int pos){
	
	emp *newnode = CreatNode();
	emp *temp = head;
	
	if(temp == NULL){
		temp = newnode;
	}else if(pos >= 2 && pos <= NodeCount()){

		while(pos - 2){
			temp = temp -> next;
			pos --;
		}

		newnode -> next = temp -> next;
		temp -> next = newnode;
	}
	else{
		printf("Enter position in between 2 to %d\n",NodeCount());
	}	

}

void main(){
	
	int node;
	printf("Enter number of nodes = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode();
	}

	PrintLL();
	
	int pos;

	printf("Enter a position where you want to insert a node = ");
	scanf("%d",&pos);

	AddAtPos(pos);
	PrintLL();
}	
