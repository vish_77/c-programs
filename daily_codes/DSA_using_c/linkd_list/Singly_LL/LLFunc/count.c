
//print count of nodes.

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{

	char name[20];
	int Id;
	struct Employee *next;
}emp;

emp *head = NULL;

emp *CreateNode(){

	emp *newnode = (emp*)malloc(sizeof(emp));

	getchar();

	printf("Enter employee name = ");
	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
		(*newnode).name[i] = ch;
		i++;
	}

	printf("Enter id of employee = ");
	scanf("%d",&newnode -> Id);

	newnode -> next = NULL;
	
	return newnode;
}

void AddNode(){

	emp *newnode = CreateNode();

	if(head == NULL)
		head = newnode;
	else{
		emp *temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

int NodeCount(){

	emp *temp = head;
	int count = 0;

	while(temp != NULL){
		count++;
		temp = temp -> next;
	}
	
	return count;
}

void main(){
	
	int node;
	printf("Enter no of nodes = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode();
	}

	printf("count of node = %d\n",NodeCount());

}
