#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data; 
	struct Demo *next;
}demo;

demo *head = NULL;

demo *CreateNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	demo *newnode = CreateNode();

	if(head == NULL)
		head = newnode;
	else{
		demo *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}

		temp -> next = newnode;
	}
}

int NodeCount(){

	demo *temp = head;
	int count = 0;

	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}

	return count;
}

void AddFirst(){
	
	demo *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{

		newnode -> next = head;
		head = newnode;
	}
}

void AddLast(){
	AddNode();
}

void AddAtPos(int pos){

	if(pos >= 1 && pos <= NodeCount()+1){

		if(pos == 1){
			AddFirst();
		}else{
			demo *newnode = CreateNode();
			demo *temp = head;

			while(pos - 2){

				temp = temp -> next;
				pos --;
			}

			newnode -> next = temp -> next;
			temp -> next = newnode;
		}
	}else{
		printf("Please enter position between 1 to %d\n",NodeCount()+1);
	}
}

void PrintLL(){

	demo *temp = head;

	while(temp != NULL){

		printf("%d ||  ",temp -> data);
		temp = temp -> next;
	}
	printf("\n");
}

void DeleteFirst(){

	demo *temp = head;
	head = temp -> next;	//head = head -> next;
	free(temp);
}

void DeleteLast(){

	if(NodeCount() == 1){
		free(head);
		head = NULL;
	}else{

		demo *temp = head;
	
		while(temp -> next -> next != NULL){
			temp = temp -> next;
		}

		free(temp -> next);
		temp -> next = NULL;
	}
}

void DeleteAtPos(int pos){

	if(pos >= 1 && pos <= NodeCount()){

		if(pos == 1){
			DeleteFirst();
		}else{
			demo *temp = head;

			while(pos - 2){

				temp = temp -> next;
				pos --;
			}
			demo *temp2 = temp -> next;
			temp -> next = temp -> next -> next;
			free(temp2);
		}

	}else{
		printf("Enter appropriate position\n");
	}
}
	

void main(){
	
	char choice;

	do{
		printf("Enter 1 for AddNode()\n");
		printf("Enter 2 for AddFirst()\n");
		printf("Enter 3 for AddLast()\n");
		printf("Enter 4 for AddAtpos()\n");
		printf("Enter 5 for PrintLL()\n");
		printf("Enter 6 for NodeCount()\n");
		printf("Enter 7 for DeleteFirst()\n");
		printf("Enter 8 for DeleteLast()\n");
		printf("Enter 9 for DeleteAtPosition()\n");
		
		
		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);

		switch(ch){
			
			case 1:
				AddNode();
				break;
			case 2:
				AddFirst();
				break;
			case 3:
				AddLast();
				break;
			case 4:
				{
					int pos;
					printf("Enter position you want to insert = ");
					scanf("%d",&pos);

					AddAtPos(pos);
				}
				break;
			case 5:
				PrintLL();
				break;

			case 6:
				printf("Node count = %d\n",NodeCount());
				break;

			case 7:
				DeleteFirst();
				break;

			case 8:
				DeleteLast();
				break;

			case 9:
				{
					int pos;
					printf("Enter position you want to delete = ");
					scanf("%d",&pos);

					DeleteAtPos(pos);
				}
				break;

			default : 

				printf("Enter proper choice \n");
		}

		getchar();		
		
		printf("Do you want to continue = ");
		scanf("%c",&choice);
			
	}while(choice == 'y' || choice == 'Y');
}
