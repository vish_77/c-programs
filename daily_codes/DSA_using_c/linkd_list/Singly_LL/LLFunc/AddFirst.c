#include<stdio.h>
#include<stdlib.h>

typedef struct employee{

	char name[20];
	int ID;
	struct employee *next;
}emp;

emp *head = NULL;

emp *CreatNode(){

	emp *newnode = (emp*)malloc(sizeof(emp));

	getchar();

	printf("Enter employee name = ");

	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){

		(*newnode).name[i] = ch;
		i++;
	}

	printf("Enter id of employee = ");
	scanf("%d",&newnode -> ID);

	newnode -> next = NULL;
	
	return newnode;
}

void AddNode(){

	emp *newnode = CreatNode();

	if(head == NULL)
		head = newnode;
	else{
		emp *temp = head;

		while( temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	emp *temp = head;

	while(temp != NULL){

		printf("%s > ",temp -> name);
		printf("%d | ",temp -> ID);
		
		temp = temp -> next;
	}
	printf("\n");
}

void AddFirst(){

	emp *newnode = CreatNode();

	if(head == NULL)
		head = newnode;
	else{
		newnode -> next = head;
		head = newnode;
	}
}

void main(){
	
	int node;
	printf("Enter number of nodes = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode();
	}

	PrintLL();

	AddFirst();
	PrintLL();
}	
