#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

void main(){
	
	node *head = NULL;
	
	//creating 1st node.
	node *newnode = (node*)malloc(sizeof(node));
	newnode -> data = 10;
	newnode -> next = NULL;
	
	//connecting first node to head.
	head = newnode;
	
	//creating 2nd node.
	newnode = (node*)malloc(sizeof(node));
	newnode -> data = 20;
	newnode -> next = NULL;

	//connecting second node.
	head -> next = newnode;
	
	//creating 3rd node.
	newnode = (node*)malloc(sizeof(node));
	newnode -> data = 30;
	newnode -> next = NULL;

	//connecting third node.
	head -> next -> next = newnode;
	
	//creating 4th node
	newnode = (node*)malloc(sizeof(node));
	newnode -> data = 40;
	newnode -> next = NULL;

	//connecting fourth node.
	head -> next -> next -> next = newnode;
		

	printf("%d\t", head -> data);
	printf("%d\t", head -> next -> data);
	printf("%d\t", head -> next -> next -> data);
	printf("%d\t", head -> next -> next -> next -> data);
	printf("\n");

	//accessing node data by using loop
	
	node *temp = head;
	while(temp  != NULL){
		printf("%d\n",temp -> data);
		temp = temp -> next;
	}
}
