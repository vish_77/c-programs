#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Movie{

	char mName[20];
	float imdb;
	struct Movie *next;
}mov;

mov *head = NULL;

void nextNode(){

	char ch;
	mov *newnode = (mov*)malloc(sizeof(mov));

	printf("Enter movie name = ");
//	fgets(newnode -> mName,15,stdin);		
	scanf("%[^\n]",newnode -> mName);

	printf("Enter imdb rating = ");
	scanf("%f",&newnode->imdb);

	getchar();
	
	newnode -> next = NULL;
	
	if(head == NULL){
		head = newnode;
	}else{
		mov *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}

		temp -> next = newnode;
	}
}

void PrintLL(){

	mov *temp = head;
	
	while(temp != NULL){
		printf("%s > ",temp -> mName);
		printf("%f || ",temp -> imdb);
		
		temp = temp -> next;
	}
	printf("\n");
}


void main(){
	nextNode();
	nextNode();
	nextNode();

	PrintLL();
}
