#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct hostel{

	char hname[20];
	int roomNo;
	float rent;
	struct hostel *next;
}hostel;

hostel *head = NULL;

void AddNode(){

	hostel* newnode = (hostel*)malloc(sizeof(hostel));
	strcpy(newnode -> hname,"Balaji hostel");
	newnode -> roomNo = 122;
	newnode -> rent = 2500;
	newnode -> next = NULL;
	head = newnode;
}

void PrintLL(){

	hostel *temp = head;

	while (temp != NULL){
		printf("%s\n",temp -> hname);
		printf("%d\n",temp -> roomNo);
		printf("%f\n",temp -> rent);
		temp = temp -> next;
	}
}

void main(){

	AddNode();
	AddNode();
	PrintLL();
}
