//Writing head globaly -> this is the proper way to write linked list.

#include<stdio.h>
#include<stdlib.h>

typedef struct student{

	int Id;
	float ht;
	struct student *next;
}stud;

stud *head = NULL;

void Node(){
	stud *newnode = (stud*)malloc(sizeof(stud));
	newnode -> Id = 1;
	newnode -> ht = 5.5;
	newnode -> next = NULL;
	head = newnode;
}

void PrintLL(){

	stud *temp = head;

	while(temp != NULL){

		printf("%d\n",temp -> Id);
		printf("%f\n",temp -> ht);
		temp = temp -> next;
	}
}

void main(){

	Node();
	PrintLL();
}


	
