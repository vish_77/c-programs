#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

        int data;
        struct Demo *next;

}demo;

demo *head = NULL;

demo *CreateNode(){

        demo *newnode = (demo*)malloc(sizeof(demo));

        printf("Enter a number = ");
        scanf("%d",&newnode->data);

        newnode -> next = NULL;

        return newnode;
}

void AddNode(){

        demo *newnode = CreateNode();

        if(head == NULL)
                head = newnode;
        else{

                demo *temp = head;

                while(temp -> next != NULL){
                        temp = temp -> next;
                }
                temp -> next = newnode;
        }
}

int NodeCount(){

	demo *temp = head;
	int count = 0;

	while(temp != NULL){
		count++;
		temp = temp -> next;
	}
	return count;
}

int PrintLL(){

	demo *temp = head;

	while(temp != NULL){
		printf("%d  ||  ",temp -> data);
		temp = temp -> next;
	}
	printf("\n");
}


void SecLastOcc(int num){
	
	demo *temp = head;

	int pos=0;
	int flag = 0;
	int next = 0;
	int prev = 0;

	while(temp != NULL){

		pos ++;

		if(num == temp -> data){
	
			flag ++;
			prev = next;
			next = pos;
		}
		
		temp = temp -> next;
	}

	if(flag == 1){
		printf("number is present only once at position = %d\n",next);
	}else if (flag == 0){
		printf("Number not present \n");
	}else{
		printf("second last occurrence position = %d\n",prev);
	}
}
	

void main(){
	
	char choice;

	do{	
		printf("Enter 1 for AddNode()\n");
		printf("Enter 2 for PrintLL()\n");
		printf("Enter 3 for NodeCount()\n");
		printf("Enter 4 for SecLastOcc()\n");
		
		int ch;
		printf("Enter your choice = ");
		scanf("%d",&ch);


		switch(ch){

			case 1 :
				AddNode();
				break;

			case 2 :
				PrintLL();
				break;

			case 3 :
				printf("node count = %d\n",NodeCount());
				break;

			case 4 :
				{
					int num;
					printf("Enter a number = ");
					scanf("%d",&num);
					SecLastOcc(num);
				}
				break;
			

			default :
				printf("Enter correct choice\n");
		}

		getchar();
		printf("Do you want to continue = ");
		scanf("%c",&choice);

	}while(choice == 'y' || choice == 'Y');	
	
}
	
