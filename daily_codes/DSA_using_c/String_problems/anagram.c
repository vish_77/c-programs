

#include<stdio.h>
#include<string.h>

int IsAnagram(char *ptr1, char *ptr2){

	int arr1[125];
	int arr2[125];

	for(int i = 0; i<125; i++){
		arr1[i] = 0;
		arr2[i] = 0;
	}

	while(*ptr1 != '\0'){

		arr1[*ptr1]++;
		*ptr1++;
	}

	while(*ptr2 != '\0'){

		arr2[*ptr2]++;
		*ptr2++;
	}

	for(int i = 0; i<125; i++){

		if(arr1[i] != arr2[i]){
			return -1;
		}
	}
	return 0;
}

void main(){

	char str1[] = "abaa";
	char str2[] = "aaab";

	
	if(strlen(str1) == strlen(str2)){

		int ret = IsAnagram(str1,str2);

		if(ret == 0){
			printf("Is anagram\n");
		}else
			printf("Not anagram\n");
	}else
		printf("Not anagram\n");


}
