#include<stdio.h>

void strrev(char *ptr1){

	char *ptr2 = ptr1;
	
	while(*ptr2 != '\0'){

		ptr2++;
	}
	ptr2--;

	while(ptr1 < ptr2){

		char temp = *ptr1;
		*ptr1 = *ptr2;
		*ptr2 = temp;

		ptr1++;
		ptr2--;
	}
}

void main(){

	char str[] = {"Vishal"};

	puts(str);

	strrev(str);

	puts(str);

}
