//Tower of hanoie

#include<stdio.h>

void TOH(int N,char from, char via, char to){

	if(N > 0){
		
		TOH(N-1,from,to,via);
		printf("%c --> %c\n",from,to);
		TOH(N-1,via,from,to);
	}
}

void main(){
	
	int ring;
	printf("Enter number of rings = ");
	scanf("%d",&ring);
	TOH(ring,'A','B','C');
}


