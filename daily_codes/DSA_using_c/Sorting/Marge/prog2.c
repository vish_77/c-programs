#include<stdio.h>

void Marge(int arr[], int start, int mid, int end){

        int ele1 = mid - start + 1;
        int ele2 = end - mid;

        int arr1[ele1],arr2[ele2];

        for(int i = 0; i<ele1; i++){
                arr1[i] = arr[start + i];
        }
        for(int i = 0; i<ele2; i++){
                arr2[i] = arr[mid + 1 + i];
        }

        int itr1 = 0;
        int itr2 = 0;
        int itr3 = start;

        while(itr1 < ele1 && itr2 < ele2){
                if(arr1[ele1] < arr2[ele2]){
                        arr[itr3] = arr1[itr1];
                        itr1++;
                }else{
                        arr[itr3] = arr2[itr2];
                        itr2++;
                }
                itr3++;
        }
        while(itr1<ele1){
                arr[itr3] = arr1[itr1];
                itr1++;
                itr3++;
        }
        while(itr2<ele2){
                arr[itr3] = arr2[itr2];
                itr2++;
                itr3++;
        }
}


void MargeSort(int arr[], int start,int end){

	if(start < end){
		
		int mid = (start + end)/2;
		MargeSort(arr,start,mid);
		MargeSort(arr,mid+1,end);
		Marge(arr,start,mid,end);
	}
}

void main(){

	int arr[] = {7,-2,8,3,2,4,6,9,1};

	int size = sizeof(arr)/sizeof(arr[0]);
	int start = 0;
	int end = size -1;
	
	for(int i = 0; i<size; i++){
                printf("%d   ",arr[i]);
        }
        printf("\n");
	
	MargeSort(arr,start,end);

	for(int i = 0; i<size; i++){
		printf("%d   ",arr[i]);
	}
	printf("\n");
}
