//Naive approach

#include<stdio.h>

int Partation(int arr[],int start, int end){

	int temp[end - start + 1];
	int index = 0;
	int pivot = arr[end];

	for(int i = start; i<end; i++){
		if(arr[i] < pivot){
			temp[index++] = arr[i];
		}
	}

	int pos = index + start;
	temp[index++] = pivot;

	for(int i = start; i<end; i++){
		if(arr[i] > pivot){
			temp[index++] = arr[i];
		}
	}

	for(int i = start; i<= end; i++){
		arr[i] = temp[i-start];
	}
	return pos;

}

void QuickSort(int arr[], int start, int end){

	if(start<end){

		int pivot = Partation(arr,start,end);
		QuickSort(arr,start,pivot - 1);
		QuickSort(arr,pivot+1,end);
	}
}

void main(){

	int arr[] = {8,2,1,-3,6,7};

	int start = 0;
	int size = sizeof(arr)/sizeof(int);

	QuickSort(arr,start,size - 1);

	for(int i = 0; i<size; i++){
		printf("%d   ",arr[i]);
	}
	printf("\n");
}
