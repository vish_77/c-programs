//Hour's partition approach.


#include<stdio.h>

int Partation(int arr[], int start, int end){

	int i = start -1;
	int j = end + 1;
	int pivot = arr[start];

	while(1){

		do{
			i++;
		}while(arr[i] < pivot);

		do{
			j--;
		}while(arr[j] > pivot);

		if(i>=j)
			return j;

		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}

void QuickSort(int arr[], int start, int end){

	if(start<end){
		int pivot = Partation(arr,start,end);
		QuickSort(arr,start,pivot);
		QuickSort(arr,pivot+1,end);
	}
}

void main(){

	int arr[] = {3,0,2,1,-8,5};

	int start = 0;
	int size = sizeof(arr)/sizeof(int);

	QuickSort(arr,start,size-1);

	for(int i = 0; i<size; i++){
		printf("%d   ",arr[i]);
	}
	
}
