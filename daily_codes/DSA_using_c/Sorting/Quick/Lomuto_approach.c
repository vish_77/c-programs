//Lomuto approach 

#include<stdio.h>

void swap(int *ptr1,int *ptr2){

	int temp = *ptr1;
	*ptr1 = *ptr2;
	*ptr2 = temp;
}

int Pivot(int arr[],int start,int end){
	int pivot = arr[end];
	int itr = start -1;

	for(int i = start; i<end; i++){
		if(arr[i] < pivot){
			itr++;
			swap(&arr[i],&arr[itr]);
		}
	}
	swap(&arr[itr+1],&arr[end]);
	return (itr+1);
}

void QuickSort(int arr[],int start,int end){

	if(start < end){

		int pivot = Pivot(arr,start,end);
		QuickSort(arr,start,pivot-1);
		QuickSort(arr,pivot+1,end);
	}
	
}

void main(){

	int arr[] = {2,9,-2,8};
	int size = sizeof(arr)/sizeof(int);
	printf("%d\n",size);
	QuickSort(arr,0,size-1);
	
	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
}
