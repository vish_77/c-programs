//put the minimum value of array to first index.

#include<stdio.h>


void main(){

	int arr[] = {2,3,1,-4,6};
/* first way.
 
	for(int j = sizeof(arr)/sizeof(int) -1; j>=0; j--){
		if(arr[j] < arr[j-1]){
			int temp=arr[j];
			arr[j] = arr[j-1];
			arr[j-1] = temp;
		}
	}
	printf("%d\n",arr[0]);		// -4;
*/

	int min = arr[0];

	for(int i = 1; i<sizeof(arr)/sizeof(int); i++){
		if(min > arr[i])
			min = arr[i];

	}
	arr[0] = min;

	printf("%d\n",arr[0]);

}

