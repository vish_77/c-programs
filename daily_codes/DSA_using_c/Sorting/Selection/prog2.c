//selection sort code.


#include<stdio.h>

void main(){

	int arr[] = {9,3,2,-1,4,5};
	int size = sizeof(arr)/sizeof(arr[0]);

//for print array.
	for(int i = 0; i< size; i++){
		printf("%d  ",arr[i]);
	}

//sorting
	for(int i = 0; i<size -1; i++){

		int minIndex = i;

		for(int j = i+1; j<size -1; j++){

			if(arr[minIndex] > arr[j]){
				minIndex = j;
			}
		}
		int temp = arr[i];
		arr[i] = arr[minIndex];
		arr[minIndex] = temp;
	}
	
	printf("\n");
//for print array.
	for(int i =0; i<size; i++){
		printf("%d  ",arr[i]);
	}
}
