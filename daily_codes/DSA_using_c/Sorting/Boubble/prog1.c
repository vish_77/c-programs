#include<stdio.h>

void BubleSort(int *arr,int size){

	for(int i = 0; i<size - 1; i++){
		for(int j = 0; j<size - i -1; j++){

			if(arr[j] > arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){

	int arr[] = {2,3,1,6,4,8,0};

	for(int i = 0; i<sizeof(arr)/sizeof(int); i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");

	BubleSort(arr,sizeof(arr)/sizeof(int));
	
	for(int i = 0; i<sizeof(arr)/sizeof(int); i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");

}
