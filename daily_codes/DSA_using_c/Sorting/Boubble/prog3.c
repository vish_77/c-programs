#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node * next;
}node;

node * head  = NULL;

node * CreateNode(){

	node * newnode = (node*)malloc(sizeof(node));
	printf("Enter data = ");

	scanf("%d",&newnode->data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node * newnode = CreateNode();

	if(head == NULL){

		head = newnode;
	}else{
		node * temp = head;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void AddFirst(){

	node * newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode -> next = head;
		head = newnode;
	}
}

int CountNode(){

	int count = 0;
	node * temp = head;

	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

void PrintLL(){

	node * temp = head;

	while(temp != NULL){
		printf("%d ->",temp->data);
		temp = temp ->next;
	}
	printf("\n");
}

void SortLL(){

	int size = CountNode();
	node *temp = head;

	while(temp -> next != NULL){

		int count = 0;

		node *temp2 = head;
		while(temp2 -> next != NULL && count != size - 1){

			if(temp2 -> data > temp2 -> next -> data){
	
				int t = temp2 -> data;
				temp2 -> data = temp2 -> next -> data;
				temp2 -> next -> data = t;
			}
			temp2 = temp2 -> next;
			count ++;
			printf("%d\n",size);
		}
		temp = temp -> next;
		size --;
	}
	
}


void main(){
	
	int num;
	printf("Enter size of linked list = ");

	scanf("%d",&num);

	for(int i = 0; i<num; i++){

		AddNode();
	}
	PrintLL();
	SortLL();
	PrintLL();
}



