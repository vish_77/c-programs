//boubble sort using one for loop

#include<stdio.h>

void BubleSort(int *arr,int size){

	int count = 0;

	for(int j = 0; j<size - 1; j++){

		if(arr[j] > arr[j+1]){

			int temp = arr[j];
			arr[j] = arr[j+1];
			arr[j+1] = temp;
		}

		printf("%d\n",j);

		if(j == size -count- 2){
			j = -1;
			count ++;
		}
		if(count == size -1){
			break;
		}
	}
}

void main(){

	int arr[] = {8,-3,5,3,7,9};
	int size = sizeof(arr)/sizeof(int);

	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");

	BubleSort(arr,size);
	
	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
}
