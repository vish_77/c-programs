/* Take two array of same size make third array put the sum of 
   array ele of first two array into third array.

   IP. = arr1[] = {1,2,4,5}
   	 arr2[] = {4,5,2,1}
   
   OP = arr3[] = {5,7,6,5}
*/

#include<stdio.h>

void Sum(int arr1[], int arr2[], int arr3[], int size){

	for(int i = 0; i<size; i++){

		arr3[i] = arr1[i] + arr2[i];
	}
}

void main(){

	int arr1[] = {1,2,3,4,6};
	int arr2[] = {5,7,8,9,3};
	
	int size = sizeof(arr1)/sizeof(int);
	
	int arr3[size];

	Sum(arr1,arr2,arr3,size);

	for(int i = 0; i<size; i++){
		printf("%d  ",arr3[i]);
	}
	printf("\n");
}

