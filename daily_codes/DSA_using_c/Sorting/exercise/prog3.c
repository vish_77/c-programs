/*
	Take two array of diff. size make sort them & marge them into third array.
*/

#include<stdio.h>

void Sort(int arr[], int size){

	for(int i = 1; i<size; i++){
		
		int val = arr[i];
		int j = i-1;

		for(;j>=0 && arr[j] > val; j--){
			arr[j+1] = arr[j];
		}
		arr[j+1] = val;
	}
}

void Marge(int arr1[], int arr2[], int arr3[], int m, int n, int o){

	int a = 0;
	int b = 0;
	for(int i = 0; i<o; i++){

		if(a < m && b < n){
			if(arr1[a] < arr2[b]){
				arr3[i] = arr1[a];
				a++;
			}else{
				arr3[i] = arr2[b];
				b++;
			}

		}else{
			
			if(b == n){
				arr3[i] = arr1[a];
				a++;
			}else{
				arr3[i] = arr2[b];
				b++;
			}

		}
	}
}

			

void main(){

	int m,n,o;
	
	printf("Enter size of arr1 = ");
	scanf("%d",&m);
	int arr1[m];
	printf("Enter array elements\n");
	for(int i = 0; i<m; i++){
                scanf("%d",&arr1[i]);
        }
   
	printf("Enter size of arr2 = ");
        scanf("%d",&n);
        int arr2[n];
        printf("Enter array elements\n");
        for(int i = 0; i<n; i++){
                scanf("%d",&arr2[i]);
        }

	Sort(arr1,m);
	Sort(arr2,n);

	o=m+n;
	int arr3[o];
	Marge(arr1,arr2,arr3,m,n,o);
        
	for(int i = 0; i<o; i++){
                printf("%d  ",arr3[i]);
        }
	printf("\n");
  
}
