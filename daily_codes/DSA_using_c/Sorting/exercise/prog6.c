/*
	Take array make sort it & find middle ele.
*/

#include<stdio.h>

void Sort(int arr[],int size){

	for(int i = 0; i<size - 1; i++){
		for(int j = 0; j< size - i - 1; j++){

			if(arr[j] > arr[j+1]){
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){

	int arr[] = {2,5,1,3,4,-2};
	int size = sizeof(arr)/sizeof(int);
	Sort(arr,size);
	for(int i =0 ;i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
	printf("%d is middle ele \n",arr[(0+size-1)/2]);
}


