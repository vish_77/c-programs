//sort the names in array by its lengeth 


#include<stdio.h>
#include<string.h>


void SortbyLen(char (*arr)[10],int size){

	for(int i = 0; i<size - 1; i++){

		for(int j = 0; j<size - i - 1; j++){

			char temp[10];

			if(strlen(arr[j]) == strlen(arr[j+1])){
				char *ptr1 = arr[j];
				char *ptr2 = arr[j+1];

				while(*ptr1 == *ptr2){
					*ptr1++;
					*ptr2++;
				}

				if(*ptr1 > *ptr2){
					strcpy(temp,arr[j]);
                                	strcpy(arr[j],arr[j+1]);
                               	 	strcpy(arr[j+1],temp);
				}

			}

			if(strlen(arr[j]) > strlen(arr[j+1])){
				
				strcpy(temp,arr[j]);
				strcpy(arr[j],arr[j+1]);
				strcpy(arr[j+1],temp);
			}
		}
	}
}

void main(){

	char arr[][10] = {"Shashi","Kanha","Rajul","Raj","Anuj","Shashi"};

	int size = sizeof(arr)/sizeof(arr[0]);

	for(int i = 0; i<size; i++){
		printf("%s   ",arr[i]);
	}
	printf("\n");

	SortbyLen(arr,size);
		
	for(int i = 0; i<size; i++){
		printf("%s   ",arr[i]);
	}

}
