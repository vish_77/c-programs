#include<stdio.h>


void Counting(int arr[], int size){

	int max = arr[0];
	for(int i = 1; i<size; i++){

		if(max < arr[i])
			max = arr[i];
	}

	int Countarr[max];

	for(int i = 0; i<= max; i++){

		Countarr[i] = 0;
	}

	for(int i = 0; i< size; i++){

		Countarr[arr[i]]++;
	}

	for(int i=1; i<=max; i++){
		Countarr[i] = Countarr[i] + Countarr[i-1];
	}

	int Output[size];
	for(int i = size-1; i>= 0; i--){
		Output[Countarr[arr[i]]-1] = arr[i];
		Countarr[arr[i]]--;
	}

	for(int i = 0; i<size; i++){
		arr[i] = Output[i];
	}
}


void main(){

	int arr[] = {3,7,2,1,2,8,5,2,7};

	int size = sizeof(arr)/sizeof(int);

	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");

	Counting(arr,size);

	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
	
}
