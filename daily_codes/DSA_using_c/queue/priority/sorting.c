/*Sort:
  Sort linked list while adding node. add node in assending order.
ip:  10 -> 5 -> 15 -> 7 -> 9
op:  5 -> 7 -> 9 -> 10 -> 15

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;
	return newnode;
}

node *AddFirst(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode -> next = newnode;
		head = newnode;
	}
}

int AddBySort(){

	if(head == NULL){
		AddFirst();
		return 0;
	}else{
		node *newnode = CreateNode();

		if(newnode -> data < head -> data){
			newnode -> next = head;
			head = newnode;
			return 0;
		}

		node *temp = head;
		while(temp -> next != NULL){
			
				if(temp -> data <= newnode -> data && newnode -> data <= temp -> next -> data){
					newnode -> next = temp -> next;
					temp -> next = newnode;
					return 0;
				}
			
			temp = temp -> next;
		}
		temp -> next = newnode;
		return 0;
	}
}

void printLL(){

	node *temp = head;
	while(temp-> next != NULL){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

void main(){

	int node;
	printf("Enter number of nodes = ");
	scanf("%d",&node);

	for(int i = 0; i<node ; i++){
		AddBySort();
	}

	printLL();
}
