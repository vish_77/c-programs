/*Sort:
  Sort linked list while adding node. by priority in descending order.
ip: priority =  5 -> 0 -> 1 -> 4 -> 3
op:		5 -> 4 -> 3 -> 1 -> 0

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	int data;
	int priority;
	struct Node *next;
}node;

node *head = NULL;
int flag = 0;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	do{
		printf("Enter priority = ");
		scanf("%d",&newnode -> priority);
	}while(newnode -> priority < 0 || newnode -> priority > 5);

	newnode -> next = NULL;
	return newnode;
}

node *AddFirst(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		newnode -> next = newnode;
		head = newnode;
	}
}

int AddByPriority(){

	if(head == NULL){
		AddFirst();
		return 0;
	}else{
		node *newnode = CreateNode();

		if(newnode -> priority > head -> priority){
			newnode -> next = head;
			head = newnode;
			return 0;
		}

		node *temp = head;
		while(temp -> next != NULL){

			if(temp -> priority == temp -> next -> priority){
				temp = temp -> next;
			}else{
			
				if(temp -> priority >= newnode -> priority && newnode -> priority >= temp -> next -> priority){

					newnode -> next = temp -> next;
					temp -> next = newnode;
					return 0;
				}
			
				temp = temp -> next;
			}
		}
		temp -> next = newnode;
		return 0;
	}
}

int Dequeue(){

	if(head == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		node *temp = head;
		head = head -> next;
		int val = temp -> data;
		free(temp);
		return val;
	}
}

//to print data
int PrintLL(){

	if(head == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		node *temp = head;
		while(temp-> next != NULL){
			printf("|%d| -> ",temp -> data);
			temp = temp -> next;
		}
		printf("|%d|\n",temp -> data);
	}
}

// to print priority 
int PrintLL2(){

	if(head == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;

		node *temp = head;
	        while(temp-> next != NULL){
        	        printf("|%d| -> ",temp -> priority);
                	temp = temp -> next;
	        }
        	printf("|%d|\n",temp -> priority);
	}
}


void main(){

        char choice;

        do{

                printf("1. AddByPriority()\n");
                printf("2. dequeue\n");
                printf("3. Print LL\n");
                printf("4. Print priority\n");

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1:
                                {
				int count;
				printf("Enter node count =");
				scanf("%d",&count);
				for(int i = 1; i<= count; i++){
					AddByPriority();
				}
                                 
                                }
                                break;

                        case 2:
                                {
                                int ret = Dequeue();

                                if(flag == 0)
                                        printf("LL is empty\n");
                                else
                                        printf("%d is dequeued\n",ret);
                                }
                                break;

                         case 3:
				{
					PrintLL();

                                if(flag == 0)
                                        printf("LL is empty\n");
                                }
                                break;

			case 4:
				{
				PrintLL2();

				if(flag == 0)
					printf("LL is empty\n");
				}
				break;

                        default :
                                printf("Invalid choice\n");
              }
                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);
        }while(choice == 'y' || choice == 'Y');
}
