
//without size queue.

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *front = NULL;
node *rear = NULL;
int flag = 0;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	if(newnode == NULL){
		printf("Heap size is full\n");
	}

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;
	return newnode;
}

int Enqueue(){
	
	node *newnode = CreateNode();
	
	if(front == NULL){
		front = newnode;
		rear = newnode;
	}else{
		rear -> next = newnode;
		rear = newnode;

	}
	return 0;
	
}

int Dequeue(){

	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		int val = front -> data;
		if(front -> next == NULL){
			free(front);
			front = NULL;
			rear = NULL;
		}else{
			node *temp = front;
			front = front -> next;
			free(temp);
		}
		return val;
	}
}

int Front(){

	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		return front -> data;
	}
}

int PrintQueue(){

	if(front == NULL){
		flag = 0;
		return -1;
	}else{
		flag = 1;
		node *temp = front;
		while(temp != NULL){
			printf("|%d|  ",temp -> data);
			temp = temp -> next;
		}
		printf("\n");
	}
}

void main(){

        char choice;

        do{

                printf("1. enqueue\n");
                printf("2. dequeue\n");
                printf("3. front\n");
                printf("4. Print queue\n");

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1:
				{
				int ret = Enqueue();
				if(ret == -1)
                                       printf("Queue Overflow\n");
                                }
                                break;

                        case 2:
                                {
                                int ret = Dequeue();

                                if(flag == 0)
                                        printf("Queue Underflow\n");
                                else
                                        printf("%d is dequeued\n",ret);
                                }
                                break;
				
                        case 3:
                              {
                                int ret = Front();

                                if(flag == 0)
                                        printf("Queue Underflow\n");
                                else
                                        printf("%d is front\n",ret);
                                }
                                break;

                         case 4:
                                {
                                int ret = PrintQueue();

                                if(flag == 0)
                                        printf("Queue Underflow\n");
                                }
                                break;

                        default :
                                printf("Invalid choice\n");
              }
		getchar();
		printf("Do you want to continue = ");
		scanf("%c",&choice);
	}while(choice == 'y' || choice == 'Y');
}
