/*

 	 	 	 	*	
 	 	*	*	*	
*	*	*	*	*	
 	 	*	*	*	
 	 	 	 	*
				
*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = 2*rows-2*i;
			cols = 2*i-1;
		}else{
			space = 2*i - 2*rows;
			cols = cols -2;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("*\t");
		}
		printf("\n");
	}
}
