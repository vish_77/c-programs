/*

 	 	 	*	
 	 	*	*	
 	*	*	*	
*	*	*	*	
 	*	*	*	
 	 	*	*	
 	 	 	*	
			*
*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
		}else{
			space = i-rows;
			cols = 2*rows - i;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("*\t");
		}
		printf("\n");
	}
}
