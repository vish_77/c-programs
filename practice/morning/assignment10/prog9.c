/*

 	 	 	1	
 	 	2	4	
 	3	6	9	
4	8	12	16	
 	3	6	9	
 	 	2	4	
 	 	 	1	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 0;

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
			num++;
		}else{
			space = i-rows;
			cols = 2*rows - i;
			num--;
		}

		int temp = num;

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("%d\t",temp);
			temp+=num;
		}
		printf("\n");
	}
}
