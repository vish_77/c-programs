/*

 	 	 	1	
 	 	1	2	
 	2	3	4	
4	5	6	7	
 	7	8	9	
 	 	9	10	
 	 	 	10	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 1;

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
		}else{
			space = i-rows;
			cols = 2*rows - i;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("%d\t",num);
			num+=1;
		}
		num -=1;
		printf("\n");
	}
}
