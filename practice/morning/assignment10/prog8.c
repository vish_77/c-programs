/*

 	 	 	D	
 	 	C	D	
 	B	C	D	
A	B	C	D	
 	B	C	D	
 	 	C	D	
 	 	 	D

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = rows+1;
	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
			num--;
		}else{
			space = i-rows;
			cols = 2*rows - i;
			num++;
		}
		int temp = num;

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("%c\t",temp+64);
			temp++;
		}
		printf("\n");
	}
}
