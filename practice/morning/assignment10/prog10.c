/*

	 	 	 	1	
 	 	3	2	1	
5	4	3	2	1	
 	 	3	2	1	
 	 	 	 	1

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num;

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = 2*rows-2*i;
			cols = 2*i-1;
			num = cols;
		}else{
			space = 2*i - 2*rows;
			cols = cols -2;
			num = cols;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("%d\t",num);
			num --;
		}
		printf("\n");
	}
}
