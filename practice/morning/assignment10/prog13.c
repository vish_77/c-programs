/*

 	 	 	A	
 	 	a	b	
 	B	C	D	
d	e	f	g	
 	G	H	I	
 	 	i	j	
 	 	 	J	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 1;

	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
		}else{
			space = i-rows;
			cols = 2*rows - i;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			
			if(i%2 == 1){
				printf("%c\t",num+64);
			}else{
				printf("%c\t",num+96);
			}
			num++;
		}
		num --;
		printf("\n");
	}
}
