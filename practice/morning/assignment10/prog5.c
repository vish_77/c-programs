/*

	 	 	3	
 	 	2	3	
 	1	2	3	
0	1	2	3	
 	1	2	3	
 	 	2	3	
 	 	 	3	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = rows;
	for(int i =1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
			num--;
		}else{
			space = i-rows;
			cols = 2*rows - i;
			num++;
		}
		int temp = num;

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j=1; j<= cols; j++){
			printf("%d\t",temp);
			temp++;
		}
		printf("\n");
	}
}
