/*
 
   concat last n(n from user) umber of nodes to the linked list from the other LL 
 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(node **head){

	node *temp1 = *head;
	
	node *newnode = CreateNode();

	if(*head == NULL){
		*head = newnode;
	}else{

		node *temp = *head;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
/*
	if(temp1 == head1){
		head1 = head;
	}else{
		head2 = head;
	}
*/
}

void PrintLL(node *head){

	node *temp = head;
	while(temp -> next != NULL){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

int NodeCount(node *head){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

node *traversehead2(int pos){
	
	int count = NodeCount(head2);
	if(pos <= 0 || pos > count){
		printf("Invalid position\n");
	}

	if(head2 == NULL){
		printf("list is empty\n");
	}else{

		if(pos == 1){
			node *temp1 = head2;
			while(temp1 -> next != NULL){
				temp1 = temp1 -> next;
			}
			return temp1;
		}else if(pos == 2){
			node *temp2 = head2;
			while(temp2 -> next -> next != NULL){
				temp2 = temp2 -> next;
			}
			return temp2;

		}else if(pos == count){
			return head2;
		
		}else{
			node *temp3 = head2;
			while(count - pos){
				temp3 = temp3 -> next;
				pos --;
			}
			return temp3;
		}
	}
}

int ConcatLastNLL(int pos){
	
	node *concat = traversehead2(pos);

	if(head1 == NULL){
		head1 = concat;
		return 0;
	}else{
		printf("a\n");
		node *temp = head1;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp->next = concat;
		return 0;
	}
}

void main(){
	
	int node;
	
	printf("Enter number of nodes for LL1 = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode(&head1);
	}
	
	printf("Enter number of nodes for LL2 = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode(&head2);
	}

	PrintLL(head1);
	PrintLL(head2);

	printf("%d\n",NodeCount(head2));
	
	int num;
	printf("Enter number of nodes you want to concat = ");
	scanf("%d",&num);

	ConcatLastNLL(num);	
	PrintLL(head1);
}	
