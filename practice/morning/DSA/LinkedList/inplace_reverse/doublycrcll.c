#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	struct Node *prev;
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	newnode -> prev = NULL;

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){

		head = newnode;
		head -> next = head;
		head -> prev = head;
	}else{
		head -> prev -> next = newnode;
		newnode -> next = head;
		newnode -> prev = head -> prev;
		head -> prev = newnode;
	}
}

int NodeCount(){
	

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		node *temp = head;
		int count = 0;
	
		while(temp -> next != head){
			count++;
			temp = temp -> next;
		}

		return count + 1;
	}
}

void AddFirst(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
		head -> next = head;
		head -> prev = head;
	}else{
		newnode -> next = head;
		head -> prev -> next = newnode;
		newnode -> prev = head -> prev;
		head -> prev = newnode;
		head = newnode;
	}
}


void PrintLL(){

	node *temp = head;

	while(temp -> next != head){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

int AddAtPos(int pos){

	int count = NodeCount();
	if(pos <= 0 || pos > count + 1){
		printf("Invalid position\n");
		return -1;
	}else{
		if(pos == 1){
			AddFirst();
		}else if(pos == count + 1){
			AddNode();
		}else{
			node *newnode = CreateNode();

			node *temp = head;
			while(pos - 2){
				temp = temp -> next;
				pos --;
			}
			newnode -> next = temp -> next;
			newnode -> prev = temp;
			temp -> next -> prev = newnode;
			temp -> next = newnode;
		}
		return 0;
	}
}

int DeleteFirst(){
	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(head -> next == head){
			free(head);
			head = NULL;
		}else{
			head = head -> next;
			head -> prev = head -> prev -> prev;
			free(head -> prev -> next);
			head -> prev -> next = head;
		}
		return 0;
	}
}

int DeleteLast(){

	if(head == NULL){
		printf("Linked list is empty\n");
		return -1;
	}else{
		if(head -> next == head){
			free(head);
			head = NULL;
		}else{
			head -> prev = head -> prev -> prev;
			free(head -> prev -> next); 
			head -> prev -> next = head;
		}
		return 0;
	}
}

int DeleteAtPos(int pos){

	int count = NodeCount();

	if(pos <= 0 || pos > count){
		printf("Invalid position\n");
		return -1;
	}else{

		if(pos == 1){
			DeleteFirst();
		}else if(pos == count){
			DeleteLast();
		}else{
			node *temp = head;

			while(pos - 2){
				temp = temp -> next;
				pos --;
			}
			temp -> next = temp -> next -> next;
			free(temp -> next -> prev);
			temp -> next -> prev = temp;
		}
		return 0;
	}
}

void InplRev(){

	node *temp1 = NULL;

	while(head -> next != NULL){
		
		head -> prev = head -> next;
		head -> next = temp1;
		temp1 = head;
		head = head -> prev;
	}
	head -> next = temp1;
	head = temp1;
}

void main(){

        char choice;

        do{
                printf("Enter 1 for AddNode()\n");
                printf("Enter 2 for AddFirst()\n");
                printf("Enter 4 for AddAtPos()\n");
                printf("Enter 5 for PrintLL()\n");
                printf("Enter 6 for NodeCount()\n");
                printf("Enter 7 for DeleteFirst()\n");
                printf("Enter 8 for DeleteLast()\n");
                printf("Enter 9 for DeleteAtPos()\n");
                printf("Enter 10 for inplace reverse\n");

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int node;
                                        printf("Enter no of nodes = \n");
                                        scanf("%d",&node);

                                        for(int i = 1; i<= node; i++){
                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :
                                AddFirst();
                                break;

                        case 4 :
                                {
                                        int pos;
                                        printf("Enter position = ");
                                        scanf("%d",&pos);
                                        AddAtPos(pos);
                                }
                                break;

                        case 5 :
                                PrintLL();
                                break;

                        case 6 :
                                printf("Node count = %d\n",NodeCount());
                                break;

                        case 7 :
                                DeleteFirst();
                                break;

                        case 8 :
                                DeleteLast();
                                break;

                        case 9 :
                                {
                                        int pos;
                                        printf("Enter position = ");
                                        scanf("%d",&pos);
                                        DeleteAtPos(pos);
                                }
                                break;

			case 10 :
				InplRev();
				break;

                        default :
                                printf("Enter corrct choice\n");

                }

                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}                      
