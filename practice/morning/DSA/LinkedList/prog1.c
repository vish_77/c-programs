/*
 
   Create Two linked list in single code using one function

 */

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter data = ");
	scanf("%d",&newnode -> data);

	newnode -> next = NULL;

	return newnode;
}

void AddNode(node *head){

	node *temp1 = head;
	
	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{

		node *temp = head;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}

	if(temp1 == head1){
		head1 = head;
	}else{
		head2 = head;
	}

}

void PrintLL(node *head){

	node *temp = head;
	while(temp -> next != NULL){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

void ConcatLL(){

	node *temp = head1;

	while(temp -> next != NULL){
		temp = temp -> next;
	}
	temp -> next = head2;
}

void main(){
	
	int node;
	
	printf("Enter number of nodes for LL1 = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode(head1);
	}
	
	printf("Enter number of nodes for LL2 = ");
	scanf("%d",&node);

	for(int i = 1; i<= node; i++){
		AddNode(head2);
	}

	PrintLL(head1);
	PrintLL(head2);

	ConcatLL();
	PrintLL(head1);
}	
