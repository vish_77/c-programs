#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int space,cols;

	int num = 1;

	for(int i = 1; i<rows*2; i++){

		if(i<rows){
			space = rows - i;
			cols = i*2-1;
		}else{
			space = i - rows;
			cols = 4*rows-2*i-1;
		}

		for(int i = 1; i<= space; i++){
			printf(" \t");
		}

		for(int j = 1; j<= cols; j++){
			printf("%d\t",num);
			num++;
		}
		
		printf("\n");
	}
}
