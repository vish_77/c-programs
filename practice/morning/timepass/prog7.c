/*

      * 
    * * * 
  * * * * * 
* * * * * * * 
  * * * * * 
    * * * 
      * 

*/

#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int space,cols;


	for(int i = 1; i<rows*2; i++){

		if(i<=rows){
			space = rows - i;
			cols = i;
		}else{
			space = i - rows;
			cols = cols-1;
		}

		for(int i = 1; i<= space; i++){
			printf("  ");
		}

		for(int j = 1; j<= cols; j++){
			printf("* ");
		}
		
		printf("\n");
	}
}
