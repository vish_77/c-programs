/*

        * * * * * 
      * * * * 
    * * * 
  * * 
* 
  * * 
    * * * 
      * * * * 
        * * * * * 


*/


#include<stdio.h>
void main(){

	int rows,space,cols;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<2*rows; i++){
		
		if(i<rows){
			space = rows - i;
			cols = rows+1 - i;
		}else{
			space = i - rows;
			cols = i-rows+1;
		}

		for(int sp = 1; sp <=space; sp++){
			printf("  ");
		}
	
		for(int j = 1; j<=cols; j++){
			printf("* ");
		}

		printf("\n");
	}
}
