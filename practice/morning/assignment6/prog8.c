/*

D	D	D	D	
 	c	c	c	
 	 	B	B	
 	 	 	a	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	int num = rows;

	for(int i = 1; i<= rows; i++){
	
		for(int j = 1; j<i; j++){

			printf(" \t");
		}

		for(int k = rows; k>=i; k--){
			if(i % 2 == 1 )
				printf("%c\t",num + 64);
			else
				printf("%c\t",num+96);
				
		}

		num --;
		printf("\n");
	}
}
