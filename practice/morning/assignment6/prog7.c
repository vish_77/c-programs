/*

4	3	2	1	
 	3	2	1	
 	 	2	1	
 	 	 	1	
	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	int temp = rows;

	for(int i = 1; i<= rows; i++){
	
		for(int j = 1; j<i; j++){

			printf(" \t");
		}
		
		int num = temp;

		for(int k = rows; k>=i; k--){
			printf("%d\t",num);
			num--;
		}

		temp --;
		printf("\n");
	}
}
