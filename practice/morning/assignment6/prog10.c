/*

A	b	C	d	
 	E	f	G	
 	 	H	i	
 	 	 	J

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	int num = 1;
	
	for(int i = 1; i<= rows; i++){

		for(int j = 1; j<i; j++){

			printf(" \t");
		}

		for(int k = 1; k<=rows-i+1; k++){
			if(k % 2 == 0 )
				printf("%c\t",num+96);
			else
				printf("%c\t",num+64);
						
			num ++;
		}
		
		printf("\n");
	}
}
