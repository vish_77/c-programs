/*

1	3	5	7	9	
 	9	7	5	3	
 	 	3	5	7	
 	 	 	7	5	
 	 	 	 	5	
	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	int num = 1;
	
	for(int i = 1; i<= rows; i++){
		
		int x = 1;
		for(int j = 1; j<i; j++){

			printf(" \t");
			x++;
		}

		for(int k = 1; k<=rows-i+1; k++){

			if( x % 2 == 1){
				printf("%c\t",num+64);
			}else{
				printf("%c\t",num+96);
			}

			num = num + i;
			
			x++;

		}

		printf("\n");
	}
}
