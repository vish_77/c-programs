/*

1	2	3	4	
 	1	2	3	
 	 	1	2	
 	 	 	1	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);


	for(int i = 1; i<= rows; i++){

		for(int j = 1; j<i; j++){

			printf(" \t");
		}

		for(int k = 1; k<=rows-i+1; k++){

			printf("%d\t",k);
		}

		printf("\n");
	}
}
