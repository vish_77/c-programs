/*

WAP to print the numbers whose factorial is even. Take range from user 
IP : Start 1
     End   5

OP :2	3	4	5
*/


#include<stdio.h>
void main(){

	int start,end;
	printf("Enter start  = ");
	scanf("%d",&start);
	printf("Enter end  = ");
	scanf("%d",&end);

	
	for(int num = start; num <= end; num++){
		
		int temp = num;
		int fact = 1;
	
		while(temp >= 1){
			
			fact = fact * temp;
			temp --;
		}
			
		if(fact % 2 == 0){
			printf("%d\t",num);
		}
	}
	printf("\n");
}	
