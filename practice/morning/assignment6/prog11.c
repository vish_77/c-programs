/*

1	3	5	7	9	
 	9	7	5	3	
 	 	3	5	7	
 	 	 	7	5	
 	 	 	 	5	
	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	int num = 1;
	
	for(int i = 1; i<= rows; i++){

		for(int j = 1; j<i; j++){

			printf(" \t");
		}

		for(int k = 1; k<=rows-i+1; k++){
			if( i % 2 == 1){
				printf("%d\t",num);
				num +=2;
			}else{
				printf("%d\t",num);
				num -=2;
			}
		}
		if(i%2 == 1)
			num -= 2;
		else
			num += 2;

		printf("\n");
	}
}
