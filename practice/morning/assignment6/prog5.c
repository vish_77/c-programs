/*

a	B	c	D	
 	E	f	G	
 	 	h	I	
 	 	 	J	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 1;

	for(int i = 1; i<= rows; i++){
		int a = 1;
		for(int j = 1; j<i; j++){

			printf(" \t");
			a ++;
		}

		for(int k = 1; k<=rows-i+1; k++){

			if(a % 2  == 1)
				printf("%c\t",num+96);
			else
				printf("%c\t",num+64);

			num++;
			a++;
		}

		printf("\n");
	}
}
