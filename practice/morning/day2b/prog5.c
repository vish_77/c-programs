/*

a	B	c	
d	E	f	
g	H	i		

*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter row :");
	scanf("%d",&rows);
	char ch = 65;

	for(int i = rows; i>=1; i--){
		for(int j = 1; j<= rows; j++){
			if(j%2 == 1){
				printf("%c\t",ch+32);
			}else{
				printf("%c\t",ch);
			}
			ch++;

		}
		printf("\n");
	}
}
