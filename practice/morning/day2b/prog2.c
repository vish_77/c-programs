// print whether character is alphabet or integer or special char

#include<stdio.h>
void main(){
	char ch;
	printf("Enter : ");
	scanf("%c",&ch);

	if((ch >= 'A' && ch <= 'Z') || (ch >= 'a' && ch <= 'z')){
		printf("%c is a alphabet\n",ch);
	}else if(ch >= 0 || ch <= 9){
		printf("%d is a integer\n",ch);
	}else{
		printf("%c is a special character\n",ch);
	}

}
