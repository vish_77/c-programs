/*

	 	 	1	
 	 	1	2	3	
 	1	2	3	4	3	
1	2	3	4	3	2	1	


*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = 1;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
		}

		for(int j = 1; j<= 2*i-1; j++){
			
			if(j < rows){
				printf("%d\t",num);
				num++;
			}else{
				printf("%d\t",num);
				num--;
			}
		}
		
		printf("\n");
	}
}
