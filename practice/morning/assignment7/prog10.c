/*

 	 	 	 	A	
 	 	 	b	a	b	
 	 	C	E	G	E	C	
 	d	c	b	a	b	c	d	
E	G	I	K	M	K	I	G	E	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = i;
		int x = 1;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
			x ++;
		}

		for(int j = 1; j<= 2*i-1; j++){

			if(i % 2 == 0){
			
				if(x < rows){
					printf("%c\t",num+96);
					num--;
				}else{
					printf("%c\t",num+96);
					num++;
				}
			}else{
				if(x < rows){
					printf("%c\t",num+64);
					num+=2;
				}else{
					printf("%c\t",num+64);
					num -=2;
				}
			}
			x++;

		}
		
		printf("\n");
	}
}
