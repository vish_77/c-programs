/*

 	 	 	4
 	 	3	6	3
 	2	4	6	4	2
1	2	3	4	3	2	1

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows;

	for(int i = 1; i<=rows; i++){

		int x = 1;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
			x++;
		}

		int temp = num;
		for(int j = 1; j<= 2*i-1; j++){
			if(x<rows){
				printf("%d\t",temp);
				temp += num;
			}else{
				printf("%d\t",temp);
				temp -= num;
			}
			x++;
		}
		num --;
		printf("\n");
	}
}
