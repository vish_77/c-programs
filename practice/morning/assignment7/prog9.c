/*

 	 	 	 	5	
 	 	 	5	6	7	
 	 	5	4	3	2	1	
 	5	6	7	8	9	8	7	
5	4	3	2	1	2	3	4	5	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = rows;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
		}

		for(int j = 1; j<= 2*i-1; j++){

			if(i % 2 == 1){
			
				if(j < rows){
					printf("%d\t",num);
					num--;
				}else{
					printf("%d\t",num);
					num++;
				}
			}else{
				if(j < rows){
					printf("%d\t",num);
					num++;
				}else{
					printf("%d\t",num);
					num --;
				}
			}

		}
		
		printf("\n");
	}
}
