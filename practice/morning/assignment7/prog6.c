/*

	 	 	d	
 	 	C	C	C	
 	b	b	b	b	b	
A	A	A	A	A	A	A	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows;

	for(int i = 1; i<=rows; i++){

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
		}

		for(int j = 0; j< 2*i-1; j++){
			if(i%2 == 1){
				printf("%c\t",num+96);
			}else{
				printf("%c\t",num+64);
			}			
		}
		num --;
		printf("\n");
	}
}
