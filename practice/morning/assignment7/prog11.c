/*

 	 	 	1	
 	 	A	b	A	
 	1	2	3	2	1	
A	b	C	d	C	b	A	

*/
#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = 1;
		int x = 1;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
			x ++;
		}

		for(int j = 1; j<= 2*i-1; j++){

			if(i % 2 == 1){
			
				if(x < rows){
					printf("%d\t",num);
					num++;
				}else{
					printf("%d\t",num);
					num--;
				}
			}else{
				if(x < rows){
					
					if(x % 2 == 1){
						printf("%c\t",num+64);
						num++;
					}else{
						printf("%c\t",num+96);
						num++;
					}
				}else{
					if(x % 2 == 1){
						printf("%c\t",num+64);
						num--;
					}else{
						printf("%c\t",num+96);
						num--;
					}
				}
			
				
			}
			x++;

		}
		
		printf("\n");
	}
}
