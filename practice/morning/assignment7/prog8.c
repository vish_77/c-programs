/*

 	 	 	D
 	 	c	D	c
 	B	c	D	c	B
a	B	c	D	c	B	a

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows;

	for(int i = 1; i<=rows; i++){

		int x = 1;

		for(int sp = rows; sp > i; sp--){
			printf(" \t");
			x++;
		}
			
		int temp = num;

		for(int j = 0; j< 2*i-1; j++){
			if(x<rows){
				if(x % 2 == 1){
					printf("%c\t",temp+96);
				}else{
					printf("%c\t",temp+64);
				}

				temp ++;
			}else{
				
				if(x % 2 == 1){
					printf("%c\t",temp+96);
				}else{
					printf("%c\t",temp+64);
				}

				temp --;
			}
			x++;			
		}
		num --;
		printf("\n");
	}
}
