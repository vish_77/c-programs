/*

	 	 	1	
 	 	4	7	4	
 	7	10	13	10	7	
10	13	16	19	16	13	10	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);


	int num = 1;

	for(int i = 1; i<=rows; i++){

		int x = 1;
		for(int sp = rows; sp > i; sp--){
			printf(" \t");
			x++; 
		}

		for(int j = 0; j< 2*i-1; j++){
			if(x<rows){
				printf("%d\t",num);
				num += 3;
			}else{
				printf("%d\t",num);
				num -= 3;
			}
			x++;			
		}
		num += 6;
		printf("\n");
	}
}
