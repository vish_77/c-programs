/*

1	2	3	4	
2	3	4	
3	4	
4

*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows :");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){
		for(int j = 1; j<=rows-i+1; j++){
			printf("%d\t",j+i-1);
			
		}
		printf("\n");
	}
}
