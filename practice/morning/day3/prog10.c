/*

4	3	2	1	
C	B	A	
2	1	
A	

*/

#include<stdio.h>
void main(){
	int rows;
	printf("Enter rows :");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){
		for(int j = rows; j>=i; j--){
			if(i%2==1){
				printf("%d\t",j-i+1);
			}else{
				printf("%c\t",j+65-i);
			}
		
		}
		printf("\n");
	}
}
