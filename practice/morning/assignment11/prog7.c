/*

	 	 	A	
 	 	b	A	b	
 	C	b	A	b	C	
d	C	b	A	b	C	d	
 	C	b	A	b	C	
 	 	b	A	b	
 	 	 	A	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = 0;
	for(int i = 1; i<2*rows; i++){
	
	int x = 1;

		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
			num++;
		}else{
			space = i-rows;
			cols = cols - 2;
			num--;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j<= cols; j++){
			if(x%2 == 1){
				if(x<rows){
					printf("%c\t",num+96);
					num--;
				}else{
					printf("%c\t",num+96);
					num++;
				}
			}else{
				if(x<rows){
					printf("%c\t",num+64);
					num--;
				}else{
					printf("%c\t",num+64);
					num++;
				}			
			}	
			x++;
		}
		num--;		
		printf("\n");
	}
}
