/*


*	*	*	*	*	*	*	
*	*	*	 	*	*	*	
*	*	 	 	 	*	*	
*	 	 	 	 	 	*	
*	*	 	 	 	*	*	
*	*	*	 	*	*	*	
*	*	*	*	*	*	*	


*/

#include<stdio.h>
void main(){

	int rows,cols,space,star;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<rows*2; i++){
		
		int num = i-1;
		if(i<= rows){
			star = rows - i+1;
			cols = i+rows-2;
			space = 2*num - 1;
		}else{
			star = i - rows+1;
			cols = cols - 1;
			space -= 2;
		}

		for(int sp = 1; sp<= star; sp++){
			printf("*\t");
		}

		for(int j = 1; j<=cols; j++){
			if(j<=space){
				printf(" \t");
			}else{
				printf("*\t");
			}
		}

		printf("\n");
	}
}
