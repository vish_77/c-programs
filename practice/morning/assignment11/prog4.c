/*

	 	 	1	
 	 	2	1	2	
 	3	2	1	2	3	
4	3	2	1	2	3	4	
 	3	2	1	2	3	
 	 	2	1	2	
 	 	 	1	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = 0;
	for(int i = 1; i<2*rows; i++){
	
	int x = 1;

		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
			num++;
		}else{
			space = i-rows;
			cols = cols - 2;
			num--;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j<= cols; j++){
			if(x<rows){
				printf("%d\t",num);
				num--;
			}else{
				printf("%d\t",num);
				num++;
			}
			x++;
		}
		num--;		
		printf("\n");
	}
}
