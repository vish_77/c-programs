/*

 	 	 	1	
 	 	2	2	2	
 	3	3	3	3	3	
4	4	4	4	4	4	4	
 	3	3	3	3	3	
 	 	2	2	2	
 	 	 	1	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = 0;
	for(int i = 1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
			num ++;
		}else{
			space = i-rows;
			cols = cols - 2;
			num--;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j = 1; j<= cols; j++){
			printf("%d\t",num);
		}
		printf("\n");
	}
}
