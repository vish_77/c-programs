/*

 	 	 	A	
 	 	b	b	b	
 	C	C	C	C	C	
d	d	d	d	d	d	d	
 	C	C	C	C	C	
 	 	b	b	b	
 	 	 	A	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = 0;
	for(int i = 1; i<2*rows; i++){

		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
			num++;
		}else{
			space = i-rows;
			cols = cols - 2;
			num--;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}

		for(int j = 1; j<= cols; j++){
			if(i%2==1){
				printf("%c\t",num+64);
			}else{
				printf("%c\t",num+96);
			}
		}
		printf("\n");
	}
}
