/*

 	 	 	D	
 	 	D	C	D	
 	D	C	B	C	D	
D	C	B	A	B	C	D	
 	D	C	B	C	D	
 	 	D	C	D	
 	 	 	D	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows;

	for(int i = 1; i<2*rows; i++){
		int x = 1;
		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
		
		}else{
			space = i-rows;
			cols = cols - 2;
		}

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j<= cols; j++){
			if(x<rows){
				printf("%c\t",num+64);
				num--;
			}else{
				printf("%c\t",num+64);
				num++;
			}
			x++;
		}
		num--;
		printf("\n");
	}
}
