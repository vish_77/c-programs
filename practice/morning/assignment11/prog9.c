/*

 	 	 	1	
 	 	4	2	4	
 	9	6	3	6	9	
16	12	8	4	8	12	16	
 	9	6	3	6	9	
 	 	4	2	4	
 	 	 	1	

*/

#include<stdio.h>

void main(){
	
	int rows,cols,space,temp;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 0;

	for(int i = 1; i<2*rows; i++){
		int x = 1;

		if(i<=rows){
			space = rows - i;
			cols = 2*i-1;
			num ++;
		}else{
			space = i-rows;
			cols = cols - 2;
			num --;
		}
		temp = num*num;

		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j<= cols; j++){
			printf("%d\t",temp);

			if(x<rows){
				temp = temp - num;
			}else{
				temp = temp + num;
			}
			x++;
		}
	
		printf("\n");
	}
}
