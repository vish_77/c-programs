/*

1	
2	4	
3	6	9	
4	8	12	16	
3	6	9	
2	4	
1

*/

#include<stdio.h>
void main(){
	
	int cols,rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 0;
	
	for(int i = 1; i<2*rows; i++){
		
		if(i<=rows){
			cols = i;
			num++;
		}else{
			cols = cols - 1;
			num--;
		}
		
		int temp = num;
		for(int j = 1; j <= cols; j++){
			printf("%d\t",temp);
			temp+=num;
		}
		printf("\n");
	}
}
