/*

4	
3	3	
2	2	2	
1	1	1	1	
2	2	2	
3	3	
4	

*/

#include<stdio.h>
void main(){
	
	int cols,rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows+1;
	
	for(int i = 1; i<2*rows; i++){
		
		if(i<=rows){
			cols = i;
			num--;
		}else{
			cols = cols - 1;
			num++;
		}
		
		for(int j = 1; j <= cols; j++){
			printf("%d\t",num);
		}
		printf("\n");
	}
}
