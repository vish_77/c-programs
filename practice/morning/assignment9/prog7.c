/*

1	
1	2	3	
1	2	3	4	5	
1	2	3	
1

*/

#include<stdio.h>
void main(){
	
	int cols,rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	

	for(int i = 1; i<2*rows; i++){
		
		if(i<=rows){
			cols = i*2-1;
		}else{
			cols = cols - 2;
		}

		for(int j = 1; j <= cols; j++){
			printf("%d\t",j);
		}
		printf("\n");
	}
}
