/*

1 
3 2 1 
5 4 3 2 1 
3 2 1 
1 

*/

#include<stdio.h>
void main(){
	
	int cols,rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = -1;
	
	for(int i = 1; i<2*rows; i++){
		
		if(i<=rows){
			cols = i*2-1;
			num+=2;
		}else{
			cols = cols - 2;
			num-=2;
		}
		
		int temp = num;
		for(int j = 1; j <= cols; j++){
			printf("%d ",temp);
			temp--;
		}
		printf("\n");
	}
}
