/*

D	
C	D	
B	C	D	
A	B	C	D	
B	C	D	
C	D	
D

*/

#include<stdio.h>
void main(){
	
	int cols,rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = rows+1;
	
	for(int i = 1; i<2*rows; i++){
		
		if(i<=rows){
			cols = i;
			num--;
		}else{
			cols = cols - 1;
			num++;
		}
		
		int temp = num;
		for(int j = 1; j <= cols; j++){
			
			printf("%c\t",temp+64);
			temp ++;
		}
		printf("\n");
	}
}
