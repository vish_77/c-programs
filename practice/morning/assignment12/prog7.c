/*
 
	 	 	 	1	
 	 	3	2	1	
5	4	3	2	1	
 	 	3	2	1	
 	 	 	 	1	

*/


#include<stdio.h>
void main(){

	int rows,space,cols;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 1;

	for(int i = 1; i<2*rows; i++){

		if(i<=rows){
			space = rows*2-2*i;
			cols = i*2-1;
		}else{
			space = 2*i-2*rows;
			cols -=2;
		}
		
		int num = cols;
		for(int sp = 1; sp<=space; sp++){
			printf(" \t");
		}
		
		for(int j = 1; j<=cols; j++){
			printf("%d\t",num);
			num--;
		}
		
		printf("\n");
	}
}
