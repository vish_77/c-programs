/*

5	D	3	B	1	
D	3	B	1	
3	B	1	
B	1	
1	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int temp = rows;

	for(int i = 1; i<= rows; i++){
		int num = temp;
		for(int j = rows; j>=i; j--){

			if(num%2 == 1){
				printf("%d\t",num);
			}else{
				printf("%c\t",num+64);
			}
			num--;
		}
		temp--;
		printf("\n");
	}
}
