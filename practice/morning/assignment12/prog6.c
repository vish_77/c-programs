/*
 
D	C	B	A	
 	e	f	g	
 	 	F	E	
 	 	 	g	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int temp = rows;

	for(int i = 1; i<= rows; i++){

		for(int sp = 1; sp<i; sp++){
			printf(" \t");
		}
		int num = temp;

		for(int j = rows; j>=i; j--){
			
			if(i%2==1){
				printf("%c\t",num+64);
				num--;
			}else{
				printf("%c\t",num+96);
				num++;
			}
		}
		temp++;
		printf("\n");
	}
}
