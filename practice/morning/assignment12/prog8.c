/*
 
 	 	 	1	
 	 	1	b	3	
 	1	b	3	d	5	
1	b	3	d	5	f	7	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	for(int i = 1; i<= rows; i++){

		int num = 1;

		for(int sp = rows; sp>i; sp--){
			printf(" \t");
		}

		for(int j = 1; j<=2*i-1; j++){
			
			if(num % 2 == 0){
				printf("%c\t",num+96);
			}else{
				printf("%d\t",num);
			}
			num++;
		}
		
		printf("\n");
	}
}
