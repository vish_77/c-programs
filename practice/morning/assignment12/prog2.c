/*
 
	 	 	 	1	
 	 	 	1	b	
 	 	1	b	2	
 	1	b	2	d	
1	b	2	d	3	

*/


#include<stdio.h>
void main(){

	int rows,num,ch;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<= rows; i++){

		num = 1;
		ch = num;

		for(int sp = rows; sp>i; sp--){
			printf(" \t");
		}

		for(int j = 1; j<=i; j++){

			if(j%2 == 1){
				printf("%d\t",num);
				num++;
			}else{
				printf("%c\t",ch+96);
			}
			ch++;
		}
		printf("\n");
	}
}
