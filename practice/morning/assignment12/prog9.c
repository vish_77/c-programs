/*
 
g	6	e	4	c	2	a	
 	5	d	3	b	1	
 	 	c	2	a	
 	 	 	1	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	for(int i = 1; i<= rows; i++){

		for(int sp = 1; sp<i; sp++){
			printf(" \t");
		}
		
		int num = 2*rows-2*i+1;
		for(int j = 1; j<=2*rows-2*i+1; j++){
			
			if(i%2 == 1){
				if(j%2==1){
					printf("%c\t",num+96);
				}else{
					printf("%d\t",num);
				}
			}else{
				if(j%2==1)
					printf("%d\t",num);
				else
					printf("%c\t",num+96);
			}

			num--;
		}
		printf("\n");
	}
}
