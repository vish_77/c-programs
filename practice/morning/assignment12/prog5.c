/*
 
D	c	B	a	
a	B	c	D	
F	e	D	c	
b	C	d	E	

*/


#include<stdio.h>
void main(){

	int rows,num1,num2,temp1,temp2;
	printf("Enter rows = ");
	scanf("%d",&rows);
	num1 = rows;
	num2 = 1;

	for(int i = 1; i<= rows; i++){

		temp1 = num1;
		temp2 = num2;

		for(int j = 1; j<=rows; j++){

			if(i%2 == 1){
				if(j%2==1){
					printf("%c\t",temp1+64);
				}else{
					printf("%c\t",temp1+96);
				}
				temp1--;
			}else{
				if(j%2==1){
					printf("%c\t",temp2+96);
				}else{
					printf("%c\t",temp2+64);
				}
				temp2++;
			}
			
		}
		if(i%2==1)
			num1+=2;
		else
			num2++;
			
		printf("\n");
	}
}
