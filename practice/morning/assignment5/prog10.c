/*

 	 	 	1	
 	 	4	9	
 	64	125	216	
2401	4096	6561	10000	

*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows : ");
	scanf("%d",&rows);
	int num = 1;

	for(int i = 1; i<=rows; i++){

		for(int j = rows; j>i; j--){
			printf(" \t");
		}

		

		for(int k = 1; k<=i; k++){
			int sqr = 1;

			for(int l =1; l<=i; l++){
				sqr = sqr * num;
			}

			printf("%d\t",sqr);

			num++;
		}

		printf("\n");
	}
}	
