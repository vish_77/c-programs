/*

 	 	 	 	5	
 	 	 	5	6	
 	 	5	4	3	
 	5	6	7	8	
5	4	3	2	1	


*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){
		
		int temp = rows;
		
		for(int j = rows; j>i ; j--){

			printf(" \t");
		}
			
		for(int k = rows; k>rows - i; k--){


			if( i % 2 == 1){
				printf("%d\t",temp);
				temp--;
			}else{
				printf("%d\t",temp);
				temp++;	
			}
		}

		printf("\n");
	}
}
