/*

 	 	 	 	A	
 	 	 	b	a	
 	 	C	E	G	
 	d	c	b	a	
E	G	I	K	M	

*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = i;
	
		for(int j = rows; j>i ; j--){

			printf(" \t");
		}
			
		for(int k = rows; k>rows - i; k--){
			if(i %2 == 1){
				printf("%c\t",num+64);
				num += 2;
			}else{
				printf("%c\t",num+96);
				num --;
			}
					
		}

		printf("\n");
	}
}
