/*

 	 	 	4	
 	 	4	3	
 	4	3	2	
4	3	2	1

*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		for(int j = rows; j>i ; j--){

			printf(" \t");

		}

		int num =rows;

		for(int k = 1; k<=i; k++){
			printf("%d\t",num);
			num--;
		}

		printf("\n");
	}
}
