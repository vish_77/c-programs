/*

 	 	 	d	
 	 	c	c	
 	b	b	b	
a	a	a	a	

*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){

		int num = rows-i+1;


		for(int j = rows; j>i ; j--){

			printf(" \t");

		}
	

		for(int k = 1; k<=i; k++){
			printf("%c\t",num+96);
		
		}

		printf("\n");
	}
}
