/*

 	 	 	1	
 	 	A	b	
 	1	2	3	
A	b	C	d	

*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){
		
		for(int j = rows; j>i ; j--){

			printf(" \t");

		}
			
	
		for(int k = 1; k<=i; k++){
			if(i % 2 == 1){
				printf("%d\t",k);
			}else{
				if(k % 2 == 1)
					printf("%c\t",k+64);
				else
					printf("%c\t",k+96);
			}
	
		}

		printf("\n");
	}
}
