/* WAP to print addition of factorials of two given numbers from user
   IP : num1 = 4	//24
   	num2 = 5	//120

   OP : Addition of factorials of 4 and 5 is 144

*/

#include<stdio.h>
void main(){

	int num1,num2,fact1 = 1,fact2 = 1;
	
	printf("Enter num1 = ");
	scanf("%d",&num1);

	printf("Enter num2 = ");
	scanf("%d",&num2);

	int temp1 = num1;
	int temp2 = num2;

	while(num1 >=1){

		fact1 = fact1*num1;
		num1--;
	}

	while(num2 >= 1){

		fact2 = fact2 * num2;
		num2--;
	}

	printf("Addition of factorials of %d and %d is %d\n",temp1,temp2,fact1+fact2);
}


 	
