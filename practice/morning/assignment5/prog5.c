/*

 	 	 	D	
 	 	c	D	
 	B	c	D	
a	B	c	D	


*/


#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<=rows; i++){
		
		char ch = 'A';
		int num = 1;
		for(int j = rows; j>i ; j--){

			printf(" \t");
			ch++;
			num ++;
		}
			
	
		for(int k = 1; k<=i; k++){
			if(num % 2 == 0){
				printf("%c\t",ch);
			}else{
				printf("%c\t",ch+32);
			}
			ch ++;
			num ++;		
		}

		printf("\n");
	}
}
