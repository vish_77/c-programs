/*

7	6	5	4	3	2	1	
 	5	4	3	2	1	
 	 	3	2	1	
 	 	 	1	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	for(int i = 1; i<= rows; i++){
		
		int num = rows*2-2*i+1;
		for(int sp = 1; sp<i; sp++){
			printf(" \t");
		}

		for(int j = 1; j <= rows*2-2*i+1; j++){
			printf("%d\t",num);
			num--;
		}

		printf("\n");
	}
}



