/*

4	3	2	1	2	3	4	
 	3	2	1	2	3	
 	 	2	1	2	
 	 	 	1	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = rows;

	for(int i = 1; i<= rows; i++){
		
		int x =1;
		for(int sp = 1; sp<i; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j <= rows*2-2*i+1; j++){
			if(x < rows){
				printf("%d\t",num);
				num--;
			}else{
				printf("%d\t",num);
				num ++;
			}
			x++;
		}
		num -= 2;
		printf("\n");
	}
}



