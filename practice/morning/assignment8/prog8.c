/*

D	C	B	A	B	C	D	
 	c	b	a	b	c	
 	 	B	A	B	
 	 	 	a	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num = rows;

	for(int i = 1; i<= rows; i++){
		
		int x =1;
		for(int sp = 1; sp<i; sp++){
			printf(" \t");
			x++;
		}

		for(int j = 1; j <= rows*2-2*i+1; j++){
			if(i % 2 == 1){

				if(x < rows){
					printf("%c\t",num+64);
					num--;
				}else{
					printf("%c\t",num+64);
					num ++;
				}
			}else{
				if(x < rows){
					printf("%c\t",num+96);
					num--;
				}else{
					printf("%c\t",num+96);
					num ++;
				}
			}
			x++;
		}
		num -= 2;
		printf("\n");
	}
}



