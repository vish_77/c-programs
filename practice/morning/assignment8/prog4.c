/*

1	2	3	4	3	4	3
 	2	3	4	3	4
 	 	3	4	3
 	 	 	4

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);

	for(int i = 1; i<= rows; i++){
		
		int num = i;
		for(int sp = 1; sp<i; sp++){
			printf(" \t");
		}

		for(int j = 1; j <= rows*2-2*i+1; j++){
			if (num < rows){
				printf("%d\t",num);
				num ++;
			}else{
				printf("%d\t",num);
				num --;
			}
		}

		printf("\n");
	}
}



