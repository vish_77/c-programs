/*

A0	B1	C2	D3	E4	F5	G6	
 	H2	I3	J4	K5	L6	
 	 	M4	N5	O6	
 	 	 	P6	

*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	int num = 1;
	int x = 0;

	for(int i = 1; i<= rows; i++){

		for(int sp = 1; sp<i; sp++){
			printf(" \t");
		}
		int temp = x;
		for(int j = 1; j <= rows*2-2*i+1; j++){
			printf("%c%d\t",num+64,temp);
			num ++;
			temp++;
		}

		x += 2;
		printf("\n");
	}
}



