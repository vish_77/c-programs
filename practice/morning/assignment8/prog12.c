/*

1	3	5	7	9	7	5	3	1	
 	9	7	5	3	5	7	9	
 	 	3	5	7	5	3	
 	 	 	7	5	7	
 	 	 	 	5	
*/


#include<stdio.h>
void main(){

	int rows;
	printf("Enter rows = ");
	scanf("%d",&rows);
	
	int num1 = 1;
	for(int i = 1; i<= rows; i++){
		
		int x = 1;
		for(int sp = 1; sp<i; sp++){
			printf(" \t");
			x++;
		}
		
		int num = num1;
		for(int j = 1; j <= rows*2-2*i+2; j++){
			
			if(i % 2 == 1){

				if (x < rows){
					printf("%d\t",num);
					num += 2;
				}else if(x == rows){
					num1 = num;
				}
				else{
					printf("%d\t",num);
					num -= 2;
				}
			}else{
				  if (x < rows){
                                        printf("%d\t",num);
                                        num -= 2;
                                }else if(x == rows){
                                        num1 = num;
                                }
                                else{
                                        printf("%d\t",num);
                                        num += 2;
                                }
			}
			x++;
		}

		printf("\n");
	}
}



