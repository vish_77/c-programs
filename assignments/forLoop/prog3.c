// WAP to rint all even numbers in reverse order and odd numbers in the standard way. Both separately. Within a range.


#include<stdio.h>
void main(){
	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	printf("odd numbers are\n");
	for(int i = 1;i<= num;i++){
		if(i % 2 != 0){
			printf("%d\n",i);
		}
	}

	printf("even numbers are\n");
	for(int i = num; i>=0;i--){
		if(i % 2 == 0){
			printf("%d\n",i);
		}
	}
}

			
