/*
 
WAP to print the numbers in a given range and their multiplicative inverse.
Suppose x is a number then its multiplicative inverse or reciprocal is 1/x.
The expected output for range 1 - 5
1 = 1
2 = 1/2 i.e 0.5
3 = 1/3 i.e 0.33
4 = 0.25
5 = 0.20

*/


#include<stdio.h>
void main(){
	float num1,num2;
	printf("Enter starting value = ");
	scanf("%f",&num1);
	printf("Enter end value = ");
	scanf("%e",&num2);

	for(float i = num1;i<=num2;i++){
		printf("%f\n",1/i);
	}

			
}
