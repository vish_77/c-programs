// WAP TO take the number input and print all the factors of that number

#include<stdio.h>
void main(){
	int num;
	printf("Enter a number = ");
	scanf("%d",&num);
	
	printf("Factors of %d are\n",num);
	for(int i = 1;i<= num;i++){
		if(num % i == 0){
			printf("%d\n",i);
		}
	}
}
