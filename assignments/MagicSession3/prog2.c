
/*
 
   WAP that dynamically allocates a 1-D Array of marks, takes value from the user, and
   prints it, [use malloc()]
   
*/

#include<stdio.h>
#include<stdlib.h>

void main(){

	int size;
	printf("Enter size of array = ");
	scanf("%d",&size);

	float *ptr = (float*)malloc(size*(sizeof(float)));
	
	printf("Enter array elements:\n");
	for(int i = 0; i<size; i++){

		scanf("%f",ptr+i);
	}

	printf("Array elements are:\n");
	for(int i = 0; i<size; i++){
		printf("%f\n",*(ptr+i));
	}
}
