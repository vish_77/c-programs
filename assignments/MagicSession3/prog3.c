/*
 
   WAP that dynamically allocates a 2-D Array of integer, takes value from the user, and
   prints it, [use malloc()]
   
*/

#include<stdio.h>
#include<stdlib.h>

void main(){

	int rows,cols;
	printf("Enter rows = ");
	scanf("%d",&rows);

	printf("Enter cols = ");
	scanf("%d",&cols);

	
	
	printf("%ld\n",rows*cols*sizeof(int));
	printf("%ld\n",rows*cols*sizeof(int));

	int *ptr = (int*)malloc(rows*cols*sizeof(int));
	
	printf("Enter elements of array :\n");
	for(int i = 0; i< rows*cols; i++){
		scanf("%d",ptr+i);
	}

	printf("array elements are :\n");
	for(int i = 0; i<rows*cols; i++){
		printf("%d\n",*(ptr+i));
	}
}
