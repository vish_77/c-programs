/*
 
   WAP that dynamically allocates a 3-D Array of integer, takes value from the user, and
   prints it, [use malloc()]
   
*/

#include<stdio.h>
#include<stdlib.h>

void main(){

	int plan,rows,cols;
	printf("Enter plan : ");
	scanf("%d",&plan);

	printf("Enter rows : ");
	scanf("%d",&rows);

	printf("Enter cols : ");
	fscanf(stdin,"%d",&cols);

	int arr[plan][rows][cols];

	int *ptr = (int*)malloc(sizeof(arr));
	
	printf("Enter array elements\n");
	for(int i = 0; i<sizeof(arr)/sizeof(int); i++){
		scanf("%d",ptr+i);
	}

	printf("Array elemtns are :\n");
	for(int i = 0; i<sizeof(arr)/sizeof(int); i++){
		printf("%d\n",*ptr+i);
	}
}
