/*
D	C	B	A	
e	d	c	b	
F	E	D	C	
g	f	e	d	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i=0;i<rows;i++){
		for(int j=cols;j>=1;j--){
			if(i%2 == 0){
				printf("%c\t",j+i+64);
			}else{
				printf("%c\t",j+i+96);
			}
		}
		printf("\n");
	}
}
