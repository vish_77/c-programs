/*
D4	C3	B2	A1	
A1	B2	C3	D4	
D4	C3	B2	A1	
A1	B2	C3	D4	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);
	
	for(int i=0;i<rows;i++){
		if(i%2==0){

			for(int j=cols;j>=1;j--){
				printf("%c%d\t",j+64,j);
			}
		}else{
			for(int j=1;j<=cols;j++){
				printf("%c%d\t",j+64,j);
			}
		}
		printf("\n");
	}
}
