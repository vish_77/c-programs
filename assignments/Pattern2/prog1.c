/*
4	3	2	1	
5	4	3	2	
6	5	4	3	
7	6	5	4	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i=0;i<rows;i++){
		for(int j=cols;j>=1;j--){
			printf("%d\t",j+i);
		}
		printf("\n");
	}
}

