/*
=	=	=	=	
$	$	$	$	
=	=	=	=	
$	$	$	$	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			if(i%2==0){
				printf("=\t");
			}else{
				printf("$\t");
			}
		}
		printf("\n");
	}
}
