/*
3	2	1	
c	b	a	
3	2	1	
c	b	a	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);

	for(int i=0;i<rows;i++){
		for(int j=cols;j>=1;j--){
			if(i%2 == 0){
				printf("%d\t",j);
			}else{
				printf("%c\t",j+96);
			}
		}
		printf("\n");
	}
}
