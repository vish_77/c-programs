/*
18	16	14	
12	10	8	
6	4	2	

*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);
	
	int num = rows*cols*2;
	for(int i=rows;i>=1;i--){
		for(int j=cols;j>=1;j--){
			printf("%d\t",num);
			num=num-2;
		}
		printf("\n");
	}
}
