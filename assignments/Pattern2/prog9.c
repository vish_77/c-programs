/*

1	3	8	
15	24	35	
48	63	80	

*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);
	
	int num = 1;
	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			if(i==0 && j==0){
				printf("1\t");
			}else{
				printf("%d\t",num*num-1);
			}
			num++;
		}
		printf("\n");
	}
}
