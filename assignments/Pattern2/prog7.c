/*
1	2	3	4	
25	36	49	64	
9	10	11	12	
169	196	225	256	
	
*/


#include<stdio.h>
void main(){
	
	int rows,cols;
	printf("Enter no of rows = ");
	scanf("%d",&rows);
	printf("Enter no of cols = ");
	scanf("%d",&cols);
	
	int num = 1;
	for(int i=0;i<rows;i++){
		for(int j=0;j<cols;j++){
			if(i%2==0){
				printf("%d\t",num);
			}else{
				printf("%d\t",num*num);
			}
			num++;
		}
		printf("\n");
	}
}
