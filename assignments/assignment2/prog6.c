/*
 * Take an input number from the user and count the no of digits.
Input = 2534
Output = Number of Digits in 2534 is 4
*/

#include<stdio.h>
void main(){
	int count = 0;
	int num;
	printf("Enter a number\n");
	scanf("%d",&num);
	int temp = num;

	while(num != 0){
		count ++ ;
		num = num / 10;
	}
	printf("Number of Digits in %d is %d\n",temp,count);
}
