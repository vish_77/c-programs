/*
 *

Program 5:
WAP to print all even numbers in reverse order and odd numbers in the standard way.
Both separately. Within a range.
Input: start - 2
End - 9
Output:
8 6 4 2
3 5 7 9
*/


#include<stdio.h>
void main(){
	int num1,num2;
	printf("Enter num 1\n");
	scanf("%d",&num1);
	printf("ENter num 2\n");
	scanf("%d",&num2);

	for(int i = num1;i<= num2;i++){
		if(i%2 == 1){
			printf("%d ",i);

		}
	}
	printf("\n");
	
	for(int i = num2;i>=num1;i--){
		if(i % 2 == 0){
			printf("%d ",i);
			}
	}
	printf("\n");
}

