/*
 *Program 8: Take input from the user and print the product of digits
Input = 134
Output = product of digits is 12i
*/

#include<stdio.h>
void main(){
	int mult = 1,rem;
	int num;
	printf("Enter a number\n");
	scanf("%d",&num);

	while(num != 0){
		rem = num % 10;
		mult = mult * rem;
		num = num / 10;
	}
	printf("product of Digits is %d\n",mult);
}
