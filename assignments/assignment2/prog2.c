// WAP to print the addition of 1 to 10 with 10 to 1
// o/p
// 1+10 =11
// 2+9=11
// 3+8=11
//.
//.
//10 +1=11

#include<stdio.h>
void main(){
	int a = 1,b=10;
	int temp = b;

	for(int i=a;i<=b;i++){
		printf("%d + %d = %d\n",i,temp,i+temp);
		temp --;
	}
}

