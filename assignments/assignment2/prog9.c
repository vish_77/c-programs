/*
 Program 9: Take an input number from the user and print the number in reverse
Input: 120654
Output: 456021
*/

#include<stdio.h>
void main(){
	int rev = 0,rem;
	int num;
	printf("Enter a number\n");
	scanf("%d",&num);
	int temp = num;
	while(num != 0){
		rem = num % 10;
		rev = rev * 10 + rem;
		num = num / 10;
	}
	printf("%d\n",rev);
}
