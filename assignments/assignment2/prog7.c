/*
 *Take an input number from the user and print the sum of digits.
Input = 1234
Output = sum of digits is 10
*/

#include<stdio.h>
void main(){
	int sum = 0,rem;
	int num;
	printf("Enter a number\n");
	scanf("%d",&num);
	int temp = num;

	while(num != 0){
		rem = num % 10;
		sum = sum + rem;
		num = num / 10;
	}
	printf("sum of Digits is %d\n",sum);
}
