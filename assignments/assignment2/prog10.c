/*Program 10 :
Take a number from the user and print the Fibonacci series up to that number.
Input : 10
Output 0 1 1 2 3 5 8
*/

#include<stdio.h>
void main(){
	int intput,num1 = 1,num2 =1,num3=0;
	printf("Enter a number\n");
	scanf("%d",&intput);
		
	while(num3<intput){
		
		
		printf("%d ",num3);
		num1 = num2;
		num2 = num3;
		num3 = num1+num2;
		
	}
	printf("\n");
}
