/* WAP to print the divisors and cout of divisors of the entered num 
 * I/O 25
 *
 * O/P the divisors are 1 2 3 5 15
 * 	the cout of divisor 4
 * 	addtion of divisor is 
 */

#include<stdio.h>
void main(){
	int num,cout = 0,add = 0;
	printf("ENter a number\n");
	scanf("%d",&num);

	for(int i=1; i<= num;i++){
		if(num%i==0){
			printf("the divisors are %d\t\n",i);
			cout ++;
			add = add + i;
		}
	}
	printf("addition of divisor is %d\n",add);
	printf("The cout of divisor %d",cout);
}	
