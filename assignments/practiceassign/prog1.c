/** WAP to check weather the input is leap year or not
 */

#include<stdio.h>
void main(){
	int a;
	printf("Enter a year\n");
	scanf("%d",&a);

	if(a%4==0 || a%400 == 0){
		printf("%d is leap year\n",a);
	}else{
		printf("%d is not a leap year\n",a);
	}
}
