// WAP to print lagest element in 2D array

#include<stdio.h>
void main(){

	int arr[3][3] = {1,2,3,14,5,6,7,8,9};
	int num = arr[0][0];

	for(int i = 0; i<3; i++){
		for(int j = 0; j<3; j++){

			if(num < arr[i][j]){
				num = arr[i][j];
			}

		}

	}

	printf("Largest number = %d\n",num);
}

