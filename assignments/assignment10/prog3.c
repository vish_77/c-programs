// WAP to create 3D array and print it using pointer and in 1 for loop

#include<stdio.h>
void main(){

	int plane,rows,cols;

	printf("Enter plane = ");
	scanf("%d",&plane);

	printf("Enter rows = ");
	scanf("%d",&rows);

	printf("Enter cols = ");
	scanf("%d",&cols);

	int arr[plane][rows][cols];

	int arrsize = sizeof(arr)/sizeof(int);
	
	printf("Enter arr elements = \n");
	for(int p = 0; p<plane; p++){
		for(int i = 0; i<rows; i++){
			for(int j = 0; j<cols; j++){
				scanf("%d",&arr[p][i][j]);
			}
		}
	}

	printf("Array elements are = \n");
	for(int p = 0; p<plane; p++){

		printf("plane = %d\n",p);
		for(int i = 0; i<rows; i++){
			for(int j = 0; j<cols; j++){
				printf("%d ",*(*(*(arr+p)+i)+j));
			}
			printf("\n");
		}
	}

	printf("3D array in 1 for loop:\n");

	for(int i = 0; i<arrsize; i++){

		printf("%d\n",*(*(*arr))+i);
	}
}


	
			

	
