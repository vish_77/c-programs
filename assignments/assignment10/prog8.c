//WAP to print largest element in 3D array

#include<stdio.h>
void main(){

	int arr[][2][2] = {{1,2,22,4},{5,6,7,8}};
	int num = arr[0][0][0];

	for(int i = 0; i<2; i++){
		for(int j = 0; j<2; j++){
			for(int k = 0; k<2; k++){

				if(num < arr[i][j][k]){
					num = arr[i][j][k];
				}
			}
		}
	}

	printf("largest element = %d\n",num);;
}
