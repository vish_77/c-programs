// Wap to create 1D array and print it using ptr


#include<stdio.h>
void main(){

	int size;
	printf("Enter size = ");
	scanf("%d",&size);

	int arr[size];

	for(int i = 0; i<size; i ++){
		scanf("%d",&arr[i]);
	}

	printf("array is :\n");
	for(int i = 0; i<size; i++){
		printf("%d\n",*(arr+i));
	}
}

