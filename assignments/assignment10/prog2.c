// WAP to create 2D array and print it using pointer and print in 1 for loop

#include<stdio.h>
void main(){

	int rows,cols;
	printf("Enter rows = ");
	scanf("%d",&rows);

	printf("Enter cols = ");
	scanf("%d",&cols);

	int arr[rows][cols];
	
	printf("Enter arr elements = \n");
	for(int i = 0; i<rows; i++){
		for(int j = 0; j<cols; j++){
			scanf("%d",&arr[i][j]);
		}
		printf("\n");
	}

	printf("Array elements are = \n");
	for(int i = 0; i<rows; i++){
		for(int j = 0; j<cols; j++){
			printf("%d ",*(*(arr+i)+j));
		}
		printf("\n");
	}

	printf("array in 1 for loop\n");

	for(int i = 0; i<sizeof(arr)/sizeof(int); i++){
		printf("%d\n",*(*arr)+i);
	}
}	


	
			

	
