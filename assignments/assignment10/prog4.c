//WAP to print diagonal sum of 2D array

#include<stdio.h>

int arrAdd1(int (*arr)[3], int rows, int cols){

        int sum = 0;
        for(int i = 0; i<rows; i++){
                for(int j= 0; j<cols; j++){

                        if (((i+j) % 2) == 0)
                        sum = sum + *(*(arr+i)+j);
                }
        }

        return sum;
}

int arrAdd2(int (*arr)[3],int arrSize){

        int sum = 0;
        for(int i = 0;i<arrSize; i++){
                if(i % 2 == 0)
                        sum = sum + *(*arr+i);
			
        }
        return sum;

}
void main(){
        int arr[3][3] = {1,2,3,4,5,6,7,8,9};
        int arrSize = sizeof(arr)/sizeof(int);

        int sum1 = arrAdd1(arr,3,3);
        int sum2 = arrAdd2(arr,arrSize);

        printf("sum of diag ele = %d\n",sum1);
        printf("sum of diag ele = %d\n",sum2);

}

