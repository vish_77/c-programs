//WAP to print diagonal sum 3D array

#include<stdio.h>

int printsum(int (*ptr)[3][3],int plane, int rows, int cols){
	
	int sum = 0;
	for(int i = 0; i<plane; i++){
		for(int j = 0; j<rows; j++){
			for(int k = 0; k<cols; k++){

				if((j+k) % 2 == 0){
					sum = sum + *(*(*(ptr+i)+j)+k);
				}
			}
		}
	}

	return sum;
}

int printsum1(int (*ptr)[3][3],int arrsize){

	int sum = 0;

	for(int i = 0; i<arrsize; i++){

		if( (i % 2 == 0) && (i <= arrsize/2)){
			sum = sum + *(*(*ptr)+i) ; 
		}else if ((i % 2 == 1) && (i >= arrsize/2)){
			sum = sum + *(*(*ptr)+i);
		}
	}
	
	return sum;
}


void main(){

	int arr[2][3][3] = {{1,2,3,4,5,6,7,8,9},{10,11,12,13,14,15,16,17,18}};
	int arrsize = sizeof(arr)/sizeof(int);

	int sum = printsum(arr,2,3,3);
	int sum1 = printsum1(arr,arrsize);

	printf("sum of diagonal ele = %d\n",sum);	
	printf("sum of diagonal ele = %d\n",sum1);
	
}
