/*

* - - -
- * - -
- - * -
- - - *

*/

#include<stdio.h>
void main(){
	int rows,cols;
	printf("ENter rows = ");
	scanf("%d",&rows);
	printf("ENter cols = ");
	scanf("%d",&cols);

	for(int i = 1;i<=rows;i++){
		for(int j = 1;j<=cols;j++){
			if(i == j){
				printf("* ");
			}else{
				printf("- ");
			}
		}
		printf("\n");
	}
}
