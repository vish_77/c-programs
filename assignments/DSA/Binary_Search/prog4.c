/*

   Given a sorted array with a Ditinct element, which is formed by rotating a sorted array k times.
   Given an X element.
   Find its index of it in the given array.

   [Note: k value not given]
*/


#include<stdio.h>

int EleSearch(int arr[], int size, int ele){

	int start = 0;
	int end = size - 1;
	int flag = 0;

	while(start <= end){

		if(flag == 0 && arr[start] < arr[start + 1]){
			
			start++;
			if(arr[start] > arr[start - 1] && arr[start] > arr[start + 1]){
				flag = 1;
				
				if(ele <= arr[end]){
                                	start = start+1;
                        	}

                      	 	if(ele > arr[end]){
                                	end = start;
                                	start = 0;
		                }
			}

		}else{

			int mid = (start + end)/2;

			if(arr[mid] == ele){
				return mid;	
			}

			if(ele > arr[mid]){
				start = mid + 1;
			}

			if(ele < arr[mid]){
				end = mid - 1;
			}
		}		

	}
	return -1;
}

void main(){

        int arr[] = {10,11,13,15,2,4,6,8};

        int size = sizeof(arr)/sizeof(int);

        for(int i = 0; i<size; i++){
                printf("%d  ",arr[i]);
        }
        printf("\n");

        int ele;
        printf("Enter an element = ");
        scanf("%d",&ele);

        int ret = EleSearch(arr,size,ele);

	if(ret == -1){
                printf("Element not found\n");
        }else{
                printf("Element found at index = %d\n",ret);
        }

}
	
