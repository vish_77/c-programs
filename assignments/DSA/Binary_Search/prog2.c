/*

   given a sorted array arr.
   Find the first occurrence of K.
   If found then return index ele return -1.
*/

#include<stdio.h>

int FirstOcc(int arr[], int size, int k){

	int start = 0;
	int end = size -1;
	
	while(start <= end){

		int mid = (start+end)/2;
		printf("%d\n",mid);

		if(arr[mid] == k){

			while(arr[mid -1] == arr[mid]){

				mid --;
			}
			return mid;
		}

		if(k > arr[mid]){
			start = mid + 1;
		}
		if(k < arr[mid]){
			end = mid - 1;
		}
	}
	return -1;

}

void main(){

	int arr[] = {2,3,3,4,9,10,11,14,14,14,15,18};
	
	for(int i = 0; i< sizeof(arr)/sizeof(int); i++){
		printf("%d   ",arr[i]);
	}

	int k;
	printf("Enter an element to search = ");
	scanf("%d",&k);
	
	int ret = FirstOcc(arr,sizeof(arr)/sizeof(int),k);

	if(ret == -1){
		printf("Element not found\n");
	}else{
		printf("Element found at index = %d\n",ret);
	}
}
