/*

   given a sorted array with DISTINCT elements.
   Find the floor of no k in the array.

   floor => greatest elements >= k

*/

#include<stdio.h>

int Floor(int *arr,int size, int k){

	int start = 0;
	int end = size - 1;
	int ret=0;

	while(start <= end){

		if(start == end && arr[start] == k){
			return arr[start];
		}else{
			int mid = (start+end)/2;

			if(arr[mid] == k){
				return arr[mid];
			}
			if(k > arr[mid]){
				start = mid + 1;
				ret = arr[mid];
			}
			if(k < arr[mid]){
				end = mid - 1;
			}
		}
	}
	return ret;
}

void main(){

	int arr[] = {2,3,6,9,10,11,14,18};

	int ele;
	printf("Enter element = ");
	scanf("%d",&ele);

	int size = sizeof(arr)/sizeof(int);

	int ret = Floor(arr,size,ele);

	printf("floor = %d\n",ret);
}
