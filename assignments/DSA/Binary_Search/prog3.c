/*

   Given a sorted array with a Ditinct element, which is formed by rotating a sorted array k times.
   Given an X element.
   Find its index of it in the given array.

   [Note: k value not given]
*/

#include<stdio.h>

int EleSearch(int arr[], int size, int ele){

	int start = 0;
	int end = size - 1;
	int mid;

	for(int i = 0; i< size; i++){

		if(arr[i] > arr[i+1]){
			mid = i;
			break;
		}
	}
	
	if(ele <= arr[end]){
		start = mid + 1;
	}
	if(ele > arr[end]){
		end = mid;
	}

	while(start <= end){

		int mid1 = (start+end)/2;

		if(arr[mid1] == ele){
			return mid1;
		}
		if(ele < arr[mid1]){
			end = mid1 - 1;
		}
		if(ele > arr[mid1]){
			start = mid1 + 1;
		}
	}
	return -1;
}

void main(){

	int arr[] = {10,11,13,15,2,4,6,8};
	
	int size = sizeof(arr)/sizeof(int);

	for(int i = 0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");

	int ele;
	printf("Enter an element = ");
	scanf("%d",&ele);
	
	int ret = EleSearch(arr,size,ele);
	if(ret == -1){
		printf("Element not found\n");
	}else{
		printf("Element found at index = %d\n",ret);
	}

}
