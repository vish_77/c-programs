/*

   Reverse in range:

   Given an array 
   Also given two integers B and C
   Reverse thhe array in the given range [B,C]
*/

#include<stdio.h>

void Reverse(int arr[],int size, int B, int C){

	int temp;
	while(B<C){
		
		temp = arr[C];
		arr[C] = arr[B];
		arr[B] = temp;

		B++;
		C--;
	}
}


void main(){

	int arr[] = {1,2,3,4,5,6,7,8,9};

	int B,C;
	printf("Enter start = ");
	scanf("%d",&B);

	printf("Enter end = ");
	scanf("%d",&C);

	int size = sizeof(arr)/sizeof(int);

	for(int i = 0; i<size; i++){

		printf("%d   ",arr[i]);
		
	}
	printf("\n");

	if(B<C && B>0 && B<size && C>0 && C<size){

		Reverse(arr,size,B,C);
	}else
		printf("Enter appropriate ragne\n");

	for(int i = 0; i< size; i++){

		printf("%d   ",arr[i]);
		
	}
	printf("\n");
}

