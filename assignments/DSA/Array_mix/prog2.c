/*

   Good Pair
   given an array A and integer B.
   A pair (i,j) in thhe array is good pair if i != j and (A[i] + A[j] == B).
   Check if any good pair exist or not.
   Return 1 if good pair exist otherwiese return 0.

*/

#include<stdio.h>

int GoodPair(int *arr,int size,int B){

	for(int i = 0; i< size; i++){

		for(int j = i+1; j<size; j++){

			if(arr[i] + arr[j] == B)
				return 1;
		}
	}
	return -1;
}

void main(){

	int arr[] = {1,2,3,4};

	int B;
	printf("Enter a number = ");
	scanf("%d",&B);

	int size = sizeof(arr)/sizeof(int);

	int ret = GoodPair(arr,size,B);

	if(ret == 1){
		printf("Good pair exit \n");
	}else
		printf("Good pair dosn't exist\n");
}
