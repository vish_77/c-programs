/*

   Reverse the array:

   you are given a constant array A.
   You are required to return another array which is the reversed form of the input array.
   Return and integer array.

*/

#include<stdio.h>

void Reverse(int arr1[],int arr2[], int size){

	int i = 0;
	int j = size-1;

	while(i<size){

		arr2[j] = arr1[i];
		i++;
		j--;
	}
}

void main(){

	int arr1[] = {10,2,3};
	int size = sizeof(arr1)/sizeof(int);

	int arr2[size];

	for(int i=0; i<size; i++){
		printf("%d  ",arr1[i]);
	}
	printf("\n");

	Reverse(arr1,arr2,size);

	for(int i=0; i<size; i++){
		printf("%d  ",arr2[i]);
	}
	printf("\n");
}
