/*

   Minor Diagonal sum:

   -you have given a N X N integer matrix.
   -you have to find the sum of all the minor diagonal elements of A.
   -The minor diagonal of a matrix A is a collection of elements A[i,j] such that i+j = M + 1.
   -REturn integer denoting the sum of main diagonal elements.

*/

#include<stdio.h>

int rows;

int Sum(int (*arr)[rows]){

	int sum = 0;
	for(int i = 0; i<rows; i++){
		for(int j = 0; j<rows; j++){

			if(i+j+1 == rows)
				sum += arr[i][j];
		}
	}
	return sum;
}


void main(){

	printf("Enter rows = ");
	scanf("%d",&rows);
	int cols = rows;

	int arr[rows][cols];

	printf("Enter elements = \n");
	for(int i = 0; i<rows ; i++){
		for(int j = 0; j<cols; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	for(int i = 0; i<rows ; i++){
		for(int j = 0; j<cols; j++){
			printf("%d  ",arr[i][j]);		

		}
		printf("\n");
	}

	printf("Sum of main diagonal is = %d\n",Sum(arr));
}









