/*

   Given an integer array of size of N.
   Count the number of elements having at least 1 element greater than itself.

*/

#include<stdio.h>

int Greater(int arr[], int size, int num){

	int count = 0;
	for(int i = 0; i<size; i++){

		if(arr[i] > num)
			count ++;
	}
	if(count == 0)
		return -1;

	return count;
}

void main(){

	int arr[] = {6,4,5,7,3,-2,0,9,1};
	int size = sizeof(arr)/sizeof(int);

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);

	int ret = Greater(arr,size,num);

	if(ret != -1)

		printf("Count of greater elements = %d\n",ret);
	else
		printf("Not any element is greater than %d\n",num);
}



