/*

   Main Diagonal sum:

   -you have given a N X N integer matrix.
   -you have to find the sum of all the main diagonal elements of A.
   -The main diagonal of fa matrix A is a collection of elements A[i,j] such that i == j.
   -REturn integer denoting the sum of main diagonal elements.

*/

#include<stdio.h>

int rows;

int Sum(int (*arr)[rows]){

	int sum = 0;

	for(int i = 0; i<rows; i++){

		sum += arr[i][i];
	}
	return sum;
}


void main(){

	printf("Enter rows = ");
	scanf("%d",&rows);
	int cols = rows;

	int arr[rows][cols];

	printf("Enter elements = \n");
	for(int i = 0; i<rows ; i++){
		for(int j = 0; j<cols; j++){
			scanf("%d",&arr[i][j]);
		}
	}
	
	for(int i = 0; i<rows ; i++){
		for(int j = 0; j<cols; j++){
			printf("%d  ",arr[i][j]);		

		}
		printf("\n");
	}

	printf("Sum of main diagonal is = %d\n",Sum(arr));
}









