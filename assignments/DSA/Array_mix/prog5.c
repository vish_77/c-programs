/*

   Array Rotation:

   Given an integer array A of size N and integer B, you have to return the same array
   after rotating it B times towards the right.
   Return the array A after rotating it B times to the right.
*/


#include<stdio.h>

void Rotate(int arr[], int size, int B){

	while(B){

		int temp = arr[0];

		for(int i = 0; i<size-1; i++){

			int x = temp;
			temp = arr[i+1];
			arr[i+1] = x;
		}
		arr[0] = temp;

		B--;
	}
}

void main(){

	int arr[] = {1,2,3,4};
	int size = sizeof(arr)/sizeof(int);
	
	int B;
	printf("Enter an integer to rotate array = ");
	scanf("%d",&B);

	Rotate(arr,size,B); 
	for(int i=0; i<size; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
}
