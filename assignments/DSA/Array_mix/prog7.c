/*

   Column Sum:

   You are given 2D integer matrix, and return a 1D integer array contaning column wise sums of 
   the original matrix.
   Return an array contanning column-wise sums of the original matrix.

*/

#include<stdio.h>

int rows;
int cols;

void Sum(int (*arr)[cols],int arr1[]){
	

	for(int j = 0; j<cols; j++){

		int sum = 0;
		for(int i = 0; i< rows; i++){
			sum += arr[i][j];
		}

		arr1[j] = sum;
	}
}


void main(){

	printf("Enter number of rows = ");
	scanf("%d",&rows);

	printf("Enter number of cols = ");
	scanf("%d",&cols);

	int arr[rows][cols];
	int arr1[cols];

	for(int i = 0; i<rows; i++){
		for(int j = 0; j<cols; j++){
			printf("Enter a element = ");
			scanf("%d",&arr[i][j]);
		}
	}

	for(int i = 0; i<rows; i++){
		for(int j = 0; j<cols; j++){

			printf("%d  ",arr[i][j]);
		}
		printf("\n");
	}

	Sum(arr,arr1);

	for(int i=0; i<cols; i++){
		printf("%d  ",arr1[i]);
	}
	printf("\n");

}
