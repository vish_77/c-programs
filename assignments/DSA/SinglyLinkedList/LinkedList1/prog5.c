/*
 
   Print the addition of the first and last node data from the above code.

*/  


#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int num;
	struct Demo *next;
}demo;

demo *head = NULL;

void NextNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter a number = ");
	scanf("%d",&newnode->num);

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		demo *temp = head;
		while(temp -> next != NULL){
			temp = temp->next;
		}
		temp -> next = newnode;
	}
}

int Add(){

	demo *temp = head;
	int sum = 0;
	
	while(temp != NULL){

		if(temp -> next == NULL)
			sum = head -> num + temp -> num;

		temp = temp -> next;
	}
	return sum;
}

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	printf("sum of first and last node data = %d\n",Add());
}
