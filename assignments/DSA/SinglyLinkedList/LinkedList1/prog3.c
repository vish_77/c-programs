/*
 
   Q.3] WAP for the LinkedList of Festvals in India. 
   	Take input from the user in the LinkedList & Print its data.

   Q.4] WAP to count the number of festival nodes in the above program
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Festival{

	char festival[20];
	int guest;
	struct Festival *next;
}fest;

fest *head = NULL;

void NextNode(){

	fest *newnode = (fest*)malloc(sizeof(fest));

	printf("Enter name of festival = ");
	fgets(newnode -> festival,20,stdin);

	printf("Enter number of guest = ");
	scanf("%d",&newnode -> guest);

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		fest *temp = head;
		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	fest *temp = head;

	while(temp != NULL){
		printf("%s",temp -> festival);
		printf("%d\n",temp -> guest);
		
		temp = temp -> next;
	}
}

int NodeCount(){

	fest *temp = head;
	int count = 0; 
	while(temp != NULL){
		temp = temp -> next;
		count++;
	}
	return count;
}
		

void main(){
	
	NextNode();
	NextNode();
	NextNode();

	PrintLL();

	printf("Node count is = %d\n",NodeCount());
}

