/*
 
   Print minimum integer data from the node.

*/  


#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int num;
	struct Demo *next;
}demo;

demo *head = NULL;

void NextNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter a number = ");
	scanf("%d",&newnode->num);

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		demo *temp = head;
		while(temp -> next != NULL){
			temp = temp->next;
		}
		temp -> next = newnode;
	}
}

int Min(){

	demo *temp = head;
	int num = temp -> num;
	
	while(temp != NULL){
		
		if(num > temp -> num){
			num = temp -> num;
		}
		temp = temp -> next;
	}
	return num;
}	

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	printf("minimum number from the node = %d\n",Min());
}
