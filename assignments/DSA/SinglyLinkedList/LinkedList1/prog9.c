/*
 
   write a real-time example for a linked list and print its data 
   Take 5 nodes from the user
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct laptop{

	char lName[20];
	float price;
	int RAM;
	struct laptop *next;
}laptop;

laptop *head = NULL;

void NextNode(){

	laptop *newnode = (laptop*)malloc(sizeof(laptop));

	printf("Enter name of laptop = ");
	fgets(newnode -> lName,20,stdin);

	printf("Enter price of laptop = ");
	scanf("%f",&newnode -> price);

	printf("Enter RAM = ");
	scanf("%d",&newnode -> RAM);

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		laptop *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp->next = newnode;
	}
}

void PrintLL(){

	laptop *temp = head;

	while(temp != NULL){

		printf("%s",temp -> lName);
		printf("%f\n",temp -> price);
		printf("%d\n",temp -> RAM);

		temp = temp -> next;
	}
}

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	PrintLL();
}
       	
