/*
 
   WAP to check the prime number present in the data from the node.

*/  


#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int num;
	struct Demo *next;
}demo;

demo *head = NULL;

void NextNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter a number = ");
	scanf("%d",&newnode->num);

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		demo *temp = head;
		while(temp -> next != NULL){
			temp = temp->next;
		}
		temp -> next = newnode;
	}
}

void Prime(){
	
	demo *temp = head;

	while(temp != NULL){
		
		int flag = 0;
		for(int i = 2; i<=temp->num; i++){
			
			if(temp -> num % i==0)
				flag++;
		}
		
		if(flag == 1)
			printf("Prime number = %d\n",temp->num);

		temp = temp -> next;
	}
	
}	

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	Prime();
}
