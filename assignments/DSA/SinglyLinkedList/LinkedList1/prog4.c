/*
 
   Q.5] Write a demo structure consisting of integer data
   	Take the number of nodes from the user & print the addition of the integer data.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int num;
	struct Demo *next;
}demo;

demo *head = NULL;

void NextNode(){

	demo *newnode = (demo*)malloc(sizeof(demo));

	printf("Enter a number = ");
	scanf("%d",&newnode->num);

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		demo *temp = head;
		while(temp -> next != NULL){
			temp = temp->next;
		}
		temp -> next = newnode;
	}
}

int Add(){

	demo *temp = head;
	int sum = 0;
	while(temp != NULL){
		sum = sum + temp -> num;
		temp = temp -> next;
	}
	return sum;
}

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	printf("sum of data = %d\n",Add());
}
