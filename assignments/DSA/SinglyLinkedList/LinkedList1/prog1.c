/*
 
 WAP for the LinkedList of malls consisting of it name, number of shops, & revenue; 
 connect 3 malls in the LinkedList & Print their data.

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Mall{
	char mName[20];
	int shops;
	float revenue;
	struct Mall *next;
}mall;

mall *head = NULL;

void NextNode(){

	mall *newnode = (mall*)malloc(sizeof(mall));

	printf("Enter mall name = ");
	fgets(newnode->mName,20,stdin);

	printf("Enter no of shops = ");
	scanf("%d",&newnode -> shops);

	printf("Enter revenue = ");
	scanf("%f",&newnode -> revenue);

	getchar();

	newnode -> next = NULL;

	if(head == NULL)
		head = newnode;
	else{
		mall *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	mall *temp = head;

	while(temp != NULL){
		
		printf("%s",temp -> mName);
		printf("%d\n",temp -> shops);
		printf("%f\n",temp -> revenue);
		
		temp = temp -> next;
	}
}

void main(){

	NextNode();
	NextNode();
	NextNode();

	PrintLL();
}






























