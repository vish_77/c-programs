/*
   WAP  for the LinkedList of States in India xonsisting of its name, Population, Budget, & Literacy 
   Connect 4 States in the LInkedList & Print their data.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct State{

	char stName[20];
	long Population;
	float Budget;
	float Literacy;
	struct State *next;
}St;

St *head = NULL;

void NextNode(){

	St *newnode = (St*)malloc(sizeof(St));

	printf("Enter name of state = ");
	fgets(newnode->stName,20,stdin);

	printf("Enter population of state = ");
	scanf("%ld",&newnode -> Population);

	printf("Enter Budget of state = ");
	scanf("%f",&newnode -> Budget);

	printf("Enter literacy rate = ");
	scanf("%f",&newnode -> Literacy);

	getchar();

	newnode -> next = NULL;

	if(head == NULL){
		head = newnode;
	}else{
		St *temp = head;

		while(temp -> next != NULL){
			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	St *temp = head;

	while(temp != NULL){

		printf("%s",temp -> stName);
		printf("%ld\n",temp -> Population);
		printf("%f\n",temp -> Budget);
		printf("%f\n",temp -> Literacy);

		temp = temp -> next;
	}
}

void main(){

	NextNode();
	NextNode();
	NextNode();
	NextNode();

	PrintLL();
}
