/*

   WAP that searches all the Palindrome data elements from a singly linear
   linked list. And Print the position of palindrome data
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;

}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter a data = ");
	scanf("%d",&(newnode -> data));

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){

			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	node *temp = head;

	while(temp->next != NULL){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

int NodeCount(){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

void Palindrome(){
	
	node *temp = head;
	int count = 0;

	while(temp != NULL){
		count ++;
		int sum = 0;
		int num = temp -> data;

		while(temp -> data != 0){
			
			sum = (sum * 10)+(temp -> data % 10);
			temp -> data = temp -> data / 10;
		}

		if(sum == num)
			printf("Palindrom found at %d\n",count);

		temp = temp -> next;
	}
}

void main(){

        char choice;

        do{
                printf("Enter 1 for AddNode()\n");
                printf("Enter 2 for PrintLL()\n");
                printf("Enter 3 for NodeCount()\n");
                printf("Enter 4 for Palindrome\n");                         

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int node;
                                        printf("Enter no of nodes = ");
                                        scanf("%d",&node);

                                        for(int i = 1; i<= node; i++){
                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :
                                PrintLL();
                                break;

                        case 3 :
                                printf("Node count = %d\n",NodeCount());
                                break;

			case 4 :
				Palindrome();
				break;
				
                        default :
                                printf("Enter corrct choice\n");

                }

                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}
                                                                                                                                                                                                                                                                                                                                                  
	

