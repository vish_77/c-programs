/*

   WAP that accepts a singly linear linked list from the user.
   Reverse the data elements from the linked list.

*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Node{

	char str[20];
	struct Node *next;

}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter a name = ");
	scanf("%s",newnode->str);

	newnode -> next = NULL;

	return newnode;
}


void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){

			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	node *temp = head;

	while(temp->next != NULL){
		printf("|%s| -> ",temp -> str);
		temp = temp -> next;
	}
	printf("|%s|\n",temp -> str);
}

int NodeCount(){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

void StrReverse(){

	node * temp = head;

	while(temp != NULL){

		char *ptr2 = temp -> str;
		char *ptr1 = temp -> str;

		int i = 0;

		while(*ptr1 != '\0'){
			ptr1++;
		}

		char *temp1;
		while(ptr2 < ptr1){
			
			*temp1 = *ptr2;
			*ptr2 = *ptr1;
			(*temp).str[i] = *temp1;

			i++;
			ptr2 ++;
			ptr1 --;
		}
		temp = temp -> next;

	}
}
			

void main(){

        char choice;

        do{
                printf("Enter 1 for AddNode()\n");
                printf("Enter 2 for PrintLL()\n");
                printf("Enter 3 for NodeCount()\n");
 		printf("Enter 4 for StrReverse()\n");                            

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int node;
                                        printf("Enter no of nodes = ");
                                        scanf("%d",&node);

                                        for(int i = 1; i<= node; i++){
                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :
                                PrintLL();
                                break;

                        case 3 :
                                printf("Node count = %d\n",NodeCount());
                                break;

			case 4 :
				StrReverse();
				break;

			default :
                                printf("Enter corrct choice\n");

                }

                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}
                                                                                                                                                                                                                                                                                                                                                  
	

