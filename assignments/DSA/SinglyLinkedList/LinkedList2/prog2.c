/*

   WAP that searches for the second occurrence of a particular 
   element from a singly linear linked list.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;

}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter a data = ");
	scanf("%d",&(newnode -> data));

	newnode -> next = NULL;

	return newnode;
}

void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){

			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	node *temp = head;

	while(temp->next != NULL){
		printf("|%d| -> ",temp -> data);
		temp = temp -> next;
	}
	printf("|%d|\n",temp -> data);
}

int NodeCount(){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

void SecLastOcc(int num){

	node *temp = head;
	
	int flag = 0;
	int count = 0,sl = 0, l = 0;
	
	while(temp != NULL){
		count++;
		if(temp -> data == num){
			flag ++;
			sl = l;
			l = count;
		}
		temp = temp -> next;
	}

	if(flag == 0)
		printf("Number is not present in list\n");

	else if(flag == 1)
		printf("Number is occur only once\n");

	else
		printf("Second last occurance = %d\n",sl);

}		


void main(){

        char choice;

        do{
                printf("Enter 1 for AddNode()\n");
                printf("Enter 2 for PrintLL()\n");
                printf("Enter 3 for NodeCount()\n");
                printf("Enter 4 for second last occurance\n");                         

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int node;
                                        printf("Enter no of nodes = \n");
                                        scanf("%d",&node);

                                        for(int i = 1; i<= node; i++){
                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :
                                PrintLL();
                                break;

                        case 3 :
                                printf("Node count = %d\n",NodeCount());
                                break;

			case 4 :
				{
					int num;
					printf("Enter a number = ");
					scanf("%d",&num);
					SecLastOcc(num);
				}
				break;
				
                        default :
                                printf("Enter corrct choice\n");

                }

                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}
                                                                                                                                                                                                                                                                                                                                                  
	

