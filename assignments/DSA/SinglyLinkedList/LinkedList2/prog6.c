/*

   WAP that accepts a singly linear linked list from the user. Take a 
   number from the user nad print the data of the length of that number.

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	char str[20];
	struct Node *next;

}node;

node *head = NULL;

node *CreateNode(){

	node *newnode = (node*)malloc(sizeof(node));

	printf("Enter a name = ");
	scanf("%s",newnode->str);
/*
	char ch;
	int i= 0;

	while((ch = getchar()) != '\n'){

		(*newnode).str[i] = ch;
		i++;
	}
	
	getchar();
*/
	newnode -> next = NULL;

	return newnode;
}


void AddNode(){

	node *newnode = CreateNode();

	if(head == NULL){
		head = newnode;
	}else{
		node *temp = head;

		while(temp -> next != NULL){

			temp = temp -> next;
		}
		temp -> next = newnode;
	}
}

void PrintLL(){

	node *temp = head;

	while(temp->next != NULL){
		printf("|%s| -> ",temp -> str);
		temp = temp -> next;
	}
	printf("|%s|\n",temp -> str);
}

int NodeCount(){

	node *temp = head;
	int count = 0;
	while(temp != NULL){
		count ++;
		temp = temp -> next;
	}
	return count;
}

void StrPrint(int num){

	node *temp = head;

	while(temp != NULL){
		int count = 0;
		
		char *ch = temp -> str;

		while( *ch != '\0'){
			count++;
			ch ++;
		}
		if(count == num){
			printf("%s\n",temp -> str);
		}
		temp = temp -> next;
	}
}


void main(){

        char choice;

        do{
                printf("Enter 1 for AddNode()\n");
                printf("Enter 2 for PrintLL()\n");
                printf("Enter 3 for NodeCount()\n");
 		printf("Enter 4 for StrPrint()\n");                            

                int ch;
                printf("Enter your choice = ");
                scanf("%d",&ch);

                switch(ch){

                        case 1 :
                                {
                                        int node;
                                        printf("Enter no of nodes = ");
                                        scanf("%d",&node);

                                        for(int i = 1; i<= node; i++){
                                                AddNode();
                                        }
                                }
                                break;

                        case 2 :
                                PrintLL();
                                break;

                        case 3 :
                                printf("Node count = %d\n",NodeCount());
                                break;

			case 4 :
				{
					int num;
					printf("Enter a length = ");
					scanf("%d",&num);
					StrPrint(num);
				}
				break;

			default :
                                printf("Enter corrct choice\n");

                }

                getchar();
                printf("Do you want to continue = ");
                scanf("%c",&choice);

        }while(choice == 'y' || choice == 'Y');
}
                                                                                                                                                                                                                                                                                                                                                  
	

