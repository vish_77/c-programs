//prime number

#include<stdio.h>
#include<math.h>
#include<stdbool.h>

bool Isprime(int,int);
int sqrot(int);

void main(){

	int num;
	printf("Enter a number = ");
	scanf("%d",&num);
	
	int range = sqrot(num);
	printf("%d\n",range);
	int a = Isprime(num,range);
	
	if(a == 1)
		printf("%d is prime\n",num);
	else
		printf("%d is not prime\n",num);
	

}

int sqrot(int num){

	for(int i = 1; i<=num/2; i++){
		
		int x = i*i;
	
		if(x >= num){
		       return i;
		}
	}
}

bool Isprime(int num,int range){
	
	int count = 0;

	for(int i = 2; i<= range; i++){

		if(num % i == 0)
			count ++;

	}
	if(count == 0)
		return true;
	else
		return false;
}


