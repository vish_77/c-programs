//give count of array ele pairs which sum equals to 6.

#include<stdio.h>

int SumCount(int *,int);

void main(){

	int arr[] = {0,1,2,3,4,5,6};
	int size = sizeof(arr)/sizeof(int);
	int count = SumCount(arr,size);
	printf("%d\n",count);
}

int SumCount(int *arr,int size){
	
	int count=0;

	for(int i = 0; i<size; i++){
		for(int j = i+1; j<size; j++){

			if(arr[i] + arr[j] == 6){
				count ++;
			}
		}
	}
	return count;
}
	
