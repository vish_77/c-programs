// Wap to take an array from the user in another function and print that array in Main Function. take the array size from the user.


#include<stdio.h>

int* fun(int* arr,int size){


	printf("Enter array elements:\n");
	
	for(int i = 0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	return arr;
}

void main(){

	int size;
	printf("Enter array size:");
	scanf("%d",&size);
	
	int arr[size];

	int* ptr = fun(arr,size);
	printf("array elements :\n");
	
	for(int i = 0;i<size; i++){
		printf("%d\n",*(ptr+i));
	}


}
