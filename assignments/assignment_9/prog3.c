//wap to take an array from the user in the main function and print that array in another function. take the array size from the user.

#include<stdio.h>
void printArr(int* ,int);

void main(){
	
	int size;

	printf("Enter array size:");
	scanf("%d",&size);

	int arr[size];

	printf("Enter array elements\n");
	for(int i = 0; i<size; i++){
		scanf("%d",&arr[i]);
	}

	printArr(arr,size);
}

void printArr(int * arr, int size){
	
	printf("Array elements are\n");
	for(int i = 0; i<size; i++){
		printf("%d\n",*(arr+i));
	}

}
