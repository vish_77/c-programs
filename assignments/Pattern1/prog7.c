/*

1	2	9	4	
25	6	49	8	
81	10	121	12	
169	14	225	16	

 */

#include<stdio.h>
void main(){
	int row,cols;
	printf("Enter rows = ");
	scanf("%d",&row);
	printf("Enter cols = ");
	scanf("%d",&cols);
	
	int num = 1;
	for(int i = 0;i<row;i++){
	
		for(int j = 0;j<cols;j++){
			if(j%2 == 0){
				printf("%d\t",num*num);
			}else{
				printf("%d\t",num);
			}
			num++;
		}
		printf("\n");
	}
}

		
