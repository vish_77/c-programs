/*

1 2 3
a b c
1 2 3
a b c

*/

#include<stdio.h>
void main(){
	int rows,cols;
	printf("Enter no rows = ");
	scanf("%d",&rows);
	printf("Enter no cols = ");
	scanf("%d",&cols);

	for(int i = 0;i<rows;i++){

		for(int j = 1;j<=cols;j++){
			if(i%2==0){
				printf("%d ",j);
			}else{
				printf("%c ",j+96);
			}

		}
		printf("\n");
	}
}	
