// WAP to allocate memory of int type then free its memory without using the free function

#include<stdio.h>
#include<stdlib.h>

void main(){

	int *ptr = (int*)malloc(sizeof(int));

	*ptr = 10;

	printf("%d\n",*ptr);

	ptr = (int*)realloc(ptr,0);

	printf("%d\n",*ptr);
}
