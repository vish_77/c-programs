//WAP to allocate a memory of float type Take value from the user & print it use malloc function

#include<stdio.h>
#include<stdlib.h>

void main(){

	float *ptr = (float *)malloc(sizeof(float));

	printf("Enter float value = ");
	scanf("%f",ptr);
	printf("%f\n",*ptr);
	free(ptr);
}
