// WAP to allocate a memory for salary of n friends use calloc
//increase a number of friends in 3rd prog use realloc


#include<stdio.h>
#include<stdlib.h>

void main(){

	int num;
	printf("Enter no of friends =");
	scanf("%d",&num);

	float *ptr = (float*)calloc(num,sizeof(float));

	printf("Enter salary = \n");
	for(int i = 0; i<num; i++){
		scanf("%f",ptr+i);
	}

	printf("Salary of friends = \n");
	for(int i = 0; i<num; i++){
		printf("%f\n",*(ptr+i));
	}
	
	int num2;
	printf("ENter no of friends you want to increase = ");
	scanf("%d",&num2);

	float *ptr2 = (float*)realloc(ptr,num2);

	printf("Enter salary = \n");
	for(int i = num; i<num2; i++){
		scanf("%f",ptr2+i);
	}
	
	printf("Salary of all friends = \n");
	for(int i = 0; i<num2; i++){
		printf("%f\n",*(ptr2+i));
	}
}

				
