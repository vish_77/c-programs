// WAP to allocate a memory for salary of n friends use calloc

#include<stdio.h>
#include<stdlib.h>

void main(){

	int num;
	printf("Enter no of friends =");
	scanf("%d",&num);

	float *ptr = (float*)calloc(num,sizeof(float));

	printf("Enter salary = \n");
	for(int i = 0; i<num; i++){
		scanf("%f",ptr+i);
	}

	printf("Salary of friends = \n");
	for(int i = 0; i<num; i++){
		printf("%f\n",*(ptr+i));
	}
}
				
