/*

   WAP that accepts number from user saperate digits from that number and enter them sort the array in assending
   order.

   IP = Enter number : 942111423
   OP = 1 | 1 | 1 | 2 | 2 | 3 | 4 | 4 | 9

*/

#include<stdio.h>

void main(){

	int num,x=0,rem;
	printf("Enter a number = ");
	scanf("%d",&num);

	int arr[11];
	int temp = num;
	
	while(temp != 0){

		rem = temp % 10;	
		arr[x] = rem;
		temp = temp/10;
		x++;
	}
	
	int temp1;
	for(int i = 0; i<x; i++){
		
		for(int j = i+1; j<x; j++){

			if(arr[i] > arr[j]){
				
				temp1 = arr[i];
				arr[i] = arr[j];
				arr[j] = temp1;
			}
		}
	}

	for(int i = 0; i<x; i++){
		printf("%d	",arr[i]);
	}
	printf("\n");
}
