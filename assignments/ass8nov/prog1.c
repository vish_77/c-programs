/*
 
WAP that accepts a string from the user and prints the length of the string. Use myStrlen()

*/

#include<stdio.h>

int mystrlen(char *src){
	
	int num=0;
	while(*src != '\0'){
		num++;
		src++;
	}

	return num;
}

void main(){

	char arr[100];
	printf("Enter string = \n");
	gets(arr);

	printf("count is = %d\n",mystrlen(arr));
}
