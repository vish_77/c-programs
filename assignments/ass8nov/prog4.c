/*
 
	WAP that accepts an Array on Length N from the user and calculates squares of all even elements and 
	cubes of all odd elements from that array and replaces the elements respectively with the answer
	
	IP = Length of Array: 4
	     Enter Elements in Array: 1  2  3  4  

	OP = 1  4  27  16
	
*/

#include<stdio.h>

void main(){

	int arrsize;
	printf("Length of Array = ");
	scanf("%d",&arrsize);

	int arr[arrsize];
	
	printf("Enter Elements in Array :\n");

	for(int i = 0; i<arrsize; i++){
		
		scanf("%d",&arr[i]);

		if(arr[i] % 2 == 0){
			arr[i] = arr[i]*arr[i];
		}else{
			arr[i] = arr[i]*arr[i]*arr[i];
		}
	}

	for(int i = 0; i<arrsize; i++){
		printf("%d  ",arr[i]);
	}
	printf("\n");
}
