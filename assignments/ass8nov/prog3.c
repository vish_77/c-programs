/*

   WAP that accepts number from user saperate digits from that number and enter them sort the array in descending
   order.

   IP = Enter number : 942111423
   OP = 9 | 4 | 4 | 3 | 2 | 2 | 1 | 1 | 1

*/


#include<stdio.h>

void main(){
	
	int arr[10];
	int num,x=0,rem;
	printf("Enter num = ");
	scanf("%d",&num);
	
	while(num != 0){

		rem = num % 10;
		arr[x] = rem;
		num = num / 10;
		x++;
	}
	
	int temp;
	for(int i = 0; i<x; i++){

		for(int j = i+1; j<x; j++){

			if(arr[i]<arr[j]){
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	for(int i = 0; i<x; i++){
		printf("%d	",arr[i]);
	}
	printf("\n");
}
	
