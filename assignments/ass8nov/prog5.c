/*
 
	WAP that accepts 2 strings from the user and concat the strings and print 
	the concated string use mystrcat

*/


#include<stdio.h>

char * mystrcat(char * dest, const char * src){

	while(*dest != '\0'){
		dest++;
	}

	while(*src != '\0'){
		*dest = *src;
		dest++;
		src++;
	}

	*dest = '\0';
	return dest;
}

void main(){
	
	char arr1[20];
	char arr2[20];

	printf("Enter string1 = \n");
	gets(arr1);

	printf("Enter string2 = \n");
	gets(arr2);

	mystrcat(arr1,arr2);

	puts(arr1);
}	
